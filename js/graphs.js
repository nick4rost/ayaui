/**
 * Created by Elorm on 2/22/2020.
 */
var chartTooltip = {
    borderWidth: 0.5,
    bodySpacing: 10,
    xPadding: 15,
    yPadding: 15,
    cornerRadius: 0.15,
    displayColors: false
};

//generate graph of number of purchased slots against company
function loadHitsGraph(search, url){
    $('.tabbed-menu-list-item').removeClass('active');
    $('#mp-reports').addClass('active');
    $('.results-view').css('display', 'none');
    $('.displaying-tbls').css('display', 'none');
    $('#myChart').css('display', 'block');
    $('#myChart2').css('display', 'block');
    if(search != '0'){
        search = '&search='+search;
    }else{
        search='';
    }
    var dataString= 'find=mp-reports&type=spots'+search;
    $.ajax({
        url: url,
        method: 'post',
        data: dataString,
        success: function (json) {
            $('.spotsGraph').html("<div id='spotsGraphDisplay'>"+json+"</div>");
        },
        error: function(){
            $('.view-tbls').css('display', 'block');
            $('.show-graphs').css('display', 'none');
            $('.show-tbls').css('display', 'block');
            $('.displaying-tbls').html('<h3>No records available</h3>');
        }
    });

}

//graph of spend/revenue
function loadSpendGraph(search, url){
    if(search != '0'){
        search = '&search='+search;
    }else{
        search='';
    }
    var dataString= 'find=mp-reports&type=spend'+search;
    $.ajax({
        url: url,
        method: 'post',
        data: dataString,
        success: function (json) {
            $('.spendGraph').html("<div id='spendGraphDisplay'>"+json+"</div>");
        },
        error: function(){
            $('.view-tbls').css('display', 'block');
            $('.show-graphs').css('display', 'none');
            $('.show-tbls').css('display', 'block');
            $('.displaying-tbls').html('<h3>No records available</h3>');
        }
    });

}

//display report tables
function getTables(val, id, url){
    //console.log('here');
    $('.view-tbls').css('display', 'block');
    $('.show-graphs').css('display', 'none');
    $('.show-tbls').css('display', 'block');
    var dataString = 'find='+id+'&search='+val;
    $.ajax({
        url: url,
        method: "post",
        data: dataString,
        success: function (response) {
            $('.displaying-tbls').html(response);
        }
        //error: function () {
        //    alert("Failed processing query");
        //}
    });
};

//client graphs


//generate graph of number of purchased slots against company
function loadPieGraph(url){
    var dataString= 'type=pie';
    $.ajax({
        url: url,
        method: 'post',
        data: dataString,
        success: function (json) {
            $('.pieGraph').html("<div id='pieGraphDisplay'>"+json+"</div>");
        },
        error: function(){
            $('.adverts-playing').html('<h3>No records available</h3>');
        }
    });

}

//graph of spend/revenue
function loadBarGraph(url){
    var dataString= 'type=bar';
    $.ajax({
        url: url,
        method: 'post',
        data: dataString,
        success: function (json) {
            $('.barGraph').html("<div id='barGraphDisplay'>"+json+"</div>");
        },
        error: function(){
            $('.adverts-playing').html('<h3>No records available</h3>');
        }
    });

}