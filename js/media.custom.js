/**
 * Created by Elorm on 2/22/2020.
 */


//default tables
$(document).ready(function(){

    //navigation
    var index = window.location.href.lastIndexOf("/") + 1;
    var filenameWithExtension = window.location.href.substr(index);
    var page = filenameWithExtension.split(".")[0]; // <-- added this line
    //add active class to main menu
    $('#'+page).addClass('active');

    //find <a> with href = filenameWithExtension for sub-menu
    var menu = $('.sub-menu a');
    menu.filter(function(){
            var href= $(this).attr('href');
            if(filenameWithExtension == href){
                $(this).closest('li').addClass('active');
            }
        }
    )

    //top bar details
    //$.ajax({
    //    url: "includes/topbar.php",
    //    type: "GET",
    //    success: function (html) {
    //        $('.fixed-top').html(html);
    //    }
    //});

    //console.log('here');
    var dataString= 'find=mp-adverts';
    $.ajax({
        url: 'processing/Media.Processing.php',
        method: "post",
        data: dataString,
        success: function(response){
            $('.results-view').html(response);
        },
        error:function(){
            //alert("Failed processing query");
        }
    });
});

//change view on tab click
$('#mp-adverts').click(function(){
    $('.tabbed-menu-list-item').removeClass('active');
    $(this).addClass('active');
    $('#myChart').css('display', 'none');
    $('#myChart2').css('display', 'none');
    $('.displaying-tbls').css('display', 'none');
    var val= $(this).attr('id');
    var nav= $(this).attr('rel');
    var url = nav+'-processing.php';
    //console.log(url);
    var dataString= 'find='+val;
    $.ajax({
        url: url,
        method: "post",
        data: dataString,
        success: function(response){
            $('.results-view').css('display', 'block');
            $('.results-view').html(response);
        },
        error:function(){
            //alert("Failed processing query");
        }
    });
});

//search by date
$('.search-btn').click(function(){
    var val = document.getElementById('select-date').value;
    var id = $('.active').attr('id');
    //console.log(id);
    if(id=='mp-adverts') {
        $('#myChart').css('display', 'none');
        $('#myChart2').css('display', 'none');
        $('.displaying-tbls').css('display', 'none');
        var dataString = 'find=mp-adverts&search=' + val;
        $.ajax({
            url: 'processing/Media.Processing.php',
            method: "post",
            data: dataString,
            success: function (response) {
                $('.results-view').css('display', 'block');
                $('.results-view').html(response);
            },
            error: function () {
                //alert("Failed processing query");
            }
        });
    }else{
        loadHitsGraph(val, 'graphs-processing.php');
        loadSpendGraph(val, 'graphs-processing.php');
        getTables(val, 'mp-reports', 'processing/Media.Processing.php');
    }
});

//display report tables
function showTables(){
    $('.graph-container').css('display', 'none');
    $('.results-view').css('display', 'none');
    $('.show-tbls').css('display', 'none');
    $('.displaying-tbls').css('display', 'block');
    $('.show-graphs').css('display', 'block');
};

//display report tables
function showGraphs(){
    $('.results-view').css('display', 'none');
    $('.displaying-tbls').css('display', 'none');
    $('.show-graphs').css('display', 'none');
    $('.show-tbls').css('display', 'block');
    $('.graph-container').css('display', 'block');
};



//change status
function changeState(id, status, tab, url){
    //console.log(status + tab + url + id);
    var dataString= 'find='+tab+'&id='+id+'&status='+status;
    $.ajax({
        url:url,
        method: "post",
        data: dataString,
        success: function(response){
            //console.log(response);
        },
        error:function(){
            //alert("Failed processing query");
        }
    });
};

//    Schedule/Cancel action
//$('.my-small-btn').click(function(){
//    console.log('here');
//    var id = $(this).attr('id');
//    var rel = $(this).attr('rel');
//    if($(this).hasClass('btn-success')){
//        changeState(id, 1,rel,'processing/Media.Processing.php');
//        $(this).removeClass('btn-success');
//        $(this).addClass('btn-danger');
//        $(this).html('<i class="fa fa-lg fa-ban"></i>');
//        $('.'+id+'-current-status').html('Yes');
//
//    }else{
//        changeState(id, 0,rel,'processing/Media.Processing.php');
//        $(this).removeClass('btn-danger');
//        $(this).addClass('btn-success');
//        $(this).html('<i class="fa fa-lg fa-check-circle"></i>');
//        $('.'+id+'-current-status').html('No');
//    }
//});

function manageSchedule(id, rel){
    //console.log(id, rel, $('#'+id).hasClass('btn-success'));
    if($('#'+id).hasClass('btn-success')){
        changeState(id, 1,rel,'processing/Media.Processing.php');
        $('#'+id).removeClass('btn-success');
        $('#'+id).addClass('btn-danger');
        $('#'+id).html('Cancel Schedule');
        $('#'+id+'-current-status').html('Yes');

    }else{
        changeState(id, 0,rel,'processing/Media.Processing.php');
        $('#'+id).removeClass('btn-danger');
        $('#'+id).addClass('btn-success');
        $('#'+id).html('Confirm Schedule');
        $('#'+id+'-current-status').html('No');
    }
};


function showAdvert(file, folder){
    //var file = $(this).attr();
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    file = file.replace("~", "'");
    folder = folder+file;
    $('.advert-title-display').html(file);
    if(extension == 'mp4'){
        $('.display-advert').html(' <video  style="width:100% !important;"  controls="controls" id="audio_player">' +
            '<source src="'+folder+'" type="video/mp4">' +
            'Your browser does not support the audio element. </video>');
    }else if(extension == 'mp3'){
        $('.display-advert').html(' <audio  style="width:100% !important;"  controls="controls" id="audio_player">' +
            '<source src="'+folder+'" type="audio/mpeg">' +
            'Your browser does not support the audio element. </audio>');
    }else{
        $('.display-advert').html('<img class="advert-image" style="width:100% !important;" src="'+folder+'" />');
    }
    //console.log(extension + ' this is it');
}