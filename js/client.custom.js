/**
 * Created by Elorm on 2/26/2020.
 */
$(document).ready(function(){

    //navigation
    var index = window.location.href.lastIndexOf("/") + 1;
    var filenameWithExtension = window.location.href.substr(index);
    var page = filenameWithExtension.split(".")[0]; // <-- added this line
    //add active class to main menu
    $('#'+page).addClass('active');

    //find <a> with href = filenameWithExtension for sub-menu
    var menu = $('.sub-menu a');
    menu.filter(function(){
            var href= $(this).attr('href');
            if(filenameWithExtension == href){
                $(this).closest('li').addClass('active');
            }
        }
    )

    //top bar details
    //$.ajax({
    //    url: "includes/topbar.php",
    //    type: "GET",
    //    success: function (html) {
    //        $('.fixed-top').html(html);
    //    }
    //});

//default calendar display
    function myCalendar(){
        $('.wait-loading').html('<span class="btn btn-warning">Loading...please wait</span>');
        $.ajax({
            url: 'processing/Client.Processing.php?default',
            method: "get",
            success: function(response){
                $('.wait-loading').html('');
                $('.calendar').html(response);
            },
            error:function(){
                //alert("Failed processing query");
            }
        });
    };

    myCalendar();

    $('.slotid').hide();
    $('.rates').hide();

    //getting value for pause button and slot id after clicking the pause button
    $(".pause").click(function(){
        var pause= $(this).val();
        var row = $(this).closest('tr');
        var slotid = row.find('td:eq(7)').text();
        row.attr('value', slotid);
        $.ajax({
            url: "processing/Client.Processing.php",
            type: "POST",
            data: {"pause":pause,"slotid":slotid},
            success: function(data){
                window.location.href="client-dashboard.php?client-purchase";
            },
            error: function(){
                $('.notice-msg').html("Operation Failed. Please try again.");
            }
        });
    });

    //getting value of button and id of slot when cancel button is clicked
    $(".cancel").click(function(){
        var cancel= $(this).val();
        var row = $(this).closest('tr');
        var slotid2 = row.find('td:eq(7)').text();
        row.attr('value', slotid2);
        $.ajax({
            url: "processing/Client.Processing.php",
            type: "POST",
            data: {"cancel":cancel,"slotid2":slotid2},
            success: function(data){
                window.location.href="client-dashboard.php?client-purchase";
            },
            error: function(){
                $('.notice-msg').html("Operation Failed. Please try again.");
            }
        });
    });

    $(".remove").click(function(){
        if(confirm("Do you want to delete this advert?")) {
            var remove = $(this).val();
            var row = $(this).closest('tr');
            var slotid3 = row.find('td:eq(7)').text();
            row.attr('value', slotid3);
            $.ajax({
                url: "processing/Client.Processing.php",
                type: "POST",
                data: {"remove": remove, "slotid3": slotid3},
                success: function (data) {
                    row.hide();
                },
                error: function(){
                    $('.notice-msg').html("Operation Failed. Please try again.");
                }
            });
        }
    });

    //when the already paused button is clicked-->unpausing
    $(".unpause").click(function(){
        var unpause= $(this).val();
        var row = $(this).closest('tr');
        var slotid4 = row.find('td:eq(7)').text();
        row.attr('value', slotid4);
        $.ajax({
            url: "processing/Client.Processing.php",
            type: "POST",
            data: {"unpause":unpause,"slotid4":slotid4},
            success: function(data){
                window.location.href="client-dashboard.php?client-purchase";
            },
            error: function(){
                $('.notice-msg').html("Operation Failed. Please try again.");
            }
        });
    });

    //when the already cancelled  button is clicked-->uncancelling
    $(".uncancel").click(function(){
        //console.log('here');
        var uncancel= $(this).val();
        var row = $(this).closest('tr');
        var slotid5 = row.find('td:eq(7)').text();
        row.attr('value', slotid5);
        $.ajax({
            url: "processing/Client.Processing.php",
            type: "POST",
            data: {"uncancel":uncancel,"slotid5":slotid5},
            success: function(data){
                window.location.href="client-dashboard.php?client-purchase";
            },
            error: function(){
                $('.notice-msg').html("Operation Failed. Please try again.");
            }
        });
    });

});

function addClassActive(arg){
    $('.tabbed-menu-list-item').removeClass('active');
    $("#"+arg).addClass('active');
};


//change status
function changeState(id, status){
    //console.log('here');
    var dataString= 'find=cl-status&id='+id+'&status='+status;
    $.ajax({
        url: 'processing/Client.Processing.php',
        method: "post",
        data: dataString,
        success: function(response){
        },
        error:function(){
            //alert("Failed processing query");
        }
    });
};


//search by date
$('.search-btn').click(function(){
    var val = document.getElementById('select-date').value;
    var id = $('.active').attr('id');
    //console.log(id);
    if(id=='cl-pur-adverts') {
        $('#myChart').css('display', 'none');
        $('#myChart2').css('display', 'none');
        var dataString = 'find=cl-adverts&search=' + val;
        $.ajax({
            url: 'processing/Client.Processing.php',
            method: "post",
            data: dataString,
            success: function (response) {
                $('.results-view').css('display', 'block');
                $('.results-view').html(response);
            },
            error: function () {
                //alert("Failed processing query");
            }
        });
    }else{
        loadHitsGraph(val);
        loadSpendGraph(val);
    }
});

//show field based on media type
//show station select for tv and region select for radio
function getMtResult(){
    var sel = document.getElementById('select-mt').value;
    $('.wait-loading').html('<span class="btn btn-warning">Loading...please wait</span>');
    if(sel == 'TV') {
        $('.select-region').css('display', 'none');
    }else if(sel == 'Radio') {
        $('.select-region').css('display', 'block');
    }
    var dataString = "mt="+sel+"&find=get-radio";
    $.ajax({
        url: "processing/Client.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            $('.wait-loading').html(' ');
            $('.select-station').css('display', 'block');
            $('.radio-tv').html(response);
            //getCalendar();
            //console.log(response);
        },
        error: function(){
            //console.log('Failed Retrieving data');
        }
    });
}

function getRegionRadio(){
    var sel = document.getElementById('select-region').value;
    var dataString = "region="+sel+"&mt=radio&find=get-radio";
    $('.wait-loading').html('<span class="btn btn-warning">Loading...please wait</span>');
    $.ajax({
        url: "processing/Client.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            $('.wait-loading').html(' ');
            $('.select-station').css('display', 'block');
            $('.radio-tv').html(response);
            //console.log(response);
        }
    });
}

function getRegionRadio1(){
    var sel = 0;
    var dataString = "region="+sel+"&mt=radio&find=get-radio";
    $('.wait-loading').html('<span class="btn btn-warning">Loading...please wait</span>');
    $.ajax({
        url: "processing/Client.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            $('.wait-loading').html(' ');
            $('.select-station').css('display', 'block');
            $('.radio-tv').html(response);
            //console.log(response);
        }
    });
}

function getStation2(){
    var sel=[];
    $(':checkbox:checked').each(function(i){
        sel[i] = $(this).val();
    });
    //console.log(sel);
    var dataString = "region="+sel+"&mt=radio&find=get-radio2";
    $('.wait-loading').html('<span class="btn btn-warning">Loading...please wait</span>');
    $.ajax({
        url: "processing/Client.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            $('.wait-loading').html(' ');
            $('.select-station').css('display', 'block');
            $('.radio-tv').html(response);
            //console.log(response);
        }
    });
}

//show and hide all regions list
function getAllRegions(){
    if($('#all-regions'). prop("checked") == true) {
        $('.regions-list-wrap').css('display', 'none');
        getRegionRadio1();
    }else {
        $('.regions-list-wrap').css('display', 'block');
    }
};


function getCalendar(){
    var sel = document.getElementById('select-station').value;
    var all='';
    if($('#all-regions'). prop("checked") == true) {
        var mt = document.getElementById('select-mt').value;
        all='&mt='+mt;
    }
    if(sel==''){
        sel ='0';
    }
    $('.wait-loading').html('<span class="btn btn-warning">Loading...please wait</span>');
    var dataString = "station="+sel+"&find=calendar"+all;
    $.ajax({
        url: "processing/Client.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            $('.wait-loading').html('');
            $('.calendar').html(response);
            //console.log(response);
        }
    });
}


//show add new advert form
function showAddAdvertForm(){
    $('.add-advert').html('Loading, please wait...');
    $.ajax({
        url: "forms/Add.Edit.Advert.Form.php",
        type: "GET",
        success: function (html) {
            $('.add-advert').html(html);
        }
    });
}

//get edit details and display edit form
function showEditAdvertForm(id) {
    $.ajax({
        url: "forms/Add.Edit.Advert.Form.php",
        data: "edit=" + id,
        type: "POST",
        success: function (html) {
            $('.edit-advert').html(html);
        }
    });
}


//get edit details and display edit form
function showEditSocialForm(id) {
    $.ajax({
        url: "forms/Add.Edit.Social.Form.php",
        data: "edit=" + id,
        type: "POST",
        success: function (html) {
            $('.edit-social').html(html);
        }
    });
}

//get edit details and display edit form
function buySocialForm() {
    $('.buy-social').html('Loading, please wait...');
    $.ajax({
        url: "forms/Add.Edit.Social.Form.php",
        type: "GET",
        success: function (html) {
            $('.buy-social').html(html);
        }
    });
}

//get edit details and display edit form
function buyAdvertForm() {
    $.ajax({
        url: "forms/Buy.Advert.Space.Form.php",
        type: "GET",
        success: function (html) {
            $('.buy-advert').html(html);
        }
    });
}

function buyAdvertForForm(ad,id) {
    $.ajax({
        url: "forms/Buy.Advert.Space.Form.php",
        data: "id=" + id +'&ad='+ad,
        type: "GET",
        success: function (html) {
            $('.buy-advert').html(html);
        }
    });
}

//delete advert space action
function deleteAdvert(id){
    if(confirm("Are you sure you want to delete this record?")) {
        var dataString = "del=" + id ;
        $.ajax({
            url: "processing/Client.Processing.php",
            method: "get",
            data: dataString,
            success: function (response) {
                //console.log(response);
                var result = response.split('~');
                $('.message-box').html(result[1]);
                if (result[0] == 0) {
                    setTimeout(function () {
                        location.reload();
                    }, 3000);
                }
            }
        });
    }
}

//delete social media advert space action
function deleteSocial(id){
    if(confirm("Are you sure you want to delete this record?")) {
        var dataString = "smdelete=" + id ;
        $.ajax({
            url: "processing/Client.Processing.php",
            method: "get",
            data: dataString,
            success: function (response) {
                //console.log(response);
                var result = response.split('~');
                $('.message-box').html(result[1]);
                if (result[0] == 0) {
                    setTimeout(function () {
                        location.reload();
                    }, 3000);
                }
            }
        });
    }
}

function getRate(id){
    var selectslots = $('#selectslots'+id).children("option:selected").val();
    var row = $('#selectslots'+id).closest('tr');
    var myselectslots=selectslots.split("/");
    row.find('td:eq(5)').html(myselectslots[1]);
}


$('.submit-slots').click(function(e){
    if($("input:checked").length < 1 ){
        e.preventDefault();
        $('.error-msg').html("Please select a slot to continue.");
//        console.log('here');
    }
});


function openThis(id){
    $('.ad-details').removeClass('show');
    $('#ad'+id).children('div .ad-details').addClass('show');
//        $('#'+id).children('div .ad-details').css('display', 'block');
//        console.log($('#'+id).children('div .ad-details'));
//    console.log('this will work');
}

function closeThis(){
    if($('#tableID .ad-details').hasClass('show')){
        $('#tableID .ad-details').removeClass('show');
    }
}

$(document).on('click touch', function(event){
//    if($('#tableID .ad-details').hasClass('show')){
    if (!$(event.target).parents().addBack().is('#tableID')) {
        $('.ad-details').removeClass('show');
    }
//    }
});

$('.ad-details .show').on('click touch', function(event){
    event.stopPropagation();
})

//proceesing for buy advert summary
function getPurchaseSummary(){
    var selected =new Array();
    var rates =new Array();
    var advert = document.getElementById('select-ad').value,
        monitored = document.getElementById('monitor2').value;

    var select = $('.advert-spaces:checked');
    select.each(function(){
                selected.push($(this).val());
            });

    var str = $( ".selectslots option:selected" );
    str.each(function(){
        if($(this).val() != '--select slot length--') {
            rates.push($(this).val());
        }
    });

    //validation
    if(advert != '' &&  rates.length == selected.length && rates.length !=0  && selected.length !=0 ) {

        var dataString = "submit&select-ad=" + advert + "&mytvslots=" + selected + "&selectslots=" + rates + "&monitor2=" + monitored;
        //console.log(dataString);
        $.ajax({
            url: "tables/Client.Buy.Advert.Summary.php",
            method: "post",
            data: dataString,
            success: function (html) {
                $('#exampleModal2').modal().hide();
                $('body').removeClass('modal-open')
                $('.modal-backdrop').remove()
                $('.main-display').html(html);
                //console.log(html);
            }
        });
    }else{
        alert('Please select all required fields');
    }
}

function confirmPayment(pid, total, medium, pm){
    var dataString = "purchaseid=" + pid +"&total="+total+"&medium="+medium+"&pm="+pm+"&record-transaction";
    $('.main-display').html('Processing purchase, please wait...');
    $.ajax({
        url: "processing/Client.Processing.php",
        method: "post",
        data: dataString,
        success: function (response) {
            //console.log(response)
            var result = response.split('~');
            $('.main-display').html(result[1]);
            if (result[0] == 0) {
                setTimeout(function () {
                    window.location.href="Client.Dashboard.Default.php";
                }, 3000);
            }
        }
    });
}


function showAdvert(file, folder){
    //var file = $(this).attr();
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    file = file.replace("~", "'");
    folder = folder+file;
    $('.advert-title-display').html(file);
    if(extension == 'mp4'){
        $('.display-advert').html(' <video  style="width:100% !important;"  controls="controls" id="audio_player">' +
            '<source src="'+folder+'" type="video/mp4">' +
            'Your browser does not support the audio element. </video>');
    }else if(extension == 'mp3'){
        $('.display-advert').html(' <audio  style="width:100% !important;"  controls="controls" id="audio_player">' +
            '<source src="'+folder+'" type="audio/mpeg">' +
            'Your browser does not support the audio element. </audio>');
    }else{
        $('.display-advert').html('<img class="advert-image" style="width:100% !important;" src="'+folder+'" />');
    }
    //console.log(extension + ' this is it');
}