/**
 * Created by Elorm on 2/19/2020.
 */
$(document).ready(function(){

    //navigation
    var index = window.location.href.lastIndexOf("/") + 1;
    var filenameWithExtension = window.location.href.substr(index);
    var page = filenameWithExtension.split(".")[0]; // <-- added this line
    //add active class to main menu
    $('#'+page).addClass('active');

    //find <a> with href = filenameWithExtension for sub-menu
    var menu = $('.sub-menu a');
    menu.filter(function(){
            var href= $(this).attr('href');
            if(filenameWithExtension == href){
                $(this).closest('li').addClass('active');
            }
        }
    )

    $('.slotid').hide();
    $('.rates').hide();

    //getting value for pause button and slot id after clicking the pause button
    $(".pause").click(function(){
        var pause= $(this).val();
        var row = $(this).closest('tr');
        var slotid = row.find('td:eq(5)').text();
        row.attr('value', slotid);
        $.ajax({
            url: "cl-processing.php",
            type: "POST",
            data: {"pause":pause,"slotid":slotid},
            success: function(data){
                window.location.href="../Admin.Dashboard.Default.php";
            },
            error: function(){
                $('.notice-msg').html("Operation Failed. Please try again.");
            }
        });
    });

    //getting value of button and id of slot when cancel button is clicked
    $(".cancel").click(function(){
        var cancel= $(this).val();
        var row = $(this).closest('tr');
        var slotid2 = row.find('td:eq(5)').text();
        row.attr('value', slotid2);
        $.ajax({
            url: "cl-processing.php",
            type: "POST",
            data: {"cancel":cancel,"slotid2":slotid2},
            success: function(data){
                window.location.href="../Admin.Dashboard.Default.php";
            },
            error: function(){
                $('.notice-msg').html("Operation Failed. Please try again.");
            }
        });
    });

    $(".remove").click(function(){
        if(confirm("Do you want to delete this advert?")) {
            var remove = $(this).val();
            var row = $(this).closest('tr');
            var slotid3 = row.find('td:eq(5)').text();
            row.attr('value', slotid3);
            $.ajax({
                url: "cl-processing.php",
                type: "POST",
                data: {"remove": remove, "slotid3": slotid3},
                success: function (data) {
                    row.hide();
                },
                error: function(){
                    $('.notice-msg').html("Operation Failed. Please try again.");
                }
            });
        }
    });

    //when the already paused button is clicked-->unpausing
    $(".unpause").click(function(){
        var unpause= $(this).val();
        var row = $(this).closest('tr');
        var slotid4 = row.find('td:eq(5)').text();
        row.attr('value', slotid4);
        $.ajax({
            url: "cl-processing.php",
            type: "POST",
            data: {"unpause":unpause,"slotid4":slotid4},
            success: function(data){
                window.location.href="../Admin.Dashboard.Default.php";
            },
            error: function(){
                $('.notice-msg').html("Operation Failed. Please try again.");
            }
        });
    });

    //when the already cancelled  button is clicked-->uncancelling
    $(".uncancel").click(function(){
        var uncancel= $(this).val();
        var row = $(this).closest('tr');
        var slotid5 = row.find('td:eq(5)').text();
        row.attr('value', slotid5);
        $.ajax({
            url: "cl-processing.php",
            type: "POST",
            data: {"uncancel":uncancel,"slotid5":slotid5},
            success: function(data){
                window.location.href="../Admin.Dashboard.Default.php";
            },
            error: function(){
                $('.notice-msg').html("Operation Failed. Please try again.");
            }
        });
    });
});

//change view on tab click
$('.tabbed-menu-list-item').click(function(){
    $('.tabbed-menu-list-item').removeClass('active');
    $(this).addClass('active');
    $('#myChart').css('display', 'none');
    $('#myChart2').css('display', 'none');
    $('.form-view').css('display', 'none');
    var val= $(this).attr('id');
    var nav= $(this).attr('rel');
    var url = nav+'-processing.php';
    console.log(url);
    var dataString= 'find='+val;
    $.ajax({
        url: url,
        method: "post",
        data: dataString,
        success: function(response){
            $('.results-view').css('display', 'block');
            $('.results-view').html(response);
        },
        error:function(){
            //alert("Failed processing query");
        }
    });
});

//change status
function changeState(id, status, rel){
    //console.log('here');
    var dataString= 'find='+rel+'&id='+id+'&status='+status;
    $.ajax({
        url: 'processing/Admin.Processing.php',
        method: "post",
        data: dataString,
        success: function(response){
            console.log(response);
        },
        error:function(){
            //alert("Failed processing query");
        }
    });
};

//search by date
$('.search-btn').click(function(){
    var val = document.getElementById('select-date').value;
    var id = $('.active').attr('id');
    //console.log(id);
    if(id=='admin-adverts') {
        $('#myChart').css('display', 'none');
        $('#myChart2').css('display', 'none');
        $('.form-view').css('display', 'none');
        var dataString = 'find=admin-adverts&search=' + val;
        $.ajax({
            url: 'processing/Admin.Processing.php',
            method: "post",
            data: dataString,
            success: function (response) {
                $('.results-view').css('display', 'block');
                $('.results-view').html(response);
            },
            error: function () {
                //alert("Failed processing query");
            }
        });
    }else{
        loadHitsGraph(val);
        loadSpendGraph(val);
    }
});

//show form
$('.add-new-space-link').click(function(){
    //console.log('here');
    $('.form-view').css('display', 'block');
    $('.results-view').css('display', 'none');
});

function addClassActive(arg){
    $('.tabbed-menu-list-item').removeClass('active');
    $("#"+arg).addClass('active');
};

//show field based on media type
//show station select for tv and region select for radio
function getMtResult(){
    var sel = document.getElementById('select-mt').value;
    //console.log(sel);
    $('.wait-loading').html('Loading...please wait');
    if(sel == 'TV'){
        $('.select-region').css('display', 'none');
        var dataString = "mt=tv&find=get-radio";
        $.ajax({
            url: "processing/Admin.Processing.php",
            method: "post",
            data: dataString,
            success: function(response){
                $('.wait-loading').html(' ');
                $('.select-station').css('display', 'block');
                $('.radio-tv').html(response);
                //getCalendar();
                //console.log(response);
            }
        });
    }else if(sel == 'Radio'){
        $('.wait-loading').html(' ');
        $('.select-station').css('display', 'none');
        $('.select-region').css('display', 'block');
    }
}

function getRegionRadio(){
    //console.log('here');
    var sel = document.getElementById('select-region').value;
    var dataString = "region="+sel+"&mt=radio&find=get-radio";
    $('.wait-loading').html('Loading...please wait');
    $.ajax({
        url: "processing/Admin.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            $('.wait-loading').html(' ');
            $('.select-station').css('display', 'block');
            $('.radio-tv').html(response);
            //console.log(response);
        }
    });
}


function getList(mtype){//list of advert spaces
    var sel = document.getElementById('select-station').value;
    if(sel==''){
        sel ='0';
    }
    $('.wait-loading').html('Loading...please wait');
    //console.log(mtype);
    var dataString = "station="+sel+"&find=get-list&mtype="+mtype;
    $.ajax({
        url: "processing/Admin.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            $('.wait-loading').html('');
            $('tbody').html(response);
            //console.log(response);
        }
    });
}

//    activate/deactivate action
function confirmPaid(id, rel){
    if($('.'+id).hasClass('btn-success')){
        changeState(id, 1,rel,'../processing/Admin.Processing.php');
        $('.'+id).removeClass('btn-success');
        $('.'+id).addClass('btn-secondary');
        $('.'+id).attr('disabled', 'disabled');
        $('.'+id).html('Accepted');
        $('.'+id+'-status').html('Paid');
    }else{
        changeState(id, 0, rel);
        $('.'+id).removeClass('btn-danger');
        $('.'+id).addClass('btn-success');
        $('.'+id).html('Activate');
    }
};

//edit advert space form values
//function openEditForm(id){
//    var rows = $('#row'+id).closest('tr').find('td');
//    var startdate = rows[0].innerHTML,
//        station = rows[1].innerHTML,
//        segment = rows[2].innerHTML,
//        starttime = rows[3].innerHTML,
//        endtime = rows[4].innerHTML,
//        los = rows[5].innerHTML,
//        rate = rows[6].innerHTML,
//        totaltime = rows[7].innerHTML;
//    $('.current-station').html(station);
//    $('.current-station').val(station);
//    $('.current-startdate').val(startdate);
//    $('.current-segment').val(segment);
//};

//show add new space form
function showAddSpaceForm(){
    $.ajax({
        url: "forms/Add.Advert.Space.Form.php",
        type: "GET",
        success: function (html) {
            $('.add-advert-space').html(html);
        }
    });
}

//get edit details and display edit form
function showEditSpaceForm(id) {
    $.ajax({
        url: "forms/Edit.Advert.Space.Form.php",
        data: "id=" + id,
        type: "POST",
        success: function (html) {
            $('.edit-form').html(html);
        }
    });
}

//add new advert space form submission
function addNewAdvertSpace(){
    var mtype = document.getElementById('mtype').value,
        station = document.getElementById('station').value,
        segment = document.getElementById('segment').value,
        starttime = document.getElementById('starttime').value,
        endtime = document.getElementById('endtime').value,
        los = document.getElementById('los').value,
        rate = document.getElementById('rate').value,
        startdate = document.getElementById('startdate').value,
        totaltime = document.getElementById('totaltime').value;
    var dataString = "mtype="+mtype+"&station="+station+"&segment="+segment+"&starttime="+starttime+"&endtime="+endtime+"&los="+los+"&rate="+rate+"&startdate="+startdate+"&totaltime="+totaltime+"&create=new-advert";
    $.ajax({
        url: "processing/Admin.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            //console.log(response);
            var result = response.split('~');
            $('.form-message-box').html(result[1]);
            if(result[0]==0) {
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        }
    });
};

//edit advert space form submission
function editAdvertSpace(){
    var station = document.getElementById('station').value,
        segment = document.getElementById('segment').value,
        starttime = document.getElementById('starttime').value,
        endtime = document.getElementById('endtime').value,
        los = document.getElementById('los').value,
        rate = document.getElementById('rate').value,
        startdate = document.getElementById('startdate').value,
        totaltime = document.getElementById('totaltime').value,
        tid = document.getElementById('id').value;
    var dataString = "&stationid="+station+"&segment="+segment+"&starttime="+starttime+"&endtime="+endtime+"&los="+los+"&rate="+rate+"&startdate="+startdate+"&totaltime="+totaltime+"&id="+tid+"&action=edit";
    $.ajax({
        url: "processing/Admin.Processing.php",
        method: "post",
        data: dataString,
        success: function(response){
            //console.log(response);
            var result = response.split('~');
            $('.form-message-box').html(result[1]);
            if(result[0]==0) {
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        }
    });
};

//delete advert space action
function deleteAdvertSpace(id){
    if(confirm("Are you sure you want to delete this record?")) {
        var dataString = "id=" + id + "&action=delete";
        $.ajax({
            url: "processing/Admin.Processing.php",
            method: "post",
            data: dataString,
            success: function (response) {
                //console.log(response);
                var result = response.split('~');
                $('.message-box').html(result[1]);
                if (result[0] == 0) {
                    setTimeout(function () {
                        location.reload();
                    }, 3000);
                }
            }
        });
    }
}

//get edit details and display edit form
function showEditInsightForm(id) {
    $.ajax({
        url: "forms/Add.Edit.Insight.Form.php",
        data: "edit=" + id,
        type: "POST",
        success: function (html) {
            $('.edit-form').html(html);
        }
    });
}

//delete advert space action
function deleteInsight(id){
    if(confirm("Are you sure you want to delete this record?")) {
        var dataString = "id=" + id + "&delete";
        $.ajax({
            url: "processing/Admin.Processing.php",
            method: "post",
            data: dataString,
            success: function (response) {
                var result = response.split('~');
                $('.message-box').html(result[1]);
                if (result[0] == 0) {
                    setTimeout(function () {
                        //console.log(result[0]);
                        location.reload();
                    }, 3000);
                }
            }
        });
    }
}


function showAdvert(file, folder){
    //var file = $(this).attr();
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    file = file.replace("~", "'");
    folder = folder+file;
    $('.advert-title-display').html(file);
    if(extension == 'mp4'){
        $('.display-advert').html(' <video  style="width:100% !important;"  controls="controls" id="audio_player">' +
            '<source src="'+folder+'" type="video/mp4">' +
        'Your browser does not support the audio element. </video>');
    }else if(extension == 'mp3'){
        $('.display-advert').html(' <audio  style="width:100% !important;"  controls="controls" id="audio_player">' +
            '<source src="'+folder+'" type="audio/mpeg">' +
            'Your browser does not support the audio element. </audio>');
    }else{
        $('.display-advert').html('<img class="advert-image" style="width:100% !important;" src="'+folder+'" />');
    }
    //console.log(extension + ' this is it');
}
