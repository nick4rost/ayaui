<html dir="ltr" lang="en"><head>
    <meta charset="UTF-8">
    <title>AYA - Smart Media Buy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="font/iconsmind-s/css/iconsminds.css">
    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/vendor/bootstrap.rtl.only.min.css">
    <link rel="stylesheet" href="css/vendor/fullcalendar.min.css">
    <link rel="stylesheet" href="css/vendor/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/datatables.responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/select2.min.css">
    <link rel="stylesheet" href="css/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="css/vendor/glide.core.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-stars.css">
    <link rel="stylesheet" href="css/vendor/nouislider.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="css/vendor/component-custom-switch.min.css">
    <link rel="stylesheet" type="text/css" href="css/dore.light.blue.min.css"><link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/custom.css">
    <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>

<body id="app-container" class="ltr flat menu-default" cz-shortcut-listen="true">
<nav class="navbar fixed-top" style="opacity: 1;">
    <div class="d-flex align-items-center navbar-left">
        <a href="#" class="menu-button d-none d-md-block">
            <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                <rect x="0.48" y="0.5" width="7" height="1"></rect>
                <rect x="0.48" y="7.5" width="7" height="1"></rect>
                <rect x="0.48" y="15.5" width="7" height="1"></rect>
            </svg>
            <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                <rect x="1.56" y="0.5" width="16" height="1"></rect>
                <rect x="1.56" y="7.5" width="16" height="1"></rect>
                <rect x="1.56" y="15.5" width="16" height="1"></rect>
            </svg>
        </a>

        <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                <rect x="0.5" y="0.5" width="25" height="1"></rect>
                <rect x="0.5" y="7.5" width="25" height="1"></rect>
                <rect x="0.5" y="15.5" width="25" height="1"></rect>
            </svg>
        </a>

        <a class="navbar-logo" href="Admin.Dashboard.Default.html">
            <span class="logo d-none d-xs-block"></span>
            <span class="logo-mobile d-block d-xs-none"></span>
        </a>

    </div>

    <div class="navbar-right">

        <div class="user d-inline-block">
            <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="name">admin</span>
                    <span>
                        <img alt="Profile Picture" src="img/profile-pic-l.jpg">
                    </span>
            </button>

            <div class="dropdown-menu dropdown-menu-right mt-3">
                <a class="dropdown-item" href="processing/logout.php">Sign out</a>
            </div>
        </div>
    </div>
</nav>
<div class="menu" style="opacity: 1;">
    <script>
        var index = window.location.href.lastIndexOf("/") + 1;
        var filenameWithExtension = window.location.href.substr(index);
        var page = filenameWithExtension.split(".")[0]; // <-- added this line
        var role =0

        if(role == 4 && page != "Client"){
            window.location.href = "Client.Dashboard.Default.php";
        }else if(role == 5 && page != "Media"){
            window.location.href = "Media.Dashboard.Default.php";
        }else if(role != 0 && role != 4 && role != 5){
            window.location.href = "processing/logout.php";
        }
    </script>
    <div class="main-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled">
                <!--                admin menu-->
                <li id="Admin">
                    <a href="#dashboard">
                        <i class="iconsminds-administrator"></i>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
                <!--                client menu-->
                <li id="Client" class="active">
                    <a href="#client">
                        <i class="iconsminds-business-man-woman"></i>
                        <span>Client Dashboard</span>
                    </a>
                </li>
                <!--                media menu-->
                <li id="Media">
                    <a href="#media">
                        <i class="iconsminds-building"></i>
                        <span>Media House Dashboard</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div>

    <div class="sub-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled" data-link="dashboard">
                <li>
                    <a href="Admin.Dashboard.Default.php">
                        <i class="simple-icon-book-open"></i><span class="d-inline-block">Advert Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Bookings.php">
                        <i class="simple-icon-calendar"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Monitored.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Monitored</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Insight.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Media Insight</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Social.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="client" style="display: block;">
                <li>
                    <a href="Client.Dashboard.Default.php">
                        <i class="iconsminds-dashboard"></i><span class="d-inline-block">My Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Library.php">
                        <i class="simple-icon-playlist"></i><span class="d-inline-block">Adverts Library</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Details.php">
                        <i class="iconsminds-letter-open"></i><span class="d-inline-block">Advert Details</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Booked.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Booked Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Social.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="media">
                <li>
                    <a href="Media.Dashboard.Default.php">
                        <i class="simple-icon-event"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Media.Dashboard.Report.php">
                        <i class="simple-icon-chart"></i><span class="d-inline-block">Reports</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div></div>

<main style="opacity: 1;" class="default-transition">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Buy Advert Space</h1>
                <div class="separator mb-5"></div>
            </div>

            <div class="col-12 mb-3 data-search ">
                <div class="col-sm-12 text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForm();">Buy Advert Space</button>
                </div>
                <div class="col-sm-12 mt-2 message-box">
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-12 mb-4">
                <div class="card">
                    <div class="card-body main-display">
                        <h5>Payment Details Page</h5>
                        <h5>Total Amount:<strong style="color:#148020;"> GHC 1100</strong></h5><br>    <!--payment by Mobile money-->
                        <h5>Pay through Mobile Money</h5>
                        <label>Voucher Code</label>
                        <strong>7789</strong><br>
                        <label>Phone number</label>
                        <strong>0201023457</strong><br><br>
                        <div class="container"><button class="row btn btn-sm btn-success" type="button" id="confirm" name="confirm" onclick="confirmPayment(369836,1100,'Radio', 'Mobile Money')">Continue to confirm</button></div><br>

                    </div>
                </div>

                <!-- Add New Advert Space Modal -->
                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel2">Buy Advert Space</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body buy-advert">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<footer class="page-footer" style="opacity: 1;">
    <div class="footer-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="mb-0 text-muted">ColoredStrategies 2019</p>
                </div>
                <div class="col-sm-6 d-none d-sm-block">
                    <ul class="breadcrumb pt-0 pr-0 float-right">
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Review</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Purchase</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Docs</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/vendor/jquery-3.3.1.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/Chart.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/chartjs-plugin-datalabels.js" style="opacity: 1;"></script>
<script src="js/vendor/moment.min.js" style="opacity: 1;"></script>
<script src="js/vendor/fullcalendar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/datatables.min.js" style="opacity: 1;"></script>
<script src="js/vendor/perfect-scrollbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/progressbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/jquery.barrating.min.js" style="opacity: 1;"></script>
<script src="js/vendor/select2.full.js" style="opacity: 1;"></script>
<script src="js/vendor/nouislider.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap-datepicker.js" style="opacity: 1;"></script>
<script src="js/vendor/Sortable.js" style="opacity: 1;"></script>
<script src="js/vendor/mousetrap.min.js" style="opacity: 1;"></script>
<script src="js/vendor/glide.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/dataTables.buttons.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.flash.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.html5.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.print.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/JSZip/jszip.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/pdfmake.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/vfs_fonts.js" style="opacity: 1;"></script>
<script src="js/dore.script.js" style="opacity: 1;"></script>
<script src="js/scripts.js" style="opacity: 1;"></script><div class="theme-colors default-transition" style="opacity: 1;"><div class="p-4"><p class="text-muted mb-2">Light Theme</p><div class="d-flex flex-row justify-content-between mb-4"><a href="#" data-theme="dore.light.blue.min.css" class="theme-color theme-color-blue active"></a><a href="#" data-theme="dore.light.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.light.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.light.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.light.red.min.css" class="theme-color theme-color-red"></a></div><p class="text-muted mb-2">Dark Theme</p><div class="d-flex flex-row justify-content-between"><a href="#" data-theme="dore.dark.blue.min.css" class="theme-color theme-color-blue"></a><a href="#" data-theme="dore.dark.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.dark.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.dark.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.dark.red.min.css" class="theme-color theme-color-red"></a></div></div><div class="p-4"><p class="text-muted mb-2">Border Radius</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="roundedRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="rounded"><label class="custom-control-label" for="roundedRadio">Rounded</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="flatRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="flat" checked="checked"><label class="custom-control-label" for="flatRadio">Flat</label></div></div><div class="p-4"><p class="text-muted mb-2">Direction</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="ltrRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="ltr" checked="checked"><label class="custom-control-label" for="ltrRadio">Ltr</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="rtlRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="rtl"><label class="custom-control-label" for="rtlRadio">Rtl</label></div></div><a href="#" class="theme-button"> <i class="simple-icon-magic-wand"></i> </a></div>
<script src="js/client.custom.js" style="opacity: 1;"></script>



</body></html>