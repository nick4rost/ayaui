<html dir="ltr" lang="en"><head>
    <meta charset="UTF-8">
    <title>AYA - Smart Media Buy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="font/iconsmind-s/css/iconsminds.css">
    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/vendor/bootstrap.rtl.only.min.css">
    <link rel="stylesheet" href="css/vendor/fullcalendar.min.css">
    <link rel="stylesheet" href="css/vendor/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/datatables.responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/select2.min.css">
    <link rel="stylesheet" href="css/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="css/vendor/glide.core.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-stars.css">
    <link rel="stylesheet" href="css/vendor/nouislider.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="css/vendor/component-custom-switch.min.css">
    <link rel="stylesheet" type="text/css" href="css/dore.light.blue.min.css"><link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/custom.css">
    <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>

<body id="app-container" class="menu-default ltr flat" cz-shortcut-listen="true">
<nav class="navbar fixed-top" style="opacity: 1;">
    <div class="d-flex align-items-center navbar-left">
        <a href="#" class="menu-button d-none d-md-block">
            <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                <rect x="0.48" y="0.5" width="7" height="1"></rect>
                <rect x="0.48" y="7.5" width="7" height="1"></rect>
                <rect x="0.48" y="15.5" width="7" height="1"></rect>
            </svg>
            <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                <rect x="1.56" y="0.5" width="16" height="1"></rect>
                <rect x="1.56" y="7.5" width="16" height="1"></rect>
                <rect x="1.56" y="15.5" width="16" height="1"></rect>
            </svg>
        </a>

        <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                <rect x="0.5" y="0.5" width="25" height="1"></rect>
                <rect x="0.5" y="7.5" width="25" height="1"></rect>
                <rect x="0.5" y="15.5" width="25" height="1"></rect>
            </svg>
        </a>

        <a class="navbar-logo" href="Admin.Dashboard.Default.html">
            <span class="logo d-none d-xs-block"></span>
            <span class="logo-mobile d-block d-xs-none"></span>
        </a>

    </div>

    <div class="navbar-right">

        <div class="user d-inline-block">
            <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="name">admin</span>
                    <span>
                        <img alt="Profile Picture" src="img/profile-pic-l.jpg">
                    </span>
            </button>

            <div class="dropdown-menu dropdown-menu-right mt-3">
                <a class="dropdown-item" href="processing/logout.php">Sign out</a>
            </div>
        </div>
    </div>
</nav>
<div class="menu" style="opacity: 1;">
    <script>
        var index = window.location.href.lastIndexOf("/") + 1;
        var filenameWithExtension = window.location.href.substr(index);
        var page = filenameWithExtension.split(".")[0]; // <-- added this line
        var role =0

        if(role == 4 && page != "Client"){
            window.location.href = "Client.Dashboard.Default.php";
        }else if(role == 5 && page != "Media"){
            window.location.href = "Media.Dashboard.Default.php";
        }else if(role != 0 && role != 4 && role != 5){
            window.location.href = "processing/logout.php";
        }
    </script>
    <div class="main-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled">
                <!--                admin menu-->
                <li id="Admin" class="active">
                    <a href="#dashboard">
                        <i class="iconsminds-administrator"></i>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
                <!--                client menu-->
                <li id="Client">
                    <a href="#client">
                        <i class="iconsminds-business-man-woman"></i>
                        <span>Client Dashboard</span>
                    </a>
                </li>
                <!--                media menu-->
                <li id="Media">
                    <a href="#media">
                        <i class="iconsminds-building"></i>
                        <span>Media House Dashboard</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div>

    <div class="sub-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled" data-link="dashboard" style="display: block;">
                <li>
                    <a href="Admin.Dashboard.Default.php">
                        <i class="simple-icon-book-open"></i><span class="d-inline-block">Advert Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Bookings.php">
                        <i class="simple-icon-calendar"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Monitored.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Monitored</span>
                    </a>
                </li>
                <li class="active">
                    <a href="Admin.Dashboard.Insight.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Media Insight</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Social.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="client">
                <li>
                    <a href="Client.Dashboard.Default.php">
                        <i class="iconsminds-dashboard"></i><span class="d-inline-block">My Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Library.php">
                        <i class="simple-icon-playlist"></i><span class="d-inline-block">Adverts Library</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Details.php">
                        <i class="iconsminds-letter-open"></i><span class="d-inline-block">Advert Details</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Booked.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Booked Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Social.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="media">
                <li>
                    <a href="Media.Dashboard.Default.php">
                        <i class="simple-icon-event"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Media.Dashboard.Report.php">
                        <i class="simple-icon-chart"></i><span class="d-inline-block">Reports</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div></div>

<main style="opacity: 1;" class="default-transition">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class=" d-none d-sm-block d-lg-inline-block">
                    <h1>Media Insights</h1>
                </div>
                <div class=" d-none d-sm-block d-lg-inline-block" style="float: right;">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">New Insight</button>
                </div>
                <!--                <div class="separator mb-1"></div>-->
            </div>

            <div class="col-sm-12 mt-2 mb-2 message-box">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-12 mb-4">
                <div class="card">
                    <div class="card-body">
<!--                        <div id="example_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row view-filter"><div class="col-sm-12 mb-2"><div class="float-right"><div class="dt-buttons btn-group">          <button class="btn btn-secondary buttons-copy buttons-html5" tabindex="0" aria-controls="example"><span>Copy</span></button> <button class="btn btn-secondary buttons-excel buttons-html5" tabindex="0" aria-controls="example"><span>Excel</span></button> <button class="btn btn-secondary buttons-csv buttons-html5" tabindex="0" aria-controls="example"><span>CSV</span></button> <button class="btn btn-secondary buttons-pdf buttons-html5" tabindex="0" aria-controls="example"><span>PDF</span></button> <button class="btn btn-secondary buttons-print" tabindex="0" aria-controls="example"><span>Print</span></button> </div></div></div><div class="col-sm-12"><div class="float-right"><div class="dataTables_length" id="example_length"><label>Items Per Page <select name="example_length" aria-controls="example" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select></label></div></div><div class="float-left"><div id="example_filter" class="dataTables_filter"><label><input type="search" class="form-control form-control-sm" placeholder="Search..." aria-controls="example"></label></div></div><div class="clearfix"></div></div></div>-->
                            <table id="example" class="data-table table-striped data-table-admin-insight dataTable no-footer" role="grid" aria-describedby="example_info">
                                <thead>
                                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 56.4833px;" aria-sort="ascending" aria-label="No.: activate to sort column descending">No.</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 95.3px;" aria-label="Station: activate to sort column ascending">Station</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 436.45px;" aria-label="Insight: activate to sort column ascending">Insight</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 270.967px;" aria-label="Action: activate to sort column ascending">Action</th></tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd"><td class="sorting_1">1</td><td>TV3</td><td>No.1 Station for all your advertising needs.</td><td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditInsightForm(4)" ;="">Edit</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteInsight(4);">Delete</button></td></tr></tbody>
                            </table>
<!--                        <div class="row view-pager"><div class="col-sm-12"><div class="text-center"><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination pagination-sm"><li class="paginate_button page-item previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link prev"><i class="simple-icon-arrow-left"></i></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item next disabled" id="example_next"><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link next"><i class="simple-icon-arrow-right"></i></a></li></ul></div></div></div></div></div>-->
<!--                    </div>-->
                </div>

                <!-- Edit Advert Space Modal -->
                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel1">Edit insight</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body edit-form">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Add New Advert Space Modal -->
                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel2">Add New Insight</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="form-view" id="form-view">
                                    <div class="login-content">
                                        <div class="login-form">
                                            <form method="post" action="processing/Admin.Processing.php" enctype="multipart/form-data" class="form-horizontal">
                                                <div class="form-elements">
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="text-input" class="">Ad Type</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <select id="mid" name="mid" class="form-control" required="">
                                                                <option selected="" disabled="">Select A Station</option>
                                                                <option value="1">TV3 </option><option value="3">Metro TV </option><option value="4">GHOne</option><option value="5">AdomTV </option><option value="6">GTV </option><option value="7">UTV </option><option value="9">AdomFM </option><option value="10">PeaceFM </option><option value="11">JoyFM </option><option value="12">OkayFM </option><option value="13">KasapaFM</option><option value="14">HitzFM </option><option value="15">SweetMelodiesFM </option><option value="16">YFM </option><option value="17">StarrFM </option><option value="18">CitiFM </option><option value="19">AsempaFM </option><option value="20">OmanFM </option><option value="21">TVAfrica </option><option value="22">Vision1FM </option><option value="23">Kwese Sports</option><option value="24">Ksi-YFM </option><option value="25">MelodyFM </option><option value="26">RadioMaxxFM</option><option value="27">RadioSilverFM</option><option value="28">SkyPowerFM </option><option value="29">AngelFM </option><option value="30">KessbenFM </option><option value="31">LuvFM </option><option value="32">NhyiraFM </option><option value="33">JoyPrime </option><option value="34">Radio JusticeFM</option><option value="35">Zaa RadioFM</option><option value="36">DiamondFM</option><option value="37">Savannah RadioFM</option><option value="38">RadioTamaleFM</option><option value="39">NorthStar RadioFM</option><option value="41">Rainbow RadioFM</option><option value="42">GreenFM</option><option value="43">ObonuFM</option><option value="44">Atlantis RadioFM</option><option value="45">Channel R FM</option><option value="46">Choice FM</option><option value="47">Great FM</option><option value="48">Happy FM</option><option value="49">Hot FM</option><option value="50">Pluzz FM</option><option value="51">Prime FM</option><option value="52">Radio Gold FM</option><option value="53">Sena Radio FM</option><option value="54">Sunny FM</option><option value="55">Sweet Melodies FM</option><option value="56">Top Radio FM</option><option value="57">Uniiq FM</option><option value="58">Vibe FM</option><option value="59">XFM</option><option value="60">Radio XYZ FM</option><option value="61">Radio Ada FM</option><option value="62">Agoo FM</option><option value="63">Pravda Radio FM</option><option value="64">Anigye FM</option><option value="65">Ashh FM</option><option value="66">Boss FM</option><option value="67">Fox FM</option><option value="68">Freedom FM</option><option value="69">Focus FM</option><option value="70">Jubilee Radio FM</option><option value="71">Light FM</option><option value="72">Holy FM</option><option value="73">Victory FM</option><option value="74">Hope FM</option><option value="75">Radio Upper West</option><option value="76">Radio Freed FM</option><option value="77">Radio Progress FM</option><option value="78">Radford FM</option><option value="79">A1 Radio FM</option><option value="80">Word FM</option><option value="81">Radio Gurune FM</option><option value="82">Rock FM</option><option value="83">Anapua FM</option><option value="84">Star FM</option><option value="85">Space FM</option><option value="86">Dinpa FM</option><option value="87">Sky FM</option><option value="88">Sompa FM</option><option value="89">Pink FM</option><option value="90">Yes FM</option><option value="91">Spark FM</option><option value="92">Solar FM</option><option value="93">Eastern FM</option><option value="94">Vision FM</option><option value="95">Sunrise FM</option><option value="96">Goodlife FM</option><option value="97">Emak FM</option><option value="98">Rok FM</option><option value="99">AngelTV</option><option value="100">ClassFM</option><option value="101">Angel FM</option><option value="106">WAAZOBIA FM ABUJA</option><option value="107">WAAZOBIA FM LAGOS</option><option value="108">WAAZOBIA FM PORT HARCOURT </option><option value="109">INSPIRATION RADIO</option><option value="110">KAPITAL RADIO</option><option value="111">FREEDOM RADIO</option><option value="112">SPLASH FM</option><option value="113">ROCKCITY FM</option><option value="114">INDEPENDENT RADIO</option><option value="115">LiveFM</option><option value="116">AbusuaFM</option><option value="117">4SyteTV</option><option value="118">ConnectFM</option><option value="119">EmpireFM</option>                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-4">
                                                            <label for="file-input" class="">Insight</label>
                                                        </div>
                                                        <div class="col-12 col-md-8">
                                                            <textarea class="form-control" id="insight" name="insight" required=""></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-footer text-right border-top pt-3">
                                                    <button type="submit" class="btn btn-primary" id="submit" name="submit" style="display: inline-block; "> Submit </button>
                                                    <button type="reset" class="btn btn-warning" style=" display: inline-block; ">
                                                        <i class="fa fa-refresh"></i> Reset
                                                    </button>
                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger" style="display: inline-block; ">
                                                        <i class="fa fa-close"></i> Cancel
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<footer class="page-footer" style="opacity: 1;">
    <div class="footer-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="mb-0 text-muted">ColoredStrategies 2019</p>
                </div>
                <div class="col-sm-6 d-none d-sm-block">
                    <ul class="breadcrumb pt-0 pr-0 float-right">
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Review</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Purchase</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Docs</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/vendor/jquery-3.3.1.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/Chart.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/chartjs-plugin-datalabels.js" style="opacity: 1;"></script>
<script src="js/vendor/moment.min.js" style="opacity: 1;"></script>
<script src="js/vendor/fullcalendar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/datatables.min.js" style="opacity: 1;"></script>
<script src="js/vendor/perfect-scrollbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/progressbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/jquery.barrating.min.js" style="opacity: 1;"></script>
<script src="js/vendor/select2.full.js" style="opacity: 1;"></script>
<script src="js/vendor/nouislider.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap-datepicker.js" style="opacity: 1;"></script>
<script src="js/vendor/Sortable.js" style="opacity: 1;"></script>
<script src="js/vendor/mousetrap.min.js" style="opacity: 1;"></script>
<script src="js/vendor/glide.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/dataTables.buttons.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.flash.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.html5.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.print.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/JSZip/jszip.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/pdfmake.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/vfs_fonts.js" style="opacity: 1;"></script>
<script src="js/dore.script.js" style="opacity: 1;"></script>
<script src="js/scripts.js" style="opacity: 1;"></script><div class="theme-colors default-transition" style="opacity: 1;"><div class="p-4"><p class="text-muted mb-2">Light Theme</p><div class="d-flex flex-row justify-content-between mb-4"><a href="#" data-theme="dore.light.blue.min.css" class="theme-color theme-color-blue active"></a><a href="#" data-theme="dore.light.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.light.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.light.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.light.red.min.css" class="theme-color theme-color-red"></a></div><p class="text-muted mb-2">Dark Theme</p><div class="d-flex flex-row justify-content-between"><a href="#" data-theme="dore.dark.blue.min.css" class="theme-color theme-color-blue"></a><a href="#" data-theme="dore.dark.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.dark.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.dark.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.dark.red.min.css" class="theme-color theme-color-red"></a></div></div><div class="p-4"><p class="text-muted mb-2">Border Radius</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="roundedRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="rounded"><label class="custom-control-label" for="roundedRadio">Rounded</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="flatRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="flat" checked="checked"><label class="custom-control-label" for="flatRadio">Flat</label></div></div><div class="p-4"><p class="text-muted mb-2">Direction</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="ltrRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="ltr" checked="checked"><label class="custom-control-label" for="ltrRadio">Ltr</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="rtlRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="rtl"><label class="custom-control-label" for="rtlRadio">Rtl</label></div></div><a href="#" class="theme-button"> <i class="simple-icon-magic-wand"></i> </a></div>
<script src="js/admin.custom.js" style="opacity: 1;"></script>


<!--advert spaces table default display-->
<script style="opacity: 1;">
    /**
     * Created by Elorm on 2/19/2020.
     */
    $(document).ready(function() {

        $.ajax({
            url: "forms/Add.Edit.Insight.Form.php",
            type: "GET",
            success: function (html) {
                $('.add-insight').html(html);
            }
        });
    })
</script>


</body></html>