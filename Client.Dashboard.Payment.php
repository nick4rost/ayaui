<html dir="ltr" lang="en"><head>
    <meta charset="UTF-8">
    <title>AYA - Smart Media Buy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="font/iconsmind-s/css/iconsminds.css">
    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/vendor/bootstrap.rtl.only.min.css">
    <link rel="stylesheet" href="css/vendor/fullcalendar.min.css">
    <link rel="stylesheet" href="css/vendor/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/datatables.responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/select2.min.css">
    <link rel="stylesheet" href="css/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="css/vendor/glide.core.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-stars.css">
    <link rel="stylesheet" href="css/vendor/nouislider.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="css/vendor/component-custom-switch.min.css">
    <link rel="stylesheet" type="text/css" href="css/dore.light.blue.min.css"><link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/custom.css">
    <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>

<body id="app-container" class="ltr flat menu-default" cz-shortcut-listen="true">
<nav class="navbar fixed-top" style="opacity: 1;">
    <div class="d-flex align-items-center navbar-left">
        <a href="#" class="menu-button d-none d-md-block">
            <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                <rect x="0.48" y="0.5" width="7" height="1"></rect>
                <rect x="0.48" y="7.5" width="7" height="1"></rect>
                <rect x="0.48" y="15.5" width="7" height="1"></rect>
            </svg>
            <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                <rect x="1.56" y="0.5" width="16" height="1"></rect>
                <rect x="1.56" y="7.5" width="16" height="1"></rect>
                <rect x="1.56" y="15.5" width="16" height="1"></rect>
            </svg>
        </a>

        <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                <rect x="0.5" y="0.5" width="25" height="1"></rect>
                <rect x="0.5" y="7.5" width="25" height="1"></rect>
                <rect x="0.5" y="15.5" width="25" height="1"></rect>
            </svg>
        </a>

        <a class="navbar-logo" href="Admin.Dashboard.Default.html">
            <span class="logo d-none d-xs-block"></span>
            <span class="logo-mobile d-block d-xs-none"></span>
        </a>

    </div>

    <div class="navbar-right">

        <div class="user d-inline-block">
            <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="name">admin</span>
                    <span>
                        <img alt="Profile Picture" src="img/profile-pic-l.jpg">
                    </span>
            </button>

            <div class="dropdown-menu dropdown-menu-right mt-3">
                <a class="dropdown-item" href="processing/logout.php">Sign out</a>
            </div>
        </div>
    </div>
</nav>
<div class="menu" style="opacity: 1;">
    <script>
        var index = window.location.href.lastIndexOf("/") + 1;
        var filenameWithExtension = window.location.href.substr(index);
        var page = filenameWithExtension.split(".")[0]; // <-- added this line
        var role =0

        if(role == 4 && page != "Client"){
            window.location.href = "Client.Dashboard.Default.php";
        }else if(role == 5 && page != "Media"){
            window.location.href = "Media.Dashboard.Default.php";
        }else if(role != 0 && role != 4 && role != 5){
            window.location.href = "processing/logout.php";
        }
    </script>
    <div class="main-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled">
                <!--                admin menu-->
                <li id="Admin">
                    <a href="#dashboard">
                        <i class="iconsminds-administrator"></i>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
                <!--                client menu-->
                <li id="Client" class="active">
                    <a href="#client">
                        <i class="iconsminds-business-man-woman"></i>
                        <span>Client Dashboard</span>
                    </a>
                </li>
                <!--                media menu-->
                <li id="Media">
                    <a href="#media">
                        <i class="iconsminds-building"></i>
                        <span>Media House Dashboard</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div>

    <div class="sub-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled" data-link="dashboard">
                <li>
                    <a href="Admin.Dashboard.Default.php">
                        <i class="simple-icon-book-open"></i><span class="d-inline-block">Advert Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Bookings.php">
                        <i class="simple-icon-calendar"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Monitored.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Monitored</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Insight.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Media Insight</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Social.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="client" style="display: block;">
                <li>
                    <a href="Client.Dashboard.Default.php">
                        <i class="iconsminds-dashboard"></i><span class="d-inline-block">My Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Library.php">
                        <i class="simple-icon-playlist"></i><span class="d-inline-block">Adverts Library</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Details.php">
                        <i class="iconsminds-letter-open"></i><span class="d-inline-block">Advert Details</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Booked.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Booked Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Social.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="media">
                <li>
                    <a href="Media.Dashboard.Default.php">
                        <i class="simple-icon-event"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Media.Dashboard.Report.php">
                        <i class="simple-icon-chart"></i><span class="d-inline-block">Reports</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div></div>

<main style="opacity: 1;" class="default-transition">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Buy Advert Space</h1>
                <div class="separator mb-5"></div>
            </div>

            <div class="col-12 mb-3 data-search ">
                <div class="col-sm-12 text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForm();">Buy Advert Space</button>
                </div>
                <div class="col-sm-12 mt-2 message-box">
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-12 mb-4">
                <div class="card">
                    <div class="card-body main-display"><h4>Purchase Summary for advert:  Kwachua mamili</h4><table class="table table-striped table-bordered summarytbl mt-2"><thead class="header"><tr><th>Station</th><th>Start Date</th><th>Segment</th><th>Ad duration(seconds)</th><th>Ad Rate(GHC) per purchase</th></tr></thead><tbody><tr><td>Happy FM</td><td>2020-04-21</td><td>00:00:00 - 01:00:00</td><td>60</td><td>371.00</td></tr><tr><td>Solar FM</td><td>2020-04-21</td><td>00:30:00 - 01:00:00</td><td>45</td><td>170.00</td></tr><tr><td>Pink FM</td><td>2020-04-21</td><td>04:00:00 - 05:00:00</td><td>45</td><td>248.00</td></tr><tr><td>Anapua FM</td><td>2020-04-21</td><td>09:30:00 - 10:30:00</td><td>45</td><td>100.00</td></tr><tr><td>Goodlife FM</td><td>2020-04-25</td><td>01:00:00 - 02:00:00</td><td>45</td><td>211.00</td></tr><tr><td></td><td></td><td></td><td><b>Total</b></td><td><b>1,100.00</b></td></tr></tbody></table><a href="processing/Client.Processing.php?confirm-purchase=Kwachua+mamili/7498/8483/8525/8373/8687/&amp;monitor=1&amp;total=1100" class="confirm-purchase btn btn-sm btn-danger" style="color: #fff;">Confirm Purchase</a></div>
                </div>

                <!-- Add New Advert Space Modal -->
                <div class="modal fade show" id="exampleModal2" tabindex="-1" role="dialog" style="display: none;" aria-modal="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel2">Buy Advert Space</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body buy-advert"><form method="POST" action="client-dashboard.php?client-summary"><div class="select-ad"><select class="form-control" style="width: 350px;" id="select-ad" name="select-ad" required=""><option value="" selected="" disabled="">Select an advert</option><option value="Kwachua mamili">Kwachua mamili</option><option value="cola mango">cola mango</option><option value="File Sample">File Sample</option><option value="my awesome ad again">my awesome ad again</option><option value="Wild Wild West">Wild Wild West</option><option value="All My Life">All My Life</option><option value="werer">werer</option><option value="werer">werer</option></select><br></div><div class="select-mediatype">
                                        <select class="form-control" style="width: 350px;" id="select-mt" name="select-mt" onchange="getMtResult();"><option value="" selected="" disabled="">Select a media type</option><option value="TV">TV</option><option value="Radio">Radio</option></select><br></div><div class="select-region" style="display: block;"><h4 class="regionsLabel">Select a region</h4><div class="custom-control custom-checkbox regions-list mb-2">
                                            <input type="checkbox" class="custom-control-input" id="all-regions" name="select-region" value="1" onclick="getAllRegions();" checked="">
                                            <label class="custom-control-label" for="all-regions">ALL REGIONS</label>
                                        </div><div class="regions-list-wrap" style="display: none;"><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="GREATERACCRAREGION" name="select-region" value="GREATER ACCRA REGION" style="display: block;">
                                                <label class="custom-control-label" for="GREATERACCRAREGION">GREATER ACCRA REGION</label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="ASHANTIREGION" name="select-region" value="ASHANTI REGION" style="display: block;">
                                                <label class="custom-control-label" for="ASHANTIREGION">ASHANTI REGION</label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="WESTERNREGION" name="select-region" value="WESTERN REGION" style="display: block;">
                                                <label class="custom-control-label" for="WESTERNREGION">WESTERN REGION</label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="NORTHERNREGION" name="select-region" value="NORTHERN REGION " style="display: block;">
                                                <label class="custom-control-label" for="NORTHERNREGION">NORTHERN REGION </label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="VOLTAREGION" name="select-region" value="VOLTA REGION" style="display: block;">
                                                <label class="custom-control-label" for="VOLTAREGION">VOLTA REGION</label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="UPPERWESTREGION" name="select-region" value="UPPER WEST REGION" style="display: block;">
                                                <label class="custom-control-label" for="UPPERWESTREGION">UPPER WEST REGION</label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="UPPEREASTREGION" name="select-region" value="UPPER EAST REGION" style="display: block;">
                                                <label class="custom-control-label" for="UPPEREASTREGION">UPPER EAST REGION</label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="BRONG-AHAFOREGION" name="select-region" value="BRONG-AHAFO REGION" style="display: block;">
                                                <label class="custom-control-label" for="BRONG-AHAFOREGION">BRONG-AHAFO REGION</label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="CENTRALREGION" name="select-region" value="CENTRAL REGION" style="display: block;">
                                                <label class="custom-control-label" for="CENTRALREGION">CENTRAL REGION</label>
                                            </div><div class="custom-control custom-checkbox regions-list col-lg-6 col-sm-12">
                                                <input type="checkbox" class="custom-control-input select-region" id="EASTERNREGION" name="select-region" value="EASTERN REGION" style="display: block;">
                                                <label class="custom-control-label" for="EASTERNREGION">EASTERN REGION</label>
                                            </div><div class="regionsbtn btn  btn-outline-primary mt-2 text-right" onclick="getStation2()">Fetch</div></div><br></div><div class="radio-tv"><div class="select-station"><select class="form-control" style="width: 350px;" id="select-station" name="select-station" onchange="getCalendar();"><option value="" selected="" disabled="">Select a station</option><option value="0">View All</option><option value="51">Prime FM</option><option value="46">Choice FM</option><option value="48">Happy FM</option><option value="19">AsempaFM </option><option value="15">SweetMelodiesFM </option><option value="31">LuvFM </option><option value="30">KessbenFM </option><option value="65">Ashh FM</option><option value="67">Fox FM</option><option value="66">Boss FM</option><option value="98">Rok FM</option><option value="27">RadioSilverFM</option><option value="28">SkyPowerFM </option><option value="26">RadioMaxxFM</option><option value="25">MelodyFM </option><option value="38">RadioTamaleFM</option><option value="35">Zaa RadioFM</option><option value="36">DiamondFM</option><option value="39">NorthStar RadioFM</option><option value="37">Savannah RadioFM</option><option value="71">Light FM</option><option value="72">Holy FM</option><option value="74">Hope FM</option><option value="70">Jubilee Radio FM</option><option value="73">Victory FM</option><option value="76">Radio Freed FM</option><option value="78">Radford FM</option><option value="75">Radio Upper West</option><option value="77">Radio Progress FM</option><option value="80">Word FM</option><option value="79">A1 Radio FM</option><option value="82">Rock FM</option><option value="81">Radio Gurune FM</option><option value="86">Dinpa FM</option><option value="83">Anapua FM</option><option value="85">Space FM</option><option value="84">Star FM</option><option value="87">Sky FM</option><option value="91">Spark FM</option><option value="92">Solar FM</option><option value="89">Pink FM</option><option value="90">Yes FM</option><option value="88">Sompa FM</option><option value="97">Emak FM</option><option value="93">Eastern FM</option><option value="95">Sunrise FM</option><option value="96">Goodlife FM</option><option value="94">Vision FM</option><option value="12">OkayFM </option></select><br></div></div><div class="wait-loading mb-2"></div><div class="media-insight mb-2"></div><div class="calendar"><table id="tableID" class="calendar table table-bordered table-striped" width="100%" cellspacing="0" border="2"><caption><h3>April 2020 - 287 Available Time Slots</h3>
                                                <div class="adlegend" style="width: 100%; margin-bottom: 20px;">
                                                    <span style="background: #158DA6; color: #fff; padding:5px 10px; border-radius:5px; margin-right:10px; text-align: center;">TV</span>
                                                    <span style="background: #ffb318; color: #fff; padding:5px 10px; border-radius:5px;  text-align: center;">Radio</span>
                                                </div>
                                                <div class="error-msg" style="color: red; font-size: small;"></div></caption><tbody><tr><th class="header">Sun</th><th class="header">Mon</th><th class="header">Tue</th><th class="header">Wed</th><th class="header">Thu</th><th class="header">Fri</th><th class="header">Sat</th></tr><tr><td colspan="3">&nbsp;</td><td border="1" class="day" rel="2020-04-01"><span class="daydate">1</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-02"><span class="daydate">2</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-03"><span class="daydate">3</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-04"><span class="daydate">4</span><span class="info"></span> </td></tr><tr><td border="1" class="day" rel="2020-04-05"><span class="daydate">5</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-06"><span class="daydate">6</span><span class="info"></span> </td><td border="1" class="day available" rel="2020-04-07"><div class="popWrap" id="7" onclick="openThis(7);"><span class="daydate">7</span><span title="Available Radio Slots" class="open-button radio">16</span></div><div id="ad7"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7865" name="mytvslots[]" value="7865/RadioTamaleFM/2020-04-07/00:30:00 - 01:00:00">
                                                                            <label class="" for="advert7865">RadioTamaleFM</label></td> <td>2020-04-07</td>
                                                                        <td>00:30:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7865" class="selectslots" name="selectslots[]" onchange="getRate(7865);"><option disabled="" selected="">--select slot length--</option><option value="30/59/7865">30</option><option value="45/148/7866">45</option><option value="60/354/7867">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8012" name="mytvslots[]" value="8012/Holy FM/2020-04-07/01:00:00 - 01:30:00">
                                                                            <label class="" for="advert8012">Holy FM</label></td> <td>2020-04-07</td>
                                                                        <td>01:00:00 - 01:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8012" class="selectslots" name="selectslots[]" onchange="getRate(8012);"><option disabled="" selected="">--select slot length--</option><option value="30/34/8012">30</option><option value="45/156/8013">45</option><option value="60/455/8014">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8006" name="mytvslots[]" value="8006/Holy FM/2020-04-07/02:00:00 - 02:30:00">
                                                                            <label class="" for="advert8006">Holy FM</label></td> <td>2020-04-07</td>
                                                                        <td>02:00:00 - 02:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8006" class="selectslots" name="selectslots[]" onchange="getRate(8006);"><option disabled="" selected="">--select slot length--</option><option value="30/58/8006">30</option><option value="45/100/8007">45</option><option value="60/433/8008">60</option><option value="30/65/8623">30</option><option value="45/167/8624">45</option><option value="60/302/8625">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8348" name="mytvslots[]" value="8348/Dinpa FM/2020-04-07/04:30:00 - 05:30:00">
                                                                            <label class="" for="advert8348">Dinpa FM</label></td> <td>2020-04-07</td>
                                                                        <td>04:30:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8348" class="selectslots" name="selectslots[]" onchange="getRate(8348);"><option disabled="" selected="">--select slot length--</option><option value="30/71/8348">30</option><option value="45/146/8349">45</option><option value="60/281/8350">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7679" name="mytvslots[]" value="7679/Fox FM/2020-04-07/05:00:00 - 05:30:00">
                                                                            <label class="" for="advert7679">Fox FM</label></td> <td>2020-04-07</td>
                                                                        <td>05:00:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7679" class="selectslots" name="selectslots[]" onchange="getRate(7679);"><option disabled="" selected="">--select slot length--</option><option value="30/96/7679">30</option><option value="45/102/7680">45</option><option value="60/482/7681">60</option><option value="30/61/8432">30</option><option value="45/191/8433">45</option><option value="60/339/8434">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7499" name="mytvslots[]" value="7499/Happy FM/2020-04-07/06:00:00 - 06:30:00">
                                                                            <label class="" for="advert7499">Happy FM</label></td> <td>2020-04-07</td>
                                                                        <td>06:00:00 - 06:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7499" class="selectslots" name="selectslots[]" onchange="getRate(7499);"><option disabled="" selected="">--select slot length--</option><option value="30/30/7499">30</option><option value="45/212/7500">45</option><option value="60/354/7501">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8147" name="mytvslots[]" value="8147/Radford FM/2020-04-07/06:30:00 - 07:00:00">
                                                                            <label class="" for="advert8147">Radford FM</label></td> <td>2020-04-07</td>
                                                                        <td>06:30:00 - 07:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8147" class="selectslots" name="selectslots[]" onchange="getRate(8147);"><option disabled="" selected="">--select slot length--</option><option value="30/80/8147">30</option><option value="45/147/8148">45</option><option value="60/311/8149">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8096" name="mytvslots[]" value="8096/Victory FM/2020-04-07/11:30:00 - 12:00:00">
                                                                            <label class="" for="advert8096">Victory FM</label></td> <td>2020-04-07</td>
                                                                        <td>11:30:00 - 12:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8096" class="selectslots" name="selectslots[]" onchange="getRate(8096);"><option disabled="" selected="">--select slot length--</option><option value="30/98/8096">30</option><option value="45/194/8097">45</option><option value="60/490/8098">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8135" name="mytvslots[]" value="8135/Radio Freed FM/2020-04-07/12:00:00 - 13:00:00">
                                                                            <label class="" for="advert8135">Radio Freed FM</label></td> <td>2020-04-07</td>
                                                                        <td>12:00:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8135" class="selectslots" name="selectslots[]" onchange="getRate(8135);"><option disabled="" selected="">--select slot length--</option><option value="30/67/8135">30</option><option value="45/159/8136">45</option><option value="60/255/8137">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8491" name="mytvslots[]" value="8491/Solar FM/2020-04-07/13:00:00 - 13:30:00">
                                                                            <label class="" for="advert8491">Solar FM</label></td> <td>2020-04-07</td>
                                                                        <td>13:00:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8491" class="selectslots" name="selectslots[]" onchange="getRate(8491);"><option disabled="" selected="">--select slot length--</option><option value="30/45/8491">30</option><option value="45/129/8492">45</option><option value="60/281/8493">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8297" name="mytvslots[]" value="8297/Rock FM/2020-04-07/15:30:00 - 16:30:00">
                                                                            <label class="" for="advert8297">Rock FM</label></td> <td>2020-04-07</td>
                                                                        <td>15:30:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8297" class="selectslots" name="selectslots[]" onchange="getRate(8297);"><option disabled="" selected="">--select slot length--</option><option value="30/78/8297">30</option><option value="45/164/8298">45</option><option value="60/499/8299">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8276" name="mytvslots[]" value="8276/Rock FM/2020-04-07/16:00:00 - 17:00:00">
                                                                            <label class="" for="advert8276">Rock FM</label></td> <td>2020-04-07</td>
                                                                        <td>16:00:00 - 17:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8276" class="selectslots" name="selectslots[]" onchange="getRate(8276);"><option disabled="" selected="">--select slot length--</option><option value="30/56/8276">30</option><option value="45/239/8277">45</option><option value="60/477/8278">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8312" name="mytvslots[]" value="8312/Radio Gurune FM/2020-04-07/17:00:00 - 17:30:00">
                                                                            <label class="" for="advert8312">Radio Gurune FM</label></td> <td>2020-04-07</td>
                                                                        <td>17:00:00 - 17:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8312" class="selectslots" name="selectslots[]" onchange="getRate(8312);"><option disabled="" selected="">--select slot length--</option><option value="30/89/8312">30</option><option value="45/212/8313">45</option><option value="60/367/8314">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7535" name="mytvslots[]" value="7535/AsempaFM /2020-04-07/19:00:00 - 19:30:00">
                                                                            <label class="" for="advert7535">AsempaFM </label></td> <td>2020-04-07</td>
                                                                        <td>19:00:00 - 19:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7535" class="selectslots" name="selectslots[]" onchange="getRate(7535);"><option disabled="" selected="">--select slot length--</option><option value="30/95/7535">30</option><option value="45/125/7536">45</option><option value="60/412/7537">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8548" name="mytvslots[]" value="8548/Yes FM/2020-04-07/19:30:00 - 20:00:00">
                                                                            <label class="" for="advert8548">Yes FM</label></td> <td>2020-04-07</td>
                                                                        <td>19:30:00 - 20:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8548" class="selectslots" name="selectslots[]" onchange="getRate(8548);"><option disabled="" selected="">--select slot length--</option><option value="30/88/8548">30</option><option value="45/243/8549">45</option><option value="60/425/8550">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7619" name="mytvslots[]" value="7619/KessbenFM /2020-04-07/20:00:00 - 20:30:00">
                                                                            <label class="" for="advert7619">KessbenFM </label></td> <td>2020-04-07</td>
                                                                        <td>20:00:00 - 20:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7619" class="selectslots" name="selectslots[]" onchange="getRate(7619);"><option disabled="" selected="">--select slot length--</option><option value="30/51/7619">30</option><option value="45/246/7620">45</option><option value="60/464/7621">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-08"><div class="popWrap" id="8" onclick="openThis(8);"><span class="daydate">8</span><span title="Available Radio Slots" class="open-button radio">12</span></div><div id="ad8"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8509" name="mytvslots[]" value="8509/Pink FM/2020-04-08/01:30:00 - 02:00:00">
                                                                            <label class="" for="advert8509">Pink FM</label></td> <td>2020-04-08</td>
                                                                        <td>01:30:00 - 02:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8509" class="selectslots" name="selectslots[]" onchange="getRate(8509);"><option disabled="" selected="">--select slot length--</option><option value="30/50/8509">30</option><option value="45/220/8510">45</option><option value="60/321/8511">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7766" name="mytvslots[]" value="7766/SkyPowerFM /2020-04-08/03:00:00 - 03:30:00">
                                                                            <label class="" for="advert7766">SkyPowerFM </label></td> <td>2020-04-08</td>
                                                                        <td>03:00:00 - 03:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7766" class="selectslots" name="selectslots[]" onchange="getRate(7766);"><option disabled="" selected="">--select slot length--</option><option value="30/38/7766">30</option><option value="45/240/7767">45</option><option value="60/451/7768">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8444" name="mytvslots[]" value="8444/Sky FM/2020-04-08/04:30:00 - 05:30:00">
                                                                            <label class="" for="advert8444">Sky FM</label></td> <td>2020-04-08</td>
                                                                        <td>04:30:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8444" class="selectslots" name="selectslots[]" onchange="getRate(8444);"><option disabled="" selected="">--select slot length--</option><option value="30/63/8444">30</option><option value="45/130/8445">45</option><option value="60/252/8446">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7796" name="mytvslots[]" value="7796/RadioMaxxFM/2020-04-08/08:00:00 - 08:30:00">
                                                                            <label class="" for="advert7796">RadioMaxxFM</label></td> <td>2020-04-08</td>
                                                                        <td>08:00:00 - 08:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7796" class="selectslots" name="selectslots[]" onchange="getRate(7796);"><option disabled="" selected="">--select slot length--</option><option value="30/47/7796">30</option><option value="45/112/7797">45</option><option value="60/355/7798">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7886" name="mytvslots[]" value="7886/Zaa RadioFM/2020-04-08/08:30:00 - 09:00:00">
                                                                            <label class="" for="advert7886">Zaa RadioFM</label></td> <td>2020-04-08</td>
                                                                        <td>08:30:00 - 09:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7886" class="selectslots" name="selectslots[]" onchange="getRate(7886);"><option disabled="" selected="">--select slot length--</option><option value="30/74/7886">30</option><option value="45/109/7887">45</option><option value="60/345/7888">60</option><option value="30/47/8429">30</option><option value="45/248/8430">45</option><option value="60/410/8431">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7649" name="mytvslots[]" value="7649/Ashh FM/2020-04-08/10:30:00 - 11:30:00">
                                                                            <label class="" for="advert7649">Ashh FM</label></td> <td>2020-04-08</td>
                                                                        <td>10:30:00 - 11:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7649" class="selectslots" name="selectslots[]" onchange="getRate(7649);"><option disabled="" selected="">--select slot length--</option><option value="30/36/7649">30</option><option value="45/249/7650">45</option><option value="60/386/7651">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7449" name="mytvslots[]" value="7449/Prime FM/2020-04-08/13:00:00 - 14:00:00">
                                                                            <label class="" for="advert7449">Prime FM</label></td> <td>2020-04-08</td>
                                                                        <td>13:00:00 - 14:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7449" class="selectslots" name="selectslots[]" onchange="getRate(7449);"><option disabled="" selected="">--select slot length--</option><option value="30/36/7449">30</option><option value="45/139/7450">45</option><option value="60/376/7451">60</option><option value="30/95/8042">30</option><option value="45/145/8043">45</option><option value="60/299/8044">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7739" name="mytvslots[]" value="7739/RadioSilverFM/2020-04-08/15:00:00 - 15:30:00">
                                                                            <label class="" for="advert7739">RadioSilverFM</label></td> <td>2020-04-08</td>
                                                                        <td>15:00:00 - 15:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7739" class="selectslots" name="selectslots[]" onchange="getRate(7739);"><option disabled="" selected="">--select slot length--</option><option value="30/31/7739">30</option><option value="45/161/7740">45</option><option value="60/324/7741">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7467" name="mytvslots[]" value="7467/Prime FM/2020-04-08/15:30:00 - 16:30:00">
                                                                            <label class="" for="advert7467">Prime FM</label></td> <td>2020-04-08</td>
                                                                        <td>15:30:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7467" class="selectslots" name="selectslots[]" onchange="getRate(7467);"><option disabled="" selected="">--select slot length--</option><option value="30/84/7467">30</option><option value="45/198/7468">45</option><option value="60/500/7469">60</option><option value="30/53/7571">30</option><option value="45/205/7572">45</option><option value="60/293/7573">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8240" name="mytvslots[]" value="8240/Word FM/2020-04-08/17:00:00 - 18:00:00">
                                                                            <label class="" for="advert8240">Word FM</label></td> <td>2020-04-08</td>
                                                                        <td>17:00:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8240" class="selectslots" name="selectslots[]" onchange="getRate(8240);"><option disabled="" selected="">--select slot length--</option><option value="30/43/8240">30</option><option value="45/213/8241">45</option><option value="60/465/8242">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8494" name="mytvslots[]" value="8494/Solar FM/2020-04-08/21:00:00 - 22:00:00">
                                                                            <label class="" for="advert8494">Solar FM</label></td> <td>2020-04-08</td>
                                                                        <td>21:00:00 - 22:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8494" class="selectslots" name="selectslots[]" onchange="getRate(8494);"><option disabled="" selected="">--select slot length--</option><option value="30/34/7332">30</option><option value="45/203/7333">45</option><option value="60/848/7334">60</option><option value="30/43/8494">30</option><option value="45/145/8495">45</option><option value="60/400/8496">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7712" name="mytvslots[]" value="7712/Rok FM/2020-04-08/21:30:00 - 22:30:00">
                                                                            <label class="" for="advert7712">Rok FM</label></td> <td>2020-04-08</td>
                                                                        <td>21:30:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7712" class="selectslots" name="selectslots[]" onchange="getRate(7712);"><option disabled="" selected="">--select slot length--</option><option value="30/46/7712">30</option><option value="45/196/7713">45</option><option value="60/455/7714">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-09"><div class="popWrap" id="9" onclick="openThis(9);"><span class="daydate">9</span><span title="Available Radio Slots" class="open-button radio">17</span></div><div id="ad9"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7943" name="mytvslots[]" value="7943/NorthStar RadioFM/2020-04-09/03:30:00 - 04:30:00">
                                                                            <label class="" for="advert7943">NorthStar RadioFM</label></td> <td>2020-04-09</td>
                                                                        <td>03:30:00 - 04:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7943" class="selectslots" name="selectslots[]" onchange="getRate(7943);"><option disabled="" selected="">--select slot length--</option><option value="30/37/7943">30</option><option value="45/204/7944">45</option><option value="60/450/7945">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7529" name="mytvslots[]" value="7529/AsempaFM /2020-04-09/04:30:00 - 05:30:00">
                                                                            <label class="" for="advert7529">AsempaFM </label></td> <td>2020-04-09</td>
                                                                        <td>04:30:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7529" class="selectslots" name="selectslots[]" onchange="getRate(7529);"><option disabled="" selected="">--select slot length--</option><option value="30/74/7529">30</option><option value="45/147/7530">45</option><option value="60/311/7531">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7631" name="mytvslots[]" value="7631/KessbenFM /2020-04-09/05:00:00 - 05:30:00">
                                                                            <label class="" for="advert7631">KessbenFM </label></td> <td>2020-04-09</td>
                                                                        <td>05:00:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7631" class="selectslots" name="selectslots[]" onchange="getRate(7631);"><option disabled="" selected="">--select slot length--</option><option value="30/35/7631">30</option><option value="45/151/7632">45</option><option value="60/332/7633">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8572" name="mytvslots[]" value="8572/Sompa FM/2020-04-09/05:30:00 - 06:00:00">
                                                                            <label class="" for="advert8572">Sompa FM</label></td> <td>2020-04-09</td>
                                                                        <td>05:30:00 - 06:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8572" class="selectslots" name="selectslots[]" onchange="getRate(8572);"><option disabled="" selected="">--select slot length--</option><option value="30/75/8572">30</option><option value="45/249/8573">45</option><option value="60/313/8574">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8360" name="mytvslots[]" value="8360/Anapua FM/2020-04-09/06:30:00 - 07:30:00">
                                                                            <label class="" for="advert8360">Anapua FM</label></td> <td>2020-04-09</td>
                                                                        <td>06:30:00 - 07:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8360" class="selectslots" name="selectslots[]" onchange="getRate(8360);"><option disabled="" selected="">--select slot length--</option><option value="30/58/8360">30</option><option value="45/186/8361">45</option><option value="60/267/8362">60</option><option value="30/90/8441">30</option><option value="45/241/8442">45</option><option value="60/497/8443">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7934" name="mytvslots[]" value="7934/NorthStar RadioFM/2020-04-09/07:00:00 - 08:00:00">
                                                                            <label class="" for="advert7934">NorthStar RadioFM</label></td> <td>2020-04-09</td>
                                                                        <td>07:00:00 - 08:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7934" class="selectslots" name="selectslots[]" onchange="getRate(7934);"><option disabled="" selected="">--select slot length--</option><option value="30/76/7934">30</option><option value="45/111/7935">45</option><option value="60/415/7936">60</option><option value="30/54/8132">30</option><option value="45/174/8133">45</option><option value="60/307/8134">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7643" name="mytvslots[]" value="7643/Ashh FM/2020-04-09/08:00:00 - 09:00:00">
                                                                            <label class="" for="advert7643">Ashh FM</label></td> <td>2020-04-09</td>
                                                                        <td>08:00:00 - 09:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7643" class="selectslots" name="selectslots[]" onchange="getRate(7643);"><option disabled="" selected="">--select slot length--</option><option value="30/86/7643">30</option><option value="45/177/7644">45</option><option value="60/464/7645">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8345" name="mytvslots[]" value="8345/Dinpa FM/2020-04-09/10:00:00 - 11:00:00">
                                                                            <label class="" for="advert8345">Dinpa FM</label></td> <td>2020-04-09</td>
                                                                        <td>10:00:00 - 11:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8345" class="selectslots" name="selectslots[]" onchange="getRate(8345);"><option disabled="" selected="">--select slot length--</option><option value="30/98/8345">30</option><option value="45/159/8346">45</option><option value="60/322/8347">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7676" name="mytvslots[]" value="7676/Fox FM/2020-04-09/13:00:00 - 14:00:00">
                                                                            <label class="" for="advert7676">Fox FM</label></td> <td>2020-04-09</td>
                                                                        <td>13:00:00 - 14:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7676" class="selectslots" name="selectslots[]" onchange="getRate(7676);"><option disabled="" selected="">--select slot length--</option><option value="30/92/7676">30</option><option value="45/176/7677">45</option><option value="60/404/7678">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7928" name="mytvslots[]" value="7928/NorthStar RadioFM/2020-04-09/13:30:00 - 14:30:00">
                                                                            <label class="" for="advert7928">NorthStar RadioFM</label></td> <td>2020-04-09</td>
                                                                        <td>13:30:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7928" class="selectslots" name="selectslots[]" onchange="getRate(7928);"><option disabled="" selected="">--select slot length--</option><option value="30/38/7928">30</option><option value="45/222/7929">45</option><option value="60/277/7930">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8405" name="mytvslots[]" value="8405/Star FM/2020-04-09/14:00:00 - 15:00:00">
                                                                            <label class="" for="advert8405">Star FM</label></td> <td>2020-04-09</td>
                                                                        <td>14:00:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8405" class="selectslots" name="selectslots[]" onchange="getRate(8405);"><option disabled="" selected="">--select slot length--</option><option value="30/40/8405">30</option><option value="45/246/8406">45</option><option value="60/444/8407">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7685" name="mytvslots[]" value="7685/Boss FM/2020-04-09/15:30:00 - 16:00:00">
                                                                            <label class="" for="advert7685">Boss FM</label></td> <td>2020-04-09</td>
                                                                        <td>15:30:00 - 16:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7685" class="selectslots" name="selectslots[]" onchange="getRate(7685);"><option disabled="" selected="">--select slot length--</option><option value="30/33/7685">30</option><option value="45/164/7686">45</option><option value="60/353/7687">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7772" name="mytvslots[]" value="7772/SkyPowerFM /2020-04-09/18:00:00 - 18:30:00">
                                                                            <label class="" for="advert7772">SkyPowerFM </label></td> <td>2020-04-09</td>
                                                                        <td>18:00:00 - 18:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7772" class="selectslots" name="selectslots[]" onchange="getRate(7772);"><option disabled="" selected="">--select slot length--</option><option value="30/53/7772">30</option><option value="45/178/7773">45</option><option value="60/262/7774">60</option><option value="30/54/7940">30</option><option value="45/242/7941">45</option><option value="60/307/7942">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8557" name="mytvslots[]" value="8557/Sompa FM/2020-04-09/18:30:00 - 19:00:00">
                                                                            <label class="" for="advert8557">Sompa FM</label></td> <td>2020-04-09</td>
                                                                        <td>18:30:00 - 19:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8557" class="selectslots" name="selectslots[]" onchange="getRate(8557);"><option disabled="" selected="">--select slot length--</option><option value="30/94/8557">30</option><option value="45/159/8558">45</option><option value="60/441/8559">60</option><option value="30/88/8706">30</option><option value="45/229/8707">45</option><option value="60/493/8708">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8647" name="mytvslots[]" value="8647/Sunrise FM/2020-04-09/21:00:00 - 21:30:00">
                                                                            <label class="" for="advert8647">Sunrise FM</label></td> <td>2020-04-09</td>
                                                                        <td>21:00:00 - 21:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8647" class="selectslots" name="selectslots[]" onchange="getRate(8647);"><option disabled="" selected="">--select slot length--</option><option value="30/38/8647">30</option><option value="45/152/8648">45</option><option value="60/304/8649">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7787" name="mytvslots[]" value="7787/SkyPowerFM /2020-04-09/21:30:00 - 22:30:00">
                                                                            <label class="" for="advert7787">SkyPowerFM </label></td> <td>2020-04-09</td>
                                                                        <td>21:30:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7787" class="selectslots" name="selectslots[]" onchange="getRate(7787);"><option disabled="" selected="">--select slot length--</option><option value="30/50/7787">30</option><option value="45/143/7788">45</option><option value="60/262/7789">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8465" name="mytvslots[]" value="8465/Spark FM/2020-04-09/22:30:00 - 23:00:00">
                                                                            <label class="" for="advert8465">Spark FM</label></td> <td>2020-04-09</td>
                                                                        <td>22:30:00 - 23:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8465" class="selectslots" name="selectslots[]" onchange="getRate(8465);"><option disabled="" selected="">--select slot length--</option><option value="30/44/8465">30</option><option value="45/112/8466">45</option><option value="60/362/8467">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-10"><div class="popWrap" id="10" onclick="openThis(10);"><span class="daydate">10</span><span title="Available Radio Slots" class="open-button radio">11</span></div><div id="ad10"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7748" name="mytvslots[]" value="7748/RadioSilverFM/2020-04-10/00:30:00 - 01:00:00">
                                                                            <label class="" for="advert7748">RadioSilverFM</label></td> <td>2020-04-10</td>
                                                                        <td>00:30:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7748" class="selectslots" name="selectslots[]" onchange="getRate(7748);"><option disabled="" selected="">--select slot length--</option><option value="30/38/7748">30</option><option value="45/234/7749">45</option><option value="60/268/7750">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8503" name="mytvslots[]" value="8503/Pink FM/2020-04-10/03:00:00 - 03:30:00">
                                                                            <label class="" for="advert8503">Pink FM</label></td> <td>2020-04-10</td>
                                                                        <td>03:00:00 - 03:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8503" class="selectslots" name="selectslots[]" onchange="getRate(8503);"><option disabled="" selected="">--select slot length--</option><option value="30/99/8503">30</option><option value="45/183/8504">45</option><option value="60/499/8505">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7568" name="mytvslots[]" value="7568/SweetMelodiesFM /2020-04-10/05:30:00 - 06:30:00">
                                                                            <label class="" for="advert7568">SweetMelodiesFM </label></td> <td>2020-04-10</td>
                                                                        <td>05:30:00 - 06:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7568" class="selectslots" name="selectslots[]" onchange="getRate(7568);"><option disabled="" selected="">--select slot length--</option><option value="30/42/7568">30</option><option value="45/190/7569">45</option><option value="60/406/7570">60</option><option value="30/31/7892">30</option><option value="45/104/7893">45</option><option value="60/322/7894">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8306" name="mytvslots[]" value="8306/Radio Gurune FM/2020-04-10/09:00:00 - 10:00:00">
                                                                            <label class="" for="advert8306">Radio Gurune FM</label></td> <td>2020-04-10</td>
                                                                        <td>09:00:00 - 10:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8306" class="selectslots" name="selectslots[]" onchange="getRate(8306);"><option disabled="" selected="">--select slot length--</option><option value="30/71/8306">30</option><option value="45/138/8307">45</option><option value="60/368/8308">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8545" name="mytvslots[]" value="8545/Yes FM/2020-04-10/12:00:00 - 13:00:00">
                                                                            <label class="" for="advert8545">Yes FM</label></td> <td>2020-04-10</td>
                                                                        <td>12:00:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8545" class="selectslots" name="selectslots[]" onchange="getRate(8545);"><option disabled="" selected="">--select slot length--</option><option value="30/94/8545">30</option><option value="45/221/8546">45</option><option value="60/424/8547">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8336" name="mytvslots[]" value="8336/Dinpa FM/2020-04-10/14:00:00 - 15:00:00">
                                                                            <label class="" for="advert8336">Dinpa FM</label></td> <td>2020-04-10</td>
                                                                        <td>14:00:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8336" class="selectslots" name="selectslots[]" onchange="getRate(8336);"><option disabled="" selected="">--select slot length--</option><option value="30/57/8336">30</option><option value="45/248/8337">45</option><option value="60/407/8338">60</option><option value="30/67/8399">30</option><option value="45/177/8400">45</option><option value="60/410/8401">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8258" name="mytvslots[]" value="8258/A1 Radio FM/2020-04-10/14:30:00 - 15:00:00">
                                                                            <label class="" for="advert8258">A1 Radio FM</label></td> <td>2020-04-10</td>
                                                                        <td>14:30:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8258" class="selectslots" name="selectslots[]" onchange="getRate(8258);"><option disabled="" selected="">--select slot length--</option><option value="30/32/8258">30</option><option value="45/174/8259">45</option><option value="60/380/8260">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7473" name="mytvslots[]" value="7473/Choice FM/2020-04-10/20:30:00 - 21:00:00">
                                                                            <label class="" for="advert7473">Choice FM</label></td> <td>2020-04-10</td>
                                                                        <td>20:30:00 - 21:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7473" class="selectslots" name="selectslots[]" onchange="getRate(7473);"><option disabled="" selected="">--select slot length--</option><option value="30/81/7473">30</option><option value="45/231/7474">45</option><option value="60/420/7475">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7838" name="mytvslots[]" value="7838/RadioTamaleFM/2020-04-10/21:00:00 - 21:30:00">
                                                                            <label class="" for="advert7838">RadioTamaleFM</label></td> <td>2020-04-10</td>
                                                                        <td>21:00:00 - 21:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7838" class="selectslots" name="selectslots[]" onchange="getRate(7838);"><option disabled="" selected="">--select slot length--</option><option value="30/30/7838">30</option><option value="45/184/7839">45</option><option value="60/349/7840">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8641" name="mytvslots[]" value="8641/Eastern FM/2020-04-10/22:00:00 - 22:30:00">
                                                                            <label class="" for="advert8641">Eastern FM</label></td> <td>2020-04-10</td>
                                                                        <td>22:00:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8641" class="selectslots" name="selectslots[]" onchange="getRate(8641);"><option disabled="" selected="">--select slot length--</option><option value="30/37/8641">30</option><option value="45/204/8642">45</option><option value="60/328/8643">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7655" name="mytvslots[]" value="7655/Ashh FM/2020-04-10/23:00:00 - 23:30:00">
                                                                            <label class="" for="advert7655">Ashh FM</label></td> <td>2020-04-10</td>
                                                                        <td>23:00:00 - 23:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7655" class="selectslots" name="selectslots[]" onchange="getRate(7655);"><option disabled="" selected="">--select slot length--</option><option value="30/44/7655">30</option><option value="45/207/7656">45</option><option value="60/305/7657">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-11"><div class="popWrap" id="11" onclick="openThis(11);"><span class="daydate">11</span><span title="Available Radio Slots" class="open-button radio">18</span></div><div id="ad11"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7760" name="mytvslots[]" value="7760/RadioSilverFM/2020-04-11/00:00:00 - 00:30:00">
                                                                            <label class="" for="advert7760">RadioSilverFM</label></td> <td>2020-04-11</td>
                                                                        <td>00:00:00 - 00:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7760" class="selectslots" name="selectslots[]" onchange="getRate(7760);"><option disabled="" selected="">--select slot length--</option><option value="30/60/7760">30</option><option value="45/175/7761">45</option><option value="60/436/7762">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8024" name="mytvslots[]" value="8024/Holy FM/2020-04-11/00:30:00 - 01:00:00">
                                                                            <label class="" for="advert8024">Holy FM</label></td> <td>2020-04-11</td>
                                                                        <td>00:30:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8024" class="selectslots" name="selectslots[]" onchange="getRate(8024);"><option disabled="" selected="">--select slot length--</option><option value="30/94/8024">30</option><option value="45/176/8025">45</option><option value="60/492/8026">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8189" name="mytvslots[]" value="8189/Radio Upper West/2020-04-11/01:30:00 - 02:30:00">
                                                                            <label class="" for="advert8189">Radio Upper West</label></td> <td>2020-04-11</td>
                                                                        <td>01:30:00 - 02:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8189" class="selectslots" name="selectslots[]" onchange="getRate(8189);"><option disabled="" selected="">--select slot length--</option><option value="30/60/8189">30</option><option value="45/140/8190">45</option><option value="60/436/8191">60</option><option value="30/99/8384">30</option><option value="45/113/8385">45</option><option value="60/280/8386">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8512" name="mytvslots[]" value="8512/Pink FM/2020-04-11/02:00:00 - 02:30:00">
                                                                            <label class="" for="advert8512">Pink FM</label></td> <td>2020-04-11</td>
                                                                        <td>02:00:00 - 02:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8512" class="selectslots" name="selectslots[]" onchange="getRate(8512);"><option disabled="" selected="">--select slot length--</option><option value="30/93/8512">30</option><option value="45/193/8513">45</option><option value="60/398/8514">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7724" name="mytvslots[]" value="7724/Rok FM/2020-04-11/03:30:00 - 04:00:00">
                                                                            <label class="" for="advert7724">Rok FM</label></td> <td>2020-04-11</td>
                                                                        <td>03:30:00 - 04:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7724" class="selectslots" name="selectslots[]" onchange="getRate(7724);"><option disabled="" selected="">--select slot length--</option><option value="30/58/7724">30</option><option value="45/238/7725">45</option><option value="60/444/7726">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7811" name="mytvslots[]" value="7811/RadioMaxxFM/2020-04-11/05:00:00 - 06:00:00">
                                                                            <label class="" for="advert7811">RadioMaxxFM</label></td> <td>2020-04-11</td>
                                                                        <td>05:00:00 - 06:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7811" class="selectslots" name="selectslots[]" onchange="getRate(7811);"><option disabled="" selected="">--select slot length--</option><option value="30/62/7811">30</option><option value="45/196/7812">45</option><option value="60/426/7813">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8273" name="mytvslots[]" value="8273/A1 Radio FM/2020-04-11/06:30:00 - 07:30:00">
                                                                            <label class="" for="advert8273">A1 Radio FM</label></td> <td>2020-04-11</td>
                                                                        <td>06:30:00 - 07:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8273" class="selectslots" name="selectslots[]" onchange="getRate(8273);"><option disabled="" selected="">--select slot length--</option><option value="30/74/8273">30</option><option value="45/234/8274">45</option><option value="60/414/8275">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7976" name="mytvslots[]" value="7976/Light FM/2020-04-11/07:00:00 - 08:00:00">
                                                                            <label class="" for="advert7976">Light FM</label></td> <td>2020-04-11</td>
                                                                        <td>07:00:00 - 08:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7976" class="selectslots" name="selectslots[]" onchange="getRate(7976);"><option disabled="" selected="">--select slot length--</option><option value="30/96/7976">30</option><option value="45/197/7977">45</option><option value="60/354/7978">60</option><option value="30/93/8674">30</option><option value="45/144/8675">45</option><option value="60/431/8676">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7742" name="mytvslots[]" value="7742/RadioSilverFM/2020-04-11/07:30:00 - 08:00:00">
                                                                            <label class="" for="advert7742">RadioSilverFM</label></td> <td>2020-04-11</td>
                                                                        <td>07:30:00 - 08:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7742" class="selectslots" name="selectslots[]" onchange="getRate(7742);"><option disabled="" selected="">--select slot length--</option><option value="30/52/7742">30</option><option value="45/108/7743">45</option><option value="60/410/7744">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8378" name="mytvslots[]" value="8378/Anapua FM/2020-04-11/08:30:00 - 09:30:00">
                                                                            <label class="" for="advert8378">Anapua FM</label></td> <td>2020-04-11</td>
                                                                        <td>08:30:00 - 09:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8378" class="selectslots" name="selectslots[]" onchange="getRate(8378);"><option disabled="" selected="">--select slot length--</option><option value="30/79/8378">30</option><option value="45/237/8379">45</option><option value="60/334/8380">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8485" name="mytvslots[]" value="8485/Solar FM/2020-04-11/11:30:00 - 12:00:00">
                                                                            <label class="" for="advert8485">Solar FM</label></td> <td>2020-04-11</td>
                                                                        <td>11:30:00 - 12:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8485" class="selectslots" name="selectslots[]" onchange="getRate(8485);"><option disabled="" selected="">--select slot length--</option><option value="30/58/8485">30</option><option value="45/161/8486">45</option><option value="60/406/8487">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8459" name="mytvslots[]" value="8459/Spark FM/2020-04-11/12:30:00 - 13:00:00">
                                                                            <label class="" for="advert8459">Spark FM</label></td> <td>2020-04-11</td>
                                                                        <td>12:30:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8459" class="selectslots" name="selectslots[]" onchange="getRate(8459);"><option disabled="" selected="">--select slot length--</option><option value="30/70/8459">30</option><option value="45/116/8460">45</option><option value="60/421/8461">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7919" name="mytvslots[]" value="7919/DiamondFM/2020-04-11/14:00:00 - 14:30:00">
                                                                            <label class="" for="advert7919">DiamondFM</label></td> <td>2020-04-11</td>
                                                                        <td>14:00:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7919" class="selectslots" name="selectslots[]" onchange="getRate(7919);"><option disabled="" selected="">--select slot length--</option><option value="30/68/7919">30</option><option value="45/159/7920">45</option><option value="60/258/7921">60</option><option value="30/31/8117">30</option><option value="45/132/8118">45</option><option value="60/300/8119">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8174" name="mytvslots[]" value="8174/Radio Upper West/2020-04-11/15:30:00 - 16:00:00">
                                                                            <label class="" for="advert8174">Radio Upper West</label></td> <td>2020-04-11</td>
                                                                        <td>15:30:00 - 16:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8174" class="selectslots" name="selectslots[]" onchange="getRate(8174);"><option disabled="" selected="">--select slot length--</option><option value="30/41/8174">30</option><option value="45/229/8175">45</option><option value="60/395/8176">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8521" name="mytvslots[]" value="8521/Pink FM/2020-04-11/17:00:00 - 18:00:00">
                                                                            <label class="" for="advert8521">Pink FM</label></td> <td>2020-04-11</td>
                                                                        <td>17:00:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8521" class="selectslots" name="selectslots[]" onchange="getRate(8521);"><option disabled="" selected="">--select slot length--</option><option value="30/72/8521">30</option><option value="45/185/8522">45</option><option value="60/296/8523">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8045" name="mytvslots[]" value="8045/Hope FM/2020-04-11/18:00:00 - 19:00:00">
                                                                            <label class="" for="advert8045">Hope FM</label></td> <td>2020-04-11</td>
                                                                        <td>18:00:00 - 19:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8045" class="selectslots" name="selectslots[]" onchange="getRate(8045);"><option disabled="" selected="">--select slot length--</option><option value="30/61/8045">30</option><option value="45/118/8046">45</option><option value="60/435/8047">60</option><option value="30/44/8177">30</option><option value="45/139/8178">45</option><option value="60/479/8179">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7532" name="mytvslots[]" value="7532/AsempaFM /2020-04-11/20:30:00 - 21:00:00">
                                                                            <label class="" for="advert7532">AsempaFM </label></td> <td>2020-04-11</td>
                                                                        <td>20:30:00 - 21:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7532" class="selectslots" name="selectslots[]" onchange="getRate(7532);"><option disabled="" selected="">--select slot length--</option><option value="30/73/7532">30</option><option value="45/177/7533">45</option><option value="60/304/7534">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8366" name="mytvslots[]" value="8366/Anapua FM/2020-04-11/21:30:00 - 22:30:00">
                                                                            <label class="" for="advert8366">Anapua FM</label></td> <td>2020-04-11</td>
                                                                        <td>21:30:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8366" class="selectslots" name="selectslots[]" onchange="getRate(8366);"><option disabled="" selected="">--select slot length--</option><option value="30/40/8366">30</option><option value="45/143/8367">45</option><option value="60/373/8368">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td></tr><tr><td border="1" class="day available" rel="2020-04-12"><div class="popWrap" id="12" onclick="openThis(12);"><span class="daydate">12</span><span title="Available Radio Slots" class="open-button radio">12</span></div><div id="ad12"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7574" name="mytvslots[]" value="7574/SweetMelodiesFM /2020-04-12/01:00:00 - 02:00:00">
                                                                            <label class="" for="advert7574">SweetMelodiesFM </label></td> <td>2020-04-12</td>
                                                                        <td>01:00:00 - 02:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7574" class="selectslots" name="selectslots[]" onchange="getRate(7574);"><option disabled="" selected="">--select slot length--</option><option value="30/80/7574">30</option><option value="45/142/7575">45</option><option value="60/346/7576">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8156" name="mytvslots[]" value="8156/Radford FM/2020-04-12/02:00:00 - 02:30:00">
                                                                            <label class="" for="advert8156">Radford FM</label></td> <td>2020-04-12</td>
                                                                        <td>02:00:00 - 02:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8156" class="selectslots" name="selectslots[]" onchange="getRate(8156);"><option disabled="" selected="">--select slot length--</option><option value="30/51/8156">30</option><option value="45/127/8157">45</option><option value="60/412/8158">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8018" name="mytvslots[]" value="8018/Holy FM/2020-04-12/04:30:00 - 05:00:00">
                                                                            <label class="" for="advert8018">Holy FM</label></td> <td>2020-04-12</td>
                                                                        <td>04:30:00 - 05:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8018" class="selectslots" name="selectslots[]" onchange="getRate(8018);"><option disabled="" selected="">--select slot length--</option><option value="30/93/8018">30</option><option value="45/245/8019">45</option><option value="60/375/8020">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8393" name="mytvslots[]" value="8393/Space FM/2020-04-12/05:30:00 - 06:30:00">
                                                                            <label class="" for="advert8393">Space FM</label></td> <td>2020-04-12</td>
                                                                        <td>05:30:00 - 06:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8393" class="selectslots" name="selectslots[]" onchange="getRate(8393);"><option disabled="" selected="">--select slot length--</option><option value="30/98/8393">30</option><option value="45/165/8394">45</option><option value="60/499/8395">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8694" name="mytvslots[]" value="8694/Goodlife FM/2020-04-12/06:00:00 - 06:30:00">
                                                                            <label class="" for="advert8694">Goodlife FM</label></td> <td>2020-04-12</td>
                                                                        <td>06:00:00 - 06:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8694" class="selectslots" name="selectslots[]" onchange="getRate(8694);"><option disabled="" selected="">--select slot length--</option><option value="30/99/8694">30</option><option value="45/121/8695">45</option><option value="60/429/8696">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7476" name="mytvslots[]" value="7476/Choice FM/2020-04-12/08:30:00 - 09:00:00">
                                                                            <label class="" for="advert7476">Choice FM</label></td> <td>2020-04-12</td>
                                                                        <td>08:30:00 - 09:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7476" class="selectslots" name="selectslots[]" onchange="getRate(7476);"><option disabled="" selected="">--select slot length--</option><option value="30/57/7476">30</option><option value="45/243/7477">45</option><option value="60/451/7478">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7871" name="mytvslots[]" value="7871/Zaa RadioFM/2020-04-12/09:00:00 - 09:30:00">
                                                                            <label class="" for="advert7871">Zaa RadioFM</label></td> <td>2020-04-12</td>
                                                                        <td>09:00:00 - 09:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7871" class="selectslots" name="selectslots[]" onchange="getRate(7871);"><option disabled="" selected="">--select slot length--</option><option value="30/60/7871">30</option><option value="45/220/7872">45</option><option value="60/255/7873">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7991" name="mytvslots[]" value="7991/Light FM/2020-04-12/15:30:00 - 16:30:00">
                                                                            <label class="" for="advert7991">Light FM</label></td> <td>2020-04-12</td>
                                                                        <td>15:30:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7991" class="selectslots" name="selectslots[]" onchange="getRate(7991);"><option disabled="" selected="">--select slot length--</option><option value="30/36/7991">30</option><option value="45/129/7992">45</option><option value="60/450/7993">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7697" name="mytvslots[]" value="7697/Boss FM/2020-04-12/16:00:00 - 17:00:00">
                                                                            <label class="" for="advert7697">Boss FM</label></td> <td>2020-04-12</td>
                                                                        <td>16:00:00 - 17:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7697" class="selectslots" name="selectslots[]" onchange="getRate(7697);"><option disabled="" selected="">--select slot length--</option><option value="30/58/7697">30</option><option value="45/148/7698">45</option><option value="60/263/7699">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8683" name="mytvslots[]" value="8683/Goodlife FM/2020-04-12/16:30:00 - 17:00:00">
                                                                            <label class="" for="advert8683">Goodlife FM</label></td> <td>2020-04-12</td>
                                                                        <td>16:30:00 - 17:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8683" class="selectslots" name="selectslots[]" onchange="getRate(8683);"><option disabled="" selected="">--select slot length--</option><option value="30/85/8683">30</option><option value="45/237/8684">45</option><option value="60/343/8685">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7757" name="mytvslots[]" value="7757/RadioSilverFM/2020-04-12/19:00:00 - 19:30:00">
                                                                            <label class="" for="advert7757">RadioSilverFM</label></td> <td>2020-04-12</td>
                                                                        <td>19:00:00 - 19:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7757" class="selectslots" name="selectslots[]" onchange="getRate(7757);"><option disabled="" selected="">--select slot length--</option><option value="30/89/7757">30</option><option value="45/193/7758">45</option><option value="60/389/7759">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7691" name="mytvslots[]" value="7691/Boss FM/2020-04-12/19:30:00 - 20:30:00">
                                                                            <label class="" for="advert7691">Boss FM</label></td> <td>2020-04-12</td>
                                                                        <td>19:30:00 - 20:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7691" class="selectslots" name="selectslots[]" onchange="getRate(7691);"><option disabled="" selected="">--select slot length--</option><option value="30/37/7691">30</option><option value="45/126/7692">45</option><option value="60/272/7693">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-13"><div class="popWrap" id="13" onclick="openThis(13);"><span class="daydate">13</span><span title="Available Radio Slots" class="open-button radio">15</span></div><div id="ad13"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7847" name="mytvslots[]" value="7847/RadioTamaleFM/2020-04-13/00:30:00 - 01:30:00">
                                                                            <label class="" for="advert7847">RadioTamaleFM</label></td> <td>2020-04-13</td>
                                                                        <td>00:30:00 - 01:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7847" class="selectslots" name="selectslots[]" onchange="getRate(7847);"><option disabled="" selected="">--select slot length--</option><option value="30/41/7847">30</option><option value="45/124/7848">45</option><option value="60/265/7849">60</option><option value="30/45/8162">30</option><option value="45/172/8163">45</option><option value="60/398/8164">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7952" name="mytvslots[]" value="7952/Savannah RadioFM/2020-04-13/02:00:00 - 02:30:00">
                                                                            <label class="" for="advert7952">Savannah RadioFM</label></td> <td>2020-04-13</td>
                                                                        <td>02:00:00 - 02:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7952" class="selectslots" name="selectslots[]" onchange="getRate(7952);"><option disabled="" selected="">--select slot length--</option><option value="30/87/7952">30</option><option value="45/242/7953">45</option><option value="60/460/7954">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8668" name="mytvslots[]" value="8668/Sunrise FM/2020-04-13/04:00:00 - 05:00:00">
                                                                            <label class="" for="advert8668">Sunrise FM</label></td> <td>2020-04-13</td>
                                                                        <td>04:00:00 - 05:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8668" class="selectslots" name="selectslots[]" onchange="getRate(8668);"><option disabled="" selected="">--select slot length--</option><option value="30/47/8668">30</option><option value="45/228/8669">45</option><option value="60/443/8670">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7973" name="mytvslots[]" value="7973/Light FM/2020-04-13/06:00:00 - 06:30:00">
                                                                            <label class="" for="advert7973">Light FM</label></td> <td>2020-04-13</td>
                                                                        <td>06:00:00 - 06:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7973" class="selectslots" name="selectslots[]" onchange="getRate(7973);"><option disabled="" selected="">--select slot length--</option><option value="30/70/7973">30</option><option value="45/228/7974">45</option><option value="60/448/7975">60</option><option value="30/32/8051">30</option><option value="45/124/8052">45</option><option value="60/285/8053">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8198" name="mytvslots[]" value="8198/Radio Progress FM/2020-04-13/07:00:00 - 07:30:00">
                                                                            <label class="" for="advert8198">Radio Progress FM</label></td> <td>2020-04-13</td>
                                                                        <td>07:00:00 - 07:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8198" class="selectslots" name="selectslots[]" onchange="getRate(8198);"><option disabled="" selected="">--select slot length--</option><option value="30/96/8198">30</option><option value="45/199/8199">45</option><option value="60/402/8200">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8180" name="mytvslots[]" value="8180/Radio Upper West/2020-04-13/09:00:00 - 09:30:00">
                                                                            <label class="" for="advert8180">Radio Upper West</label></td> <td>2020-04-13</td>
                                                                        <td>09:00:00 - 09:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8180" class="selectslots" name="selectslots[]" onchange="getRate(8180);"><option disabled="" selected="">--select slot length--</option><option value="30/34/8180">30</option><option value="45/105/8181">45</option><option value="60/268/8182">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7898" name="mytvslots[]" value="7898/DiamondFM/2020-04-13/09:30:00 - 10:00:00">
                                                                            <label class="" for="advert7898">DiamondFM</label></td> <td>2020-04-13</td>
                                                                        <td>09:30:00 - 10:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7898" class="selectslots" name="selectslots[]" onchange="getRate(7898);"><option disabled="" selected="">--select slot length--</option><option value="30/99/7898">30</option><option value="45/199/7899">45</option><option value="60/342/7900">60</option><option value="30/44/8030">30</option><option value="45/164/8031">45</option><option value="60/448/8032">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7455" name="mytvslots[]" value="7455/Prime FM/2020-04-13/10:30:00 - 11:30:00">
                                                                            <label class="" for="advert7455">Prime FM</label></td> <td>2020-04-13</td>
                                                                        <td>10:30:00 - 11:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7455" class="selectslots" name="selectslots[]" onchange="getRate(7455);"><option disabled="" selected="">--select slot length--</option><option value="30/93/7455">30</option><option value="45/117/7456">45</option><option value="60/317/7457">60</option><option value="30/69/8450">30</option><option value="45/171/8451">45</option><option value="60/317/8452">60</option><option value="30/35/8575">30</option><option value="45/118/8576">45</option><option value="60/325/8577">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8108" name="mytvslots[]" value="8108/Victory FM/2020-04-13/11:30:00 - 12:00:00">
                                                                            <label class="" for="advert8108">Victory FM</label></td> <td>2020-04-13</td>
                                                                        <td>11:30:00 - 12:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8108" class="selectslots" name="selectslots[]" onchange="getRate(8108);"><option disabled="" selected="">--select slot length--</option><option value="30/97/8108">30</option><option value="45/218/8109">45</option><option value="60/442/8110">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7661" name="mytvslots[]" value="7661/Fox FM/2020-04-13/13:30:00 - 14:30:00">
                                                                            <label class="" for="advert7661">Fox FM</label></td> <td>2020-04-13</td>
                                                                        <td>13:30:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7661" class="selectslots" name="selectslots[]" onchange="getRate(7661);"><option disabled="" selected="">--select slot length--</option><option value="30/93/7661">30</option><option value="45/195/7662">45</option><option value="60/373/7663">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7538" name="mytvslots[]" value="7538/AsempaFM /2020-04-13/14:00:00 - 15:00:00">
                                                                            <label class="" for="advert7538">AsempaFM </label></td> <td>2020-04-13</td>
                                                                        <td>14:00:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7538" class="selectslots" name="selectslots[]" onchange="getRate(7538);"><option disabled="" selected="">--select slot length--</option><option value="30/40/7538">30</option><option value="45/196/7539">45</option><option value="60/316/7540">60</option><option value="30/39/8009">30</option><option value="45/201/8010">45</option><option value="60/319/8011">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8447" name="mytvslots[]" value="8447/Sky FM/2020-04-13/15:30:00 - 16:30:00">
                                                                            <label class="" for="advert8447">Sky FM</label></td> <td>2020-04-13</td>
                                                                        <td>15:30:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8447" class="selectslots" name="selectslots[]" onchange="getRate(8447);"><option disabled="" selected="">--select slot length--</option><option value="30/74/8447">30</option><option value="45/126/8448">45</option><option value="60/349/8449">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7565" name="mytvslots[]" value="7565/SweetMelodiesFM /2020-04-13/16:00:00 - 17:00:00">
                                                                            <label class="" for="advert7565">SweetMelodiesFM </label></td> <td>2020-04-13</td>
                                                                        <td>16:00:00 - 17:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7565" class="selectslots" name="selectslots[]" onchange="getRate(7565);"><option disabled="" selected="">--select slot length--</option><option value="30/70/7565">30</option><option value="45/211/7566">45</option><option value="60/475/7567">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7482" name="mytvslots[]" value="7482/Choice FM/2020-04-13/17:00:00 - 18:00:00">
                                                                            <label class="" for="advert7482">Choice FM</label></td> <td>2020-04-13</td>
                                                                        <td>17:00:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7482" class="selectslots" name="selectslots[]" onchange="getRate(7482);"><option disabled="" selected="">--select slot length--</option><option value="30/47/7482">30</option><option value="45/103/7483">45</option><option value="60/403/7484">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7907" name="mytvslots[]" value="7907/DiamondFM/2020-04-13/18:00:00 - 18:30:00">
                                                                            <label class="" for="advert7907">DiamondFM</label></td> <td>2020-04-13</td>
                                                                        <td>18:00:00 - 18:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7907" class="selectslots" name="selectslots[]" onchange="getRate(7907);"><option disabled="" selected="">--select slot length--</option><option value="30/80/7907">30</option><option value="45/204/7908">45</option><option value="60/265/7909">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-14"><div class="popWrap" id="14" onclick="openThis(14);"><span class="daydate">14</span><span title="Available Radio Slots" class="open-button radio">11</span></div><div id="ad14"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7577" name="mytvslots[]" value="7577/SweetMelodiesFM /2020-04-14/00:00:00 - 00:30:00">
                                                                            <label class="" for="advert7577">SweetMelodiesFM </label></td> <td>2020-04-14</td>
                                                                        <td>00:00:00 - 00:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7577" class="selectslots" name="selectslots[]" onchange="getRate(7577);"><option disabled="" selected="">--select slot length--</option><option value="30/66/7577">30</option><option value="45/201/7578">45</option><option value="60/297/7579">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7916" name="mytvslots[]" value="7916/DiamondFM/2020-04-14/04:30:00 - 05:00:00">
                                                                            <label class="" for="advert7916">DiamondFM</label></td> <td>2020-04-14</td>
                                                                        <td>04:30:00 - 05:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7916" class="selectslots" name="selectslots[]" onchange="getRate(7916);"><option disabled="" selected="">--select slot length--</option><option value="30/37/7916">30</option><option value="45/165/7917">45</option><option value="60/282/7918">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7841" name="mytvslots[]" value="7841/RadioTamaleFM/2020-04-14/06:30:00 - 07:30:00">
                                                                            <label class="" for="advert7841">RadioTamaleFM</label></td> <td>2020-04-14</td>
                                                                        <td>06:30:00 - 07:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7841" class="selectslots" name="selectslots[]" onchange="getRate(7841);"><option disabled="" selected="">--select slot length--</option><option value="30/31/7841">30</option><option value="45/200/7842">45</option><option value="60/331/7843">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8066" name="mytvslots[]" value="8066/Jubilee Radio FM/2020-04-14/09:00:00 - 10:00:00">
                                                                            <label class="" for="advert8066">Jubilee Radio FM</label></td> <td>2020-04-14</td>
                                                                        <td>09:00:00 - 10:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8066" class="selectslots" name="selectslots[]" onchange="getRate(8066);"><option disabled="" selected="">--select slot length--</option><option value="30/74/8066">30</option><option value="45/139/8067">45</option><option value="60/251/8068">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7727" name="mytvslots[]" value="7727/Rok FM/2020-04-14/10:30:00 - 11:30:00">
                                                                            <label class="" for="advert7727">Rok FM</label></td> <td>2020-04-14</td>
                                                                        <td>10:30:00 - 11:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7727" class="selectslots" name="selectslots[]" onchange="getRate(7727);"><option disabled="" selected="">--select slot length--</option><option value="30/53/7727">30</option><option value="45/230/7728">45</option><option value="60/329/7729">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8435" name="mytvslots[]" value="8435/Sky FM/2020-04-14/12:00:00 - 12:30:00">
                                                                            <label class="" for="advert8435">Sky FM</label></td> <td>2020-04-14</td>
                                                                        <td>12:00:00 - 12:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8435" class="selectslots" name="selectslots[]" onchange="getRate(8435);"><option disabled="" selected="">--select slot length--</option><option value="30/34/8435">30</option><option value="45/212/8436">45</option><option value="60/480/8437">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8078" name="mytvslots[]" value="8078/Jubilee Radio FM/2020-04-14/12:30:00 - 13:00:00">
                                                                            <label class="" for="advert8078">Jubilee Radio FM</label></td> <td>2020-04-14</td>
                                                                        <td>12:30:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8078" class="selectslots" name="selectslots[]" onchange="getRate(8078);"><option disabled="" selected="">--select slot length--</option><option value="30/45/8078">30</option><option value="45/209/8079">45</option><option value="60/366/8080">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7664" name="mytvslots[]" value="7664/Fox FM/2020-04-14/15:30:00 - 16:30:00">
                                                                            <label class="" for="advert7664">Fox FM</label></td> <td>2020-04-14</td>
                                                                        <td>15:30:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7664" class="selectslots" name="selectslots[]" onchange="getRate(7664);"><option disabled="" selected="">--select slot length--</option><option value="30/56/7664">30</option><option value="45/220/7665">45</option><option value="60/360/7666">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8015" name="mytvslots[]" value="8015/Holy FM/2020-04-14/17:00:00 - 18:00:00">
                                                                            <label class="" for="advert8015">Holy FM</label></td> <td>2020-04-14</td>
                                                                        <td>17:00:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8015" class="selectslots" name="selectslots[]" onchange="getRate(8015);"><option disabled="" selected="">--select slot length--</option><option value="30/97/8015">30</option><option value="45/165/8016">45</option><option value="60/267/8017">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7721" name="mytvslots[]" value="7721/Rok FM/2020-04-14/18:00:00 - 19:00:00">
                                                                            <label class="" for="advert7721">Rok FM</label></td> <td>2020-04-14</td>
                                                                        <td>18:00:00 - 19:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7721" class="selectslots" name="selectslots[]" onchange="getRate(7721);"><option disabled="" selected="">--select slot length--</option><option value="30/84/7721">30</option><option value="45/140/7722">45</option><option value="60/354/7723">60</option><option value="30/68/8408">30</option><option value="45/133/8409">45</option><option value="60/339/8410">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8644" name="mytvslots[]" value="8644/Sunrise FM/2020-04-14/21:00:00 - 22:00:00">
                                                                            <label class="" for="advert8644">Sunrise FM</label></td> <td>2020-04-14</td>
                                                                        <td>21:00:00 - 22:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8644" class="selectslots" name="selectslots[]" onchange="getRate(8644);"><option disabled="" selected="">--select slot length--</option><option value="30/36/8644">30</option><option value="45/240/8645">45</option><option value="60/398/8646">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-15"><div class="popWrap" id="15" onclick="openThis(15);"><span class="daydate">15</span><span title="Available Radio Slots" class="open-button radio">9</span></div><div id="ad15"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7586" name="mytvslots[]" value="7586/LuvFM /2020-04-15/01:30:00 - 02:00:00">
                                                                            <label class="" for="advert7586">LuvFM </label></td> <td>2020-04-15</td>
                                                                        <td>01:30:00 - 02:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7586" class="selectslots" name="selectslots[]" onchange="getRate(7586);"><option disabled="" selected="">--select slot length--</option><option value="30/53/7586">30</option><option value="45/236/7587">45</option><option value="60/374/7588">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8093" name="mytvslots[]" value="8093/Victory FM/2020-04-15/05:00:00 - 06:00:00">
                                                                            <label class="" for="advert8093">Victory FM</label></td> <td>2020-04-15</td>
                                                                        <td>05:00:00 - 06:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8093" class="selectslots" name="selectslots[]" onchange="getRate(8093);"><option disabled="" selected="">--select slot length--</option><option value="30/45/8093">30</option><option value="45/138/8094">45</option><option value="60/305/8095">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7904" name="mytvslots[]" value="7904/DiamondFM/2020-04-15/08:30:00 - 09:00:00">
                                                                            <label class="" for="advert7904">DiamondFM</label></td> <td>2020-04-15</td>
                                                                        <td>08:30:00 - 09:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7904" class="selectslots" name="selectslots[]" onchange="getRate(7904);"><option disabled="" selected="">--select slot length--</option><option value="30/65/7904">30</option><option value="45/243/7905">45</option><option value="60/427/7906">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8165" name="mytvslots[]" value="8165/Radford FM/2020-04-15/11:00:00 - 12:00:00">
                                                                            <label class="" for="advert8165">Radford FM</label></td> <td>2020-04-15</td>
                                                                        <td>11:00:00 - 12:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8165" class="selectslots" name="selectslots[]" onchange="getRate(8165);"><option disabled="" selected="">--select slot length--</option><option value="30/88/8165">30</option><option value="45/126/8166">45</option><option value="60/376/8167">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8114" name="mytvslots[]" value="8114/Radio Freed FM/2020-04-15/12:30:00 - 13:00:00">
                                                                            <label class="" for="advert8114">Radio Freed FM</label></td> <td>2020-04-15</td>
                                                                        <td>12:30:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8114" class="selectslots" name="selectslots[]" onchange="getRate(8114);"><option disabled="" selected="">--select slot length--</option><option value="30/91/8114">30</option><option value="45/121/8115">45</option><option value="60/266/8116">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8671" name="mytvslots[]" value="8671/Goodlife FM/2020-04-15/13:00:00 - 14:00:00">
                                                                            <label class="" for="advert8671">Goodlife FM</label></td> <td>2020-04-15</td>
                                                                        <td>13:00:00 - 14:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8671" class="selectslots" name="selectslots[]" onchange="getRate(8671);"><option disabled="" selected="">--select slot length--</option><option value="30/93/8671">30</option><option value="45/250/8672">45</option><option value="60/494/8673">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8213" name="mytvslots[]" value="8213/Radio Progress FM/2020-04-15/13:30:00 - 14:30:00">
                                                                            <label class="" for="advert8213">Radio Progress FM</label></td> <td>2020-04-15</td>
                                                                        <td>13:30:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8213" class="selectslots" name="selectslots[]" onchange="getRate(8213);"><option disabled="" selected="">--select slot length--</option><option value="30/72/8213">30</option><option value="45/171/8214">45</option><option value="60/350/8215">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8656" name="mytvslots[]" value="8656/Sunrise FM/2020-04-15/16:30:00 - 17:00:00">
                                                                            <label class="" for="advert8656">Sunrise FM</label></td> <td>2020-04-15</td>
                                                                        <td>16:30:00 - 17:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8656" class="selectslots" name="selectslots[]" onchange="getRate(8656);"><option disabled="" selected="">--select slot length--</option><option value="30/47/8656">30</option><option value="45/161/8657">45</option><option value="60/350/8658">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8246" name="mytvslots[]" value="8246/Word FM/2020-04-15/20:00:00 - 21:00:00">
                                                                            <label class="" for="advert8246">Word FM</label></td> <td>2020-04-15</td>
                                                                        <td>20:00:00 - 21:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8246" class="selectslots" name="selectslots[]" onchange="getRate(8246);"><option disabled="" selected="">--select slot length--</option><option value="30/38/8246">30</option><option value="45/109/8247">45</option><option value="60/425/8248">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-16"><div class="popWrap" id="16" onclick="openThis(16);"><span class="daydate">16</span><span title="Available Radio Slots" class="open-button radio">12</span></div><div id="ad16"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7832" name="mytvslots[]" value="7832/MelodyFM /2020-04-16/00:00:00 - 00:30:00">
                                                                            <label class="" for="advert7832">MelodyFM </label></td> <td>2020-04-16</td>
                                                                        <td>00:00:00 - 00:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7832" class="selectslots" name="selectslots[]" onchange="getRate(7832);"><option disabled="" selected="">--select slot length--</option><option value="30/42/7832">30</option><option value="45/189/7833">45</option><option value="60/384/7834">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8036" name="mytvslots[]" value="8036/Hope FM/2020-04-16/00:30:00 - 01:30:00">
                                                                            <label class="" for="advert8036">Hope FM</label></td> <td>2020-04-16</td>
                                                                        <td>00:30:00 - 01:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8036" class="selectslots" name="selectslots[]" onchange="getRate(8036);"><option disabled="" selected="">--select slot length--</option><option value="30/32/8036">30</option><option value="45/116/8037">45</option><option value="60/434/8038">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8195" name="mytvslots[]" value="8195/Radio Progress FM/2020-04-16/01:00:00 - 01:30:00">
                                                                            <label class="" for="advert8195">Radio Progress FM</label></td> <td>2020-04-16</td>
                                                                        <td>01:00:00 - 01:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8195" class="selectslots" name="selectslots[]" onchange="getRate(8195);"><option disabled="" selected="">--select slot length--</option><option value="30/44/8195">30</option><option value="45/246/8196">45</option><option value="60/292/8197">60</option><option value="30/88/8375">30</option><option value="45/140/8376">45</option><option value="60/311/8377">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7784" name="mytvslots[]" value="7784/SkyPowerFM /2020-04-16/06:30:00 - 07:30:00">
                                                                            <label class="" for="advert7784">SkyPowerFM </label></td> <td>2020-04-16</td>
                                                                        <td>06:30:00 - 07:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7784" class="selectslots" name="selectslots[]" onchange="getRate(7784);"><option disabled="" selected="">--select slot length--</option><option value="30/49/7784">30</option><option value="45/208/7785">45</option><option value="60/344/7786">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7559" name="mytvslots[]" value="7559/SweetMelodiesFM /2020-04-16/07:00:00 - 07:30:00">
                                                                            <label class="" for="advert7559">SweetMelodiesFM </label></td> <td>2020-04-16</td>
                                                                        <td>07:00:00 - 07:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7559" class="selectslots" name="selectslots[]" onchange="getRate(7559);"><option disabled="" selected="">--select slot length--</option><option value="30/52/7559">30</option><option value="45/126/7560">45</option><option value="60/416/7561">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8333" name="mytvslots[]" value="8333/Dinpa FM/2020-04-16/08:00:00 - 08:30:00">
                                                                            <label class="" for="advert8333">Dinpa FM</label></td> <td>2020-04-16</td>
                                                                        <td>08:00:00 - 08:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8333" class="selectslots" name="selectslots[]" onchange="getRate(8333);"><option disabled="" selected="">--select slot length--</option><option value="30/88/8333">30</option><option value="45/183/8334">45</option><option value="60/343/8335">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8626" name="mytvslots[]" value="8626/Eastern FM/2020-04-16/09:00:00 - 09:30:00">
                                                                            <label class="" for="advert8626">Eastern FM</label></td> <td>2020-04-16</td>
                                                                        <td>09:00:00 - 09:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8626" class="selectslots" name="selectslots[]" onchange="getRate(8626);"><option disabled="" selected="">--select slot length--</option><option value="30/65/8626">30</option><option value="45/172/8627">45</option><option value="60/323/8628">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8560" name="mytvslots[]" value="8560/Sompa FM/2020-04-16/11:00:00 - 12:00:00">
                                                                            <label class="" for="advert8560">Sompa FM</label></td> <td>2020-04-16</td>
                                                                        <td>11:00:00 - 12:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8560" class="selectslots" name="selectslots[]" onchange="getRate(8560);"><option disabled="" selected="">--select slot length--</option><option value="30/53/8560">30</option><option value="45/250/8561">45</option><option value="60/369/8562">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7997" name="mytvslots[]" value="7997/Light FM/2020-04-16/12:00:00 - 13:00:00">
                                                                            <label class="" for="advert7997">Light FM</label></td> <td>2020-04-16</td>
                                                                        <td>12:00:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7997" class="selectslots" name="selectslots[]" onchange="getRate(7997);"><option disabled="" selected="">--select slot length--</option><option value="30/360/7997">30</option><option value="45/224/7998">45</option><option value="60/387/7999">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7730" name="mytvslots[]" value="7730/Rok FM/2020-04-16/14:00:00 - 15:00:00">
                                                                            <label class="" for="advert7730">Rok FM</label></td> <td>2020-04-16</td>
                                                                        <td>14:00:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7730" class="selectslots" name="selectslots[]" onchange="getRate(7730);"><option disabled="" selected="">--select slot length--</option><option value="30/55/7730">30</option><option value="45/126/7731">45</option><option value="60/460/7732">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8000" name="mytvslots[]" value="8000/Holy FM/2020-04-16/16:30:00 - 17:00:00">
                                                                            <label class="" for="advert8000">Holy FM</label></td> <td>2020-04-16</td>
                                                                        <td>16:30:00 - 17:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8000" class="selectslots" name="selectslots[]" onchange="getRate(8000);"><option disabled="" selected="">--select slot length--</option><option value="30/64/8000">30</option><option value="45/189/8001">45</option><option value="60/396/8002">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7922" name="mytvslots[]" value="7922/NorthStar RadioFM/2020-04-16/18:00:00 - 18:30:00">
                                                                            <label class="" for="advert7922">NorthStar RadioFM</label></td> <td>2020-04-16</td>
                                                                        <td>18:00:00 - 18:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7922" class="selectslots" name="selectslots[]" onchange="getRate(7922);"><option disabled="" selected="">--select slot length--</option><option value="30/61/7922">30</option><option value="45/116/7923">45</option><option value="60/435/7924">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-17"><div class="popWrap" id="17" onclick="openThis(17);"><span class="daydate">17</span><span title="Available Radio Slots" class="open-button radio">13</span></div><div id="ad17"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8396" name="mytvslots[]" value="8396/Space FM/2020-04-17/02:00:00 - 03:00:00">
                                                                            <label class="" for="advert8396">Space FM</label></td> <td>2020-04-17</td>
                                                                        <td>02:00:00 - 03:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8396" class="selectslots" name="selectslots[]" onchange="getRate(8396);"><option disabled="" selected="">--select slot length--</option><option value="30/69/8396">30</option><option value="45/141/8397">45</option><option value="60/350/8398">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8231" name="mytvslots[]" value="8231/Word FM/2020-04-17/05:00:00 - 05:30:00">
                                                                            <label class="" for="advert8231">Word FM</label></td> <td>2020-04-17</td>
                                                                        <td>05:00:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8231" class="selectslots" name="selectslots[]" onchange="getRate(8231);"><option disabled="" selected="">--select slot length--</option><option value="30/47/8231">30</option><option value="45/211/8232">45</option><option value="60/485/8233">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7601" name="mytvslots[]" value="7601/LuvFM /2020-04-17/06:30:00 - 07:00:00">
                                                                            <label class="" for="advert7601">LuvFM </label></td> <td>2020-04-17</td>
                                                                        <td>06:30:00 - 07:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7601" class="selectslots" name="selectslots[]" onchange="getRate(7601);"><option disabled="" selected="">--select slot length--</option><option value="30/92/7601">30</option><option value="45/174/7602">45</option><option value="60/498/7603">60</option><option value="30/58/8303">30</option><option value="45/114/8304">45</option><option value="60/321/8305">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8688" name="mytvslots[]" value="8688/Goodlife FM/2020-04-17/10:00:00 - 11:00:00">
                                                                            <label class="" for="advert8688">Goodlife FM</label></td> <td>2020-04-17</td>
                                                                        <td>10:00:00 - 11:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8688" class="selectslots" name="selectslots[]" onchange="getRate(8688);"><option disabled="" selected="">--select slot length--</option><option value="30/60/8688">30</option><option value="45/193/8689">45</option><option value="60/376/8690">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7781" name="mytvslots[]" value="7781/SkyPowerFM /2020-04-17/13:30:00 - 14:30:00">
                                                                            <label class="" for="advert7781">SkyPowerFM </label></td> <td>2020-04-17</td>
                                                                        <td>13:30:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7781" class="selectslots" name="selectslots[]" onchange="getRate(7781);"><option disabled="" selected="">--select slot length--</option><option value="30/57/7781">30</option><option value="45/138/7782">45</option><option value="60/389/7783">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7485" name="mytvslots[]" value="7485/Choice FM/2020-04-17/15:00:00 - 16:00:00">
                                                                            <label class="" for="advert7485">Choice FM</label></td> <td>2020-04-17</td>
                                                                        <td>15:00:00 - 16:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7485" class="selectslots" name="selectslots[]" onchange="getRate(7485);"><option disabled="" selected="">--select slot length--</option><option value="30/38/7485">30</option><option value="45/232/7486">45</option><option value="60/369/7487">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7628" name="mytvslots[]" value="7628/KessbenFM /2020-04-17/15:30:00 - 16:00:00">
                                                                            <label class="" for="advert7628">KessbenFM </label></td> <td>2020-04-17</td>
                                                                        <td>15:30:00 - 16:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7628" class="selectslots" name="selectslots[]" onchange="getRate(7628);"><option disabled="" selected="">--select slot length--</option><option value="30/70/7628">30</option><option value="45/215/7629">45</option><option value="60/464/7630">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8048" name="mytvslots[]" value="8048/Hope FM/2020-04-17/16:00:00 - 16:30:00">
                                                                            <label class="" for="advert8048">Hope FM</label></td> <td>2020-04-17</td>
                                                                        <td>16:00:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8048" class="selectslots" name="selectslots[]" onchange="getRate(8048);"><option disabled="" selected="">--select slot length--</option><option value="30/53/8048">30</option><option value="45/245/8049">45</option><option value="60/351/8050">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8488" name="mytvslots[]" value="8488/Solar FM/2020-04-17/17:30:00 - 18:00:00">
                                                                            <label class="" for="advert8488">Solar FM</label></td> <td>2020-04-17</td>
                                                                        <td>17:30:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8488" class="selectslots" name="selectslots[]" onchange="getRate(8488);"><option disabled="" selected="">--select slot length--</option><option value="30/36/8488">30</option><option value="45/243/8489">45</option><option value="60/381/8490">60</option><option value="30/67/8608">30</option><option value="45/120/8609">45</option><option value="60/490/8610">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8153" name="mytvslots[]" value="8153/Radford FM/2020-04-17/18:00:00 - 18:30:00">
                                                                            <label class="" for="advert8153">Radford FM</label></td> <td>2020-04-17</td>
                                                                        <td>18:00:00 - 18:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8153" class="selectslots" name="selectslots[]" onchange="getRate(8153);"><option disabled="" selected="">--select slot length--</option><option value="30/58/8153">30</option><option value="45/176/8154">45</option><option value="60/329/8155">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8712" name="mytvslots[]" value="8712/Vision FM/2020-04-17/18:30:00 - 19:00:00">
                                                                            <label class="" for="advert8712">Vision FM</label></td> <td>2020-04-17</td>
                                                                        <td>18:30:00 - 19:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8712" class="selectslots" name="selectslots[]" onchange="getRate(8712);"><option disabled="" selected="">--select slot length--</option><option value="30/66/8712">30</option><option value="45/210/8713">45</option><option value="60/433/8714">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8387" name="mytvslots[]" value="8387/Space FM/2020-04-17/20:00:00 - 20:30:00">
                                                                            <label class="" for="advert8387">Space FM</label></td> <td>2020-04-17</td>
                                                                        <td>20:00:00 - 20:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8387" class="selectslots" name="selectslots[]" onchange="getRate(8387);"><option disabled="" selected="">--select slot length--</option><option value="30/108/7389">30</option><option value="45/293/7390">45</option><option value="60/771/7391">60</option><option value="30/67/8387">30</option><option value="45/142/8388">45</option><option value="60/454/8389">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8141" name="mytvslots[]" value="8141/Radford FM/2020-04-17/22:30:00 - 23:00:00">
                                                                            <label class="" for="advert8141">Radford FM</label></td> <td>2020-04-17</td>
                                                                        <td>22:30:00 - 23:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8141" class="selectslots" name="selectslots[]" onchange="getRate(8141);"><option disabled="" selected="">--select slot length--</option><option value="30/40/8141">30</option><option value="45/147/8142">45</option><option value="60/500/8143">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-18"><div class="popWrap" id="18" onclick="openThis(18);"><span class="daydate">18</span><span title="Available Radio Slots" class="open-button radio">11</span></div><div id="ad18"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8282" name="mytvslots[]" value="8282/Rock FM/2020-04-18/02:00:00 - 03:00:00">
                                                                            <label class="" for="advert8282">Rock FM</label></td> <td>2020-04-18</td>
                                                                        <td>02:00:00 - 03:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8282" class="selectslots" name="selectslots[]" onchange="getRate(8282);"><option disabled="" selected="">--select slot length--</option><option value="30/74/8282">30</option><option value="45/148/8283">45</option><option value="60/261/8284">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8123" name="mytvslots[]" value="8123/Radio Freed FM/2020-04-18/08:00:00 - 09:00:00">
                                                                            <label class="" for="advert8123">Radio Freed FM</label></td> <td>2020-04-18</td>
                                                                        <td>08:00:00 - 09:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8123" class="selectslots" name="selectslots[]" onchange="getRate(8123);"><option disabled="" selected="">--select slot length--</option><option value="30/76/8123">30</option><option value="45/217/8124">45</option><option value="60/452/8125">60</option><option value="30/51/8168">30</option><option value="45/237/8169">45</option><option value="60/482/8170">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8003" name="mytvslots[]" value="8003/Holy FM/2020-04-18/09:00:00 - 10:00:00">
                                                                            <label class="" for="advert8003">Holy FM</label></td> <td>2020-04-18</td>
                                                                        <td>09:00:00 - 10:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8003" class="selectslots" name="selectslots[]" onchange="getRate(8003);"><option disabled="" selected="">--select slot length--</option><option value="30/90/8003">30</option><option value="45/176/8004">45</option><option value="60/429/8005">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7461" name="mytvslots[]" value="7461/Prime FM/2020-04-18/09:30:00 - 10:00:00">
                                                                            <label class="" for="advert7461">Prime FM</label></td> <td>2020-04-18</td>
                                                                        <td>09:30:00 - 10:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7461" class="selectslots" name="selectslots[]" onchange="getRate(7461);"><option disabled="" selected="">--select slot length--</option><option value="30/53/7461">30</option><option value="45/187/7462">45</option><option value="60/304/7463">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8111" name="mytvslots[]" value="8111/Victory FM/2020-04-18/10:30:00 - 11:30:00">
                                                                            <label class="" for="advert8111">Victory FM</label></td> <td>2020-04-18</td>
                                                                        <td>10:30:00 - 11:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8111" class="selectslots" name="selectslots[]" onchange="getRate(8111);"><option disabled="" selected="">--select slot length--</option><option value="30/80/8111">30</option><option value="45/133/8112">45</option><option value="60/323/8113">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8581" name="mytvslots[]" value="8581/Sompa FM/2020-04-18/12:30:00 - 13:30:00">
                                                                            <label class="" for="advert8581">Sompa FM</label></td> <td>2020-04-18</td>
                                                                        <td>12:30:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8581" class="selectslots" name="selectslots[]" onchange="getRate(8581);"><option disabled="" selected="">--select slot length--</option><option value="30/49/8581">30</option><option value="45/225/8582">45</option><option value="60/436/8583">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7814" name="mytvslots[]" value="7814/RadioMaxxFM/2020-04-18/13:30:00 - 14:00:00">
                                                                            <label class="" for="advert7814">RadioMaxxFM</label></td> <td>2020-04-18</td>
                                                                        <td>13:30:00 - 14:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7814" class="selectslots" name="selectslots[]" onchange="getRate(7814);"><option disabled="" selected="">--select slot length--</option><option value="30/97/7814">30</option><option value="45/226/7815">45</option><option value="60/274/7816">60</option><option value="30/92/8650">30</option><option value="45/149/8651">45</option><option value="60/464/8652">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8530" name="mytvslots[]" value="8530/Pink FM/2020-04-18/15:00:00 - 16:00:00">
                                                                            <label class="" for="advert8530">Pink FM</label></td> <td>2020-04-18</td>
                                                                        <td>15:00:00 - 16:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8530" class="selectslots" name="selectslots[]" onchange="getRate(8530);"><option disabled="" selected="">--select slot length--</option><option value="30/71/8530">30</option><option value="45/221/8531">45</option><option value="60/347/8532">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7598" name="mytvslots[]" value="7598/LuvFM /2020-04-18/17:00:00 - 18:00:00">
                                                                            <label class="" for="advert7598">LuvFM </label></td> <td>2020-04-18</td>
                                                                        <td>17:00:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7598" class="selectslots" name="selectslots[]" onchange="getRate(7598);"><option disabled="" selected="">--select slot length--</option><option value="30/85/7598">30</option><option value="45/211/7599">45</option><option value="60/309/7600">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7799" name="mytvslots[]" value="7799/RadioMaxxFM/2020-04-18/18:00:00 - 19:00:00">
                                                                            <label class="" for="advert7799">RadioMaxxFM</label></td> <td>2020-04-18</td>
                                                                        <td>18:00:00 - 19:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7799" class="selectslots" name="selectslots[]" onchange="getRate(7799);"><option disabled="" selected="">--select slot length--</option><option value="30/77/7799">30</option><option value="45/247/7800">45</option><option value="60/390/7801">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7583" name="mytvslots[]" value="7583/LuvFM /2020-04-18/19:00:00 - 19:30:00">
                                                                            <label class="" for="advert7583">LuvFM </label></td> <td>2020-04-18</td>
                                                                        <td>19:00:00 - 19:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7583" class="selectslots" name="selectslots[]" onchange="getRate(7583);"><option disabled="" selected="">--select slot length--</option><option value="30/40/7583">30</option><option value="45/132/7584">45</option><option value="60/295/7585">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td></tr><tr><td border="1" class="day available" rel="2020-04-19"><div class="popWrap" id="19" onclick="openThis(19);"><span class="daydate">19</span><span title="Available Radio Slots" class="open-button radio">14</span></div><div id="ad19"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7613" name="mytvslots[]" value="7613/KessbenFM /2020-04-19/00:00:00 - 01:00:00">
                                                                            <label class="" for="advert7613">KessbenFM </label></td> <td>2020-04-19</td>
                                                                        <td>00:00:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7613" class="selectslots" name="selectslots[]" onchange="getRate(7613);"><option disabled="" selected="">--select slot length--</option><option value="30/61/7613">30</option><option value="45/189/7614">45</option><option value="60/420/7615">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8593" name="mytvslots[]" value="8593/Emak FM/2020-04-19/00:30:00 - 01:00:00">
                                                                            <label class="" for="advert8593">Emak FM</label></td> <td>2020-04-19</td>
                                                                        <td>00:30:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8593" class="selectslots" name="selectslots[]" onchange="getRate(8593);"><option disabled="" selected="">--select slot length--</option><option value="30/81/8593">30</option><option value="45/114/8594">45</option><option value="60/381/8595">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8063" name="mytvslots[]" value="8063/Jubilee Radio FM/2020-04-19/01:30:00 - 02:30:00">
                                                                            <label class="" for="advert8063">Jubilee Radio FM</label></td> <td>2020-04-19</td>
                                                                        <td>01:30:00 - 02:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8063" class="selectslots" name="selectslots[]" onchange="getRate(8063);"><option disabled="" selected="">--select slot length--</option><option value="30/42/8063">30</option><option value="45/131/8064">45</option><option value="60/426/8065">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8420" name="mytvslots[]" value="8420/Star FM/2020-04-19/02:00:00 - 03:00:00">
                                                                            <label class="" for="advert8420">Star FM</label></td> <td>2020-04-19</td>
                                                                        <td>02:00:00 - 03:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8420" class="selectslots" name="selectslots[]" onchange="getRate(8420);"><option disabled="" selected="">--select slot length--</option><option value="30/100/8420">30</option><option value="45/211/8421">45</option><option value="60/377/8422">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8381" name="mytvslots[]" value="8381/Space FM/2020-04-19/03:30:00 - 04:00:00">
                                                                            <label class="" for="advert8381">Space FM</label></td> <td>2020-04-19</td>
                                                                        <td>03:30:00 - 04:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8381" class="selectslots" name="selectslots[]" onchange="getRate(8381);"><option disabled="" selected="">--select slot length--</option><option value="30/96/8381">30</option><option value="45/131/8382">45</option><option value="60/410/8383">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7736" name="mytvslots[]" value="7736/RadioSilverFM/2020-04-19/05:30:00 - 06:30:00">
                                                                            <label class="" for="advert7736">RadioSilverFM</label></td> <td>2020-04-19</td>
                                                                        <td>05:30:00 - 06:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7736" class="selectslots" name="selectslots[]" onchange="getRate(7736);"><option disabled="" selected="">--select slot length--</option><option value="30/89/7736">30</option><option value="45/177/7737">45</option><option value="60/341/7738">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8718" name="mytvslots[]" value="8718/Vision FM/2020-04-19/07:30:00 - 08:00:00">
                                                                            <label class="" for="advert8718">Vision FM</label></td> <td>2020-04-19</td>
                                                                        <td>07:30:00 - 08:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8718" class="selectslots" name="selectslots[]" onchange="getRate(8718);"><option disabled="" selected="">--select slot length--</option><option value="30/32/8718">30</option><option value="45/103/8719">45</option><option value="60/470/8720">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8596" name="mytvslots[]" value="8596/Emak FM/2020-04-19/09:00:00 - 10:00:00">
                                                                            <label class="" for="advert8596">Emak FM</label></td> <td>2020-04-19</td>
                                                                        <td>09:00:00 - 10:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8596" class="selectslots" name="selectslots[]" onchange="getRate(8596);"><option disabled="" selected="">--select slot length--</option><option value="30/67/8596">30</option><option value="45/200/8597">45</option><option value="60/293/8598">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7556" name="mytvslots[]" value="7556/SweetMelodiesFM /2020-04-19/10:00:00 - 11:00:00">
                                                                            <label class="" for="advert7556">SweetMelodiesFM </label></td> <td>2020-04-19</td>
                                                                        <td>10:00:00 - 11:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7556" class="selectslots" name="selectslots[]" onchange="getRate(7556);"><option disabled="" selected="">--select slot length--</option><option value="30/55/7556">30</option><option value="45/219/7557">45</option><option value="60/319/7558">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7994" name="mytvslots[]" value="7994/Light FM/2020-04-19/12:00:00 - 13:00:00">
                                                                            <label class="" for="advert7994">Light FM</label></td> <td>2020-04-19</td>
                                                                        <td>12:00:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7994" class="selectslots" name="selectslots[]" onchange="getRate(7994);"><option disabled="" selected="">--select slot length--</option><option value="30/78/7994">30</option><option value="45/207/7995">45</option><option value="60/401/7996">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8270" name="mytvslots[]" value="8270/A1 Radio FM/2020-04-19/13:00:00 - 14:00:00">
                                                                            <label class="" for="advert8270">A1 Radio FM</label></td> <td>2020-04-19</td>
                                                                        <td>13:00:00 - 14:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8270" class="selectslots" name="selectslots[]" onchange="getRate(8270);"><option disabled="" selected="">--select slot length--</option><option value="30/35/8270">30</option><option value="45/100/8271">45</option><option value="60/299/8272">60</option><option value="30/47/8324">30</option><option value="45/114/8325">45</option><option value="60/265/8326">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8222" name="mytvslots[]" value="8222/Word FM/2020-04-19/14:00:00 - 14:30:00">
                                                                            <label class="" for="advert8222">Word FM</label></td> <td>2020-04-19</td>
                                                                        <td>14:00:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8222" class="selectslots" name="selectslots[]" onchange="getRate(8222);"><option disabled="" selected="">--select slot length--</option><option value="30/82/8222">30</option><option value="45/103/8223">45</option><option value="60/266/8224">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8587" name="mytvslots[]" value="8587/Emak FM/2020-04-19/15:00:00 - 16:00:00">
                                                                            <label class="" for="advert8587">Emak FM</label></td> <td>2020-04-19</td>
                                                                        <td>15:00:00 - 16:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8587" class="selectslots" name="selectslots[]" onchange="getRate(8587);"><option disabled="" selected="">--select slot length--</option><option value="30/71/8587">30</option><option value="45/138/8588">45</option><option value="60/258/8589">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7479" name="mytvslots[]" value="7479/Choice FM/2020-04-19/18:00:00 - 18:30:00">
                                                                            <label class="" for="advert7479">Choice FM</label></td> <td>2020-04-19</td>
                                                                        <td>18:00:00 - 18:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7479" class="selectslots" name="selectslots[]" onchange="getRate(7479);"><option disabled="" selected="">--select slot length--</option><option value="30/45/7479">30</option><option value="45/198/7480">45</option><option value="60/429/7481">60</option><option value="30/47/7637">30</option><option value="45/155/7638">45</option><option value="60/391/7639">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-20"><div class="popWrap" id="20" onclick="openThis(20);"><span class="daydate">20</span><span title="Available Radio Slots" class="open-button radio">25</span></div><div id="ad20"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7700" name="mytvslots[]" value="7700/Boss FM/2020-04-20/00:00:00 - 00:30:00">
                                                                            <label class="" for="advert7700">Boss FM</label></td> <td>2020-04-20</td>
                                                                        <td>00:00:00 - 00:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7700" class="selectslots" name="selectslots[]" onchange="getRate(7700);"><option disabled="" selected="">--select slot length--</option><option value="30/48/7700">30</option><option value="45/122/7701">45</option><option value="60/311/7702">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7937" name="mytvslots[]" value="7937/NorthStar RadioFM/2020-04-20/02:00:00 - 02:30:00">
                                                                            <label class="" for="advert7937">NorthStar RadioFM</label></td> <td>2020-04-20</td>
                                                                        <td>02:00:00 - 02:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7937" class="selectslots" name="selectslots[]" onchange="getRate(7937);"><option disabled="" selected="">--select slot length--</option><option value="30/48/7937">30</option><option value="45/117/7938">45</option><option value="60/327/7939">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8243" name="mytvslots[]" value="8243/Word FM/2020-04-20/02:30:00 - 03:00:00">
                                                                            <label class="" for="advert8243">Word FM</label></td> <td>2020-04-20</td>
                                                                        <td>02:30:00 - 03:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8243" class="selectslots" name="selectslots[]" onchange="getRate(8243);"><option disabled="" selected="">--select slot length--</option><option value="30/35/8243">30</option><option value="45/231/8244">45</option><option value="60/301/8245">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7541" name="mytvslots[]" value="7541/AsempaFM /2020-04-20/03:00:00 - 04:00:00">
                                                                            <label class="" for="advert7541">AsempaFM </label></td> <td>2020-04-20</td>
                                                                        <td>03:00:00 - 04:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7541" class="selectslots" name="selectslots[]" onchange="getRate(7541);"><option disabled="" selected="">--select slot length--</option><option value="30/76/7541">30</option><option value="45/157/7542">45</option><option value="60/408/7543">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8677" name="mytvslots[]" value="8677/Goodlife FM/2020-04-20/04:00:00 - 05:00:00">
                                                                            <label class="" for="advert8677">Goodlife FM</label></td> <td>2020-04-20</td>
                                                                        <td>04:00:00 - 05:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8677" class="selectslots" name="selectslots[]" onchange="getRate(8677);"><option disabled="" selected="">--select slot length--</option><option value="30/95/8677">30</option><option value="45/199/8678">45</option><option value="60/363/8679">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8632" name="mytvslots[]" value="8632/Eastern FM/2020-04-20/04:30:00 - 05:30:00">
                                                                            <label class="" for="advert8632">Eastern FM</label></td> <td>2020-04-20</td>
                                                                        <td>04:30:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8632" class="selectslots" name="selectslots[]" onchange="getRate(8632);"><option disabled="" selected="">--select slot length--</option><option value="30/42/8632">30</option><option value="45/164/8633">45</option><option value="60/315/8634">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8354" name="mytvslots[]" value="8354/Anapua FM/2020-04-20/05:00:00 - 05:30:00">
                                                                            <label class="" for="advert8354">Anapua FM</label></td> <td>2020-04-20</td>
                                                                        <td>05:00:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8354" class="selectslots" name="selectslots[]" onchange="getRate(8354);"><option disabled="" selected="">--select slot length--</option><option value="30/51/8354">30</option><option value="45/247/8355">45</option><option value="60/320/8356">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8207" name="mytvslots[]" value="8207/Radio Progress FM/2020-04-20/05:30:00 - 06:00:00">
                                                                            <label class="" for="advert8207">Radio Progress FM</label></td> <td>2020-04-20</td>
                                                                        <td>05:30:00 - 06:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8207" class="selectslots" name="selectslots[]" onchange="getRate(8207);"><option disabled="" selected="">--select slot length--</option><option value="30/37/8207">30</option><option value="45/226/8208">45</option><option value="60/344/8209">60</option><option value="30/94/8599">30</option><option value="45/234/8600">45</option><option value="60/272/8601">60</option><option value="30/94/8662">30</option><option value="45/221/8663">45</option><option value="60/465/8664">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7880" name="mytvslots[]" value="7880/Zaa RadioFM/2020-04-20/06:30:00 - 07:30:00">
                                                                            <label class="" for="advert7880">Zaa RadioFM</label></td> <td>2020-04-20</td>
                                                                        <td>06:30:00 - 07:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7880" class="selectslots" name="selectslots[]" onchange="getRate(7880);"><option disabled="" selected="">--select slot length--</option><option value="30/48/7880">30</option><option value="45/246/7881">45</option><option value="60/321/7882">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7526" name="mytvslots[]" value="7526/AsempaFM /2020-04-20/10:00:00 - 10:30:00">
                                                                            <label class="" for="advert7526">AsempaFM </label></td> <td>2020-04-20</td>
                                                                        <td>10:00:00 - 10:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7526" class="selectslots" name="selectslots[]" onchange="getRate(7526);"><option disabled="" selected="">--select slot length--</option><option value="30/50/7526">30</option><option value="45/130/7527">45</option><option value="60/464/7528">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8294" name="mytvslots[]" value="8294/Rock FM/2020-04-20/11:30:00 - 12:00:00">
                                                                            <label class="" for="advert8294">Rock FM</label></td> <td>2020-04-20</td>
                                                                        <td>11:30:00 - 12:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8294" class="selectslots" name="selectslots[]" onchange="getRate(8294);"><option disabled="" selected="">--select slot length--</option><option value="30/90/8294">30</option><option value="45/105/8295">45</option><option value="60/365/8296">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7826" name="mytvslots[]" value="7826/MelodyFM /2020-04-20/13:00:00 - 14:00:00">
                                                                            <label class="" for="advert7826">MelodyFM </label></td> <td>2020-04-20</td>
                                                                        <td>13:00:00 - 14:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7826" class="selectslots" name="selectslots[]" onchange="getRate(7826);"><option disabled="" selected="">--select slot length--</option><option value="30/46/7826">30</option><option value="45/158/7827">45</option><option value="60/340/7828">60</option><option value="30/68/8327">30</option><option value="45/210/8328">45</option><option value="60/433/8329">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7616" name="mytvslots[]" value="7616/KessbenFM /2020-04-20/14:00:00 - 14:30:00">
                                                                            <label class="" for="advert7616">KessbenFM </label></td> <td>2020-04-20</td>
                                                                        <td>14:00:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7616" class="selectslots" name="selectslots[]" onchange="getRate(7616);"><option disabled="" selected="">--select slot length--</option><option value="30/61/7616">30</option><option value="45/227/7617">45</option><option value="60/284/7618">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8321" name="mytvslots[]" value="8321/Radio Gurune FM/2020-04-20/15:00:00 - 16:00:00">
                                                                            <label class="" for="advert8321">Radio Gurune FM</label></td> <td>2020-04-20</td>
                                                                        <td>15:00:00 - 16:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8321" class="selectslots" name="selectslots[]" onchange="getRate(8321);"><option disabled="" selected="">--select slot length--</option><option value="30/66/8321">30</option><option value="45/123/8322">45</option><option value="60/356/8323">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8264" name="mytvslots[]" value="8264/A1 Radio FM/2020-04-20/15:30:00 - 16:30:00">
                                                                            <label class="" for="advert8264">A1 Radio FM</label></td> <td>2020-04-20</td>
                                                                        <td>15:30:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8264" class="selectslots" name="selectslots[]" onchange="getRate(8264);"><option disabled="" selected="">--select slot length--</option><option value="30/62/8264">30</option><option value="45/143/8265">45</option><option value="60/307/8266">60</option><option value="30/84/8500">30</option><option value="45/186/8501">45</option><option value="60/435/8502">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7823" name="mytvslots[]" value="7823/MelodyFM /2020-04-20/16:30:00 - 17:00:00">
                                                                            <label class="" for="advert7823">MelodyFM </label></td> <td>2020-04-20</td>
                                                                        <td>16:30:00 - 17:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7823" class="selectslots" name="selectslots[]" onchange="getRate(7823);"><option disabled="" selected="">--select slot length--</option><option value="30/83/7823">30</option><option value="45/184/7824">45</option><option value="60/407/7825">60</option><option value="30/78/8584">30</option><option value="45/203/8585">45</option><option value="60/316/8586">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7553" name="mytvslots[]" value="7553/AsempaFM /2020-04-20/17:00:00 - 18:00:00">
                                                                            <label class="" for="advert7553">AsempaFM </label></td> <td>2020-04-20</td>
                                                                        <td>17:00:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7553" class="selectslots" name="selectslots[]" onchange="getRate(7553);"><option disabled="" selected="">--select slot length--</option><option value="30/67/7553">30</option><option value="45/106/7554">45</option><option value="60/260/7555">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7682" name="mytvslots[]" value="7682/Fox FM/2020-04-20/17:30:00 - 18:30:00">
                                                                            <label class="" for="advert7682">Fox FM</label></td> <td>2020-04-20</td>
                                                                        <td>17:30:00 - 18:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7682" class="selectslots" name="selectslots[]" onchange="getRate(7682);"><option disabled="" selected="">--select slot length--</option><option value="30/92/7682">30</option><option value="45/151/7683">45</option><option value="60/284/7684">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7979" name="mytvslots[]" value="7979/Light FM/2020-04-20/19:30:00 - 20:30:00">
                                                                            <label class="" for="advert7979">Light FM</label></td> <td>2020-04-20</td>
                                                                        <td>19:30:00 - 20:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7979" class="selectslots" name="selectslots[]" onchange="getRate(7979);"><option disabled="" selected="">--select slot length--</option><option value="30/63/7979">30</option><option value="45/236/7980">45</option><option value="60/441/7981">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8159" name="mytvslots[]" value="8159/Radford FM/2020-04-20/20:00:00 - 21:00:00">
                                                                            <label class="" for="advert8159">Radford FM</label></td> <td>2020-04-20</td>
                                                                        <td>20:00:00 - 21:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8159" class="selectslots" name="selectslots[]" onchange="getRate(8159);"><option disabled="" selected="">--select slot length--</option><option value="30/33/8159">30</option><option value="45/124/8160">45</option><option value="60/268/8161">60</option><option value="30/93/8390">30</option><option value="45/229/8391">45</option><option value="60/353/8392">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8411" name="mytvslots[]" value="8411/Star FM/2020-04-20/20:30:00 - 21:00:00">
                                                                            <label class="" for="advert8411">Star FM</label></td> <td>2020-04-20</td>
                                                                        <td>20:30:00 - 21:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8411" class="selectslots" name="selectslots[]" onchange="getRate(8411);"><option disabled="" selected="">--select slot length--</option><option value="30/36/8411">30</option><option value="45/145/8412">45</option><option value="60/325/8413">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8309" name="mytvslots[]" value="8309/Radio Gurune FM/2020-04-20/21:00:00 - 21:30:00">
                                                                            <label class="" for="advert8309">Radio Gurune FM</label></td> <td>2020-04-20</td>
                                                                        <td>21:00:00 - 21:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8309" class="selectslots" name="selectslots[]" onchange="getRate(8309);"><option disabled="" selected="">--select slot length--</option><option value="30/53/8309">30</option><option value="45/116/8310">45</option><option value="60/284/8311">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7895" name="mytvslots[]" value="7895/Zaa RadioFM/2020-04-20/21:30:00 - 22:30:00">
                                                                            <label class="" for="advert7895">Zaa RadioFM</label></td> <td>2020-04-20</td>
                                                                        <td>21:30:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7895" class="selectslots" name="selectslots[]" onchange="getRate(7895);"><option disabled="" selected="">--select slot length--</option><option value="30/66/7895">30</option><option value="45/150/7896">45</option><option value="60/473/7897">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8533" name="mytvslots[]" value="8533/Yes FM/2020-04-20/22:00:00 - 22:30:00">
                                                                            <label class="" for="advert8533">Yes FM</label></td> <td>2020-04-20</td>
                                                                        <td>22:00:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8533" class="selectslots" name="selectslots[]" onchange="getRate(8533);"><option disabled="" selected="">--select slot length--</option><option value="30/87/8533">30</option><option value="45/113/8534">45</option><option value="60/495/8535">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8476" name="mytvslots[]" value="8476/Spark FM/2020-04-20/22:30:00 - 23:00:00">
                                                                            <label class="" for="advert8476">Spark FM</label></td> <td>2020-04-20</td>
                                                                        <td>22:30:00 - 23:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8476" class="selectslots" name="selectslots[]" onchange="getRate(8476);"><option disabled="" selected="">--select slot length--</option><option value="30/63/7395">30</option><option value="45/319/7396">45</option><option value="60/908/7397">60</option><option value="30/57/8476">30</option><option value="45/143/8477">45</option><option value="60/368/8478">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-21"><div class="popWrap" id="21" onclick="openThis(21);"><span class="daydate">21</span><span title="Available Radio Slots" class="open-button radio">10</span></div><div id="ad21"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7497" name="mytvslots[]" value="7497/Happy FM/2020-04-21/00:00:00 - 01:00:00">
                                                                            <label class="" for="advert7497">Happy FM</label></td> <td>2020-04-21</td>
                                                                        <td>00:00:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7497" class="selectslots" name="selectslots[]" onchange="getRate(7497);"><option disabled="" selected="">--select slot length--</option><option value="45/107/7497">45</option><option value="60/371/7498">60</option><option value="30/48/8099">30</option><option value="45/164/8100">45</option><option value="60/288/8101">60</option><option value="30/62/8249">30</option><option value="45/103/8250">45</option><option value="60/479/8251">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">371</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8482" name="mytvslots[]" value="8482/Solar FM/2020-04-21/00:30:00 - 01:00:00">
                                                                            <label class="" for="advert8482">Solar FM</label></td> <td>2020-04-21</td>
                                                                        <td>00:30:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8482" class="selectslots" name="selectslots[]" onchange="getRate(8482);"><option disabled="" selected="">--select slot length--</option><option value="30/50/8482">30</option><option value="45/170/8483">45</option><option value="60/467/8484">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">170</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7706" name="mytvslots[]" value="7706/Boss FM/2020-04-21/03:00:00 - 03:30:00">
                                                                            <label class="" for="advert7706">Boss FM</label></td> <td>2020-04-21</td>
                                                                        <td>03:00:00 - 03:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7706" class="selectslots" name="selectslots[]" onchange="getRate(7706);"><option disabled="" selected="">--select slot length--</option><option value="30/47/7706">30</option><option value="45/122/7707">45</option><option value="60/373/7708">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8524" name="mytvslots[]" value="8524/Pink FM/2020-04-21/04:00:00 - 05:00:00">
                                                                            <label class="" for="advert8524">Pink FM</label></td> <td>2020-04-21</td>
                                                                        <td>04:00:00 - 05:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8524" class="selectslots" name="selectslots[]" onchange="getRate(8524);"><option disabled="" selected="">--select slot length--</option><option value="30/58/8524">30</option><option value="45/248/8525">45</option><option value="60/368/8526">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">248</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7829" name="mytvslots[]" value="7829/MelodyFM /2020-04-21/05:30:00 - 06:30:00">
                                                                            <label class="" for="advert7829">MelodyFM </label></td> <td>2020-04-21</td>
                                                                        <td>05:30:00 - 06:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7829" class="selectslots" name="selectslots[]" onchange="getRate(7829);"><option disabled="" selected="">--select slot length--</option><option value="30/48/7829">30</option><option value="45/110/7830">45</option><option value="60/304/7831">60</option><option value="30/51/8357">30</option><option value="45/105/8358">45</option><option value="60/251/8359">60</option><option value="30/46/8423">30</option><option value="45/246/8424">45</option><option value="60/423/8425">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8372" name="mytvslots[]" value="8372/Anapua FM/2020-04-21/09:30:00 - 10:30:00">
                                                                            <label class="" for="advert8372">Anapua FM</label></td> <td>2020-04-21</td>
                                                                        <td>09:30:00 - 10:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8372" class="selectslots" name="selectslots[]" onchange="getRate(8372);"><option disabled="" selected="">--select slot length--</option><option value="30/30/8372">30</option><option value="45/100/8373">45</option><option value="60/333/8374">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">100</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7820" name="mytvslots[]" value="7820/MelodyFM /2020-04-21/14:00:00 - 15:00:00">
                                                                            <label class="" for="advert7820">MelodyFM </label></td> <td>2020-04-21</td>
                                                                        <td>14:00:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7820" class="selectslots" name="selectslots[]" onchange="getRate(7820);"><option disabled="" selected="">--select slot length--</option><option value="30/81/7820">30</option><option value="45/123/7821">45</option><option value="60/305/7822">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8033" name="mytvslots[]" value="8033/Hope FM/2020-04-21/17:30:00 - 18:30:00">
                                                                            <label class="" for="advert8033">Hope FM</label></td> <td>2020-04-21</td>
                                                                        <td>17:30:00 - 18:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8033" class="selectslots" name="selectslots[]" onchange="getRate(8033);"><option disabled="" selected="">--select slot length--</option><option value="30/61/8033">30</option><option value="45/201/8034">45</option><option value="60/484/8035">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8363" name="mytvslots[]" value="8363/Anapua FM/2020-04-21/18:00:00 - 18:30:00">
                                                                            <label class="" for="advert8363">Anapua FM</label></td> <td>2020-04-21</td>
                                                                        <td>18:00:00 - 18:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8363" class="selectslots" name="selectslots[]" onchange="getRate(8363);"><option disabled="" selected="">--select slot length--</option><option value="30/68/8363">30</option><option value="45/209/8364">45</option><option value="60/484/8365">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7715" name="mytvslots[]" value="7715/Rok FM/2020-04-21/21:30:00 - 22:30:00">
                                                                            <label class="" for="advert7715">Rok FM</label></td> <td>2020-04-21</td>
                                                                        <td>21:30:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7715" class="selectslots" name="selectslots[]" onchange="getRate(7715);"><option disabled="" selected="">--select slot length--</option><option value="30/99/7715">30</option><option value="45/155/7716">45</option><option value="60/297/7717">60</option><option value="30/79/8563">30</option><option value="45/163/8564">45</option><option value="60/335/8565">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-22"><div class="popWrap" id="22" onclick="openThis(22);"><span class="daydate">22</span><span title="Available Radio Slots" class="open-button radio">12</span></div><div id="ad22"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8237" name="mytvslots[]" value="8237/Word FM/2020-04-22/00:30:00 - 01:30:00">
                                                                            <label class="" for="advert8237">Word FM</label></td> <td>2020-04-22</td>
                                                                        <td>00:30:00 - 01:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8237" class="selectslots" name="selectslots[]" onchange="getRate(8237);"><option disabled="" selected="">--select slot length--</option><option value="30/81/8237">30</option><option value="45/247/8238">45</option><option value="60/495/8239">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7625" name="mytvslots[]" value="7625/KessbenFM /2020-04-22/01:30:00 - 02:00:00">
                                                                            <label class="" for="advert7625">KessbenFM </label></td> <td>2020-04-22</td>
                                                                        <td>01:30:00 - 02:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7625" class="selectslots" name="selectslots[]" onchange="getRate(7625);"><option disabled="" selected="">--select slot length--</option><option value="30/36/7625">30</option><option value="45/113/7626">45</option><option value="60/376/7627">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7592" name="mytvslots[]" value="7592/LuvFM /2020-04-22/05:00:00 - 05:30:00">
                                                                            <label class="" for="advert7592">LuvFM </label></td> <td>2020-04-22</td>
                                                                        <td>05:00:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7592" class="selectslots" name="selectslots[]" onchange="getRate(7592);"><option disabled="" selected="">--select slot length--</option><option value="30/126/7368">30</option><option value="45/277/7369">45</option><option value="60/744/7370">60</option><option value="30/66/7592">30</option><option value="45/156/7593">45</option><option value="60/428/7594">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8228" name="mytvslots[]" value="8228/Word FM/2020-04-22/05:30:00 - 06:00:00">
                                                                            <label class="" for="advert8228">Word FM</label></td> <td>2020-04-22</td>
                                                                        <td>05:30:00 - 06:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8228" class="selectslots" name="selectslots[]" onchange="getRate(8228);"><option disabled="" selected="">--select slot length--</option><option value="30/67/8228">30</option><option value="45/120/8229">45</option><option value="60/365/8230">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8569" name="mytvslots[]" value="8569/Sompa FM/2020-04-22/09:30:00 - 10:30:00">
                                                                            <label class="" for="advert8569">Sompa FM</label></td> <td>2020-04-22</td>
                                                                        <td>09:30:00 - 10:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8569" class="selectslots" name="selectslots[]" onchange="getRate(8569);"><option disabled="" selected="">--select slot length--</option><option value="30/56/8569">30</option><option value="45/204/8570">45</option><option value="60/326/8571">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7751" name="mytvslots[]" value="7751/RadioSilverFM/2020-04-22/12:30:00 - 13:30:00">
                                                                            <label class="" for="advert7751">RadioSilverFM</label></td> <td>2020-04-22</td>
                                                                        <td>12:30:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7751" class="selectslots" name="selectslots[]" onchange="getRate(7751);"><option disabled="" selected="">--select slot length--</option><option value="30/96/7751">30</option><option value="45/221/7752">45</option><option value="60/259/7753">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8536" name="mytvslots[]" value="8536/Yes FM/2020-04-22/13:00:00 - 14:00:00">
                                                                            <label class="" for="advert8536">Yes FM</label></td> <td>2020-04-22</td>
                                                                        <td>13:00:00 - 14:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8536" class="selectslots" name="selectslots[]" onchange="getRate(8536);"><option disabled="" selected="">--select slot length--</option><option value="30/32/8536">30</option><option value="45/170/8537">45</option><option value="60/394/8538">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8084" name="mytvslots[]" value="8084/Jubilee Radio FM/2020-04-22/14:00:00 - 14:30:00">
                                                                            <label class="" for="advert8084">Jubilee Radio FM</label></td> <td>2020-04-22</td>
                                                                        <td>14:00:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8084" class="selectslots" name="selectslots[]" onchange="getRate(8084);"><option disabled="" selected="">--select slot length--</option><option value="30/46/8084">30</option><option value="45/247/8085">45</option><option value="60/322/8086">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7889" name="mytvslots[]" value="7889/Zaa RadioFM/2020-04-22/14:30:00 - 15:00:00">
                                                                            <label class="" for="advert7889">Zaa RadioFM</label></td> <td>2020-04-22</td>
                                                                        <td>14:30:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7889" class="selectslots" name="selectslots[]" onchange="getRate(7889);"><option disabled="" selected="">--select slot length--</option><option value="30/50/7889">30</option><option value="45/171/7890">45</option><option value="60/450/7891">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7913" name="mytvslots[]" value="7913/DiamondFM/2020-04-22/17:00:00 - 18:00:00">
                                                                            <label class="" for="advert7913">DiamondFM</label></td> <td>2020-04-22</td>
                                                                        <td>17:00:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7913" class="selectslots" name="selectslots[]" onchange="getRate(7913);"><option disabled="" selected="">--select slot length--</option><option value="30/71/7913">30</option><option value="45/222/7914">45</option><option value="60/402/7915">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7790" name="mytvslots[]" value="7790/SkyPowerFM /2020-04-22/21:00:00 - 22:00:00">
                                                                            <label class="" for="advert7790">SkyPowerFM </label></td> <td>2020-04-22</td>
                                                                        <td>21:00:00 - 22:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7790" class="selectslots" name="selectslots[]" onchange="getRate(7790);"><option disabled="" selected="">--select slot length--</option><option value="30/51/7790">30</option><option value="45/126/7791">45</option><option value="60/366/7792">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8426" name="mytvslots[]" value="8426/Star FM/2020-04-22/23:00:00 - 23:30:00">
                                                                            <label class="" for="advert8426">Star FM</label></td> <td>2020-04-22</td>
                                                                        <td>23:00:00 - 23:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8426" class="selectslots" name="selectslots[]" onchange="getRate(8426);"><option disabled="" selected="">--select slot length--</option><option value="30/43/8426">30</option><option value="45/102/8427">45</option><option value="60/275/8428">60</option><option value="30/84/8629">30</option><option value="45/141/8630">45</option><option value="60/397/8631">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-23"><div class="popWrap" id="23" onclick="openThis(23);"><span class="daydate">23</span><span title="Available Radio Slots" class="open-button radio">10</span></div><div id="ad23"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8183" name="mytvslots[]" value="8183/Radio Upper West/2020-04-23/00:30:00 - 01:00:00">
                                                                            <label class="" for="advert8183">Radio Upper West</label></td> <td>2020-04-23</td>
                                                                        <td>00:30:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8183" class="selectslots" name="selectslots[]" onchange="getRate(8183);"><option disabled="" selected="">--select slot length--</option><option value="30/41/8183">30</option><option value="45/241/8184">45</option><option value="60/345/8185">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8255" name="mytvslots[]" value="8255/A1 Radio FM/2020-04-23/01:30:00 - 02:30:00">
                                                                            <label class="" for="advert8255">A1 Radio FM</label></td> <td>2020-04-23</td>
                                                                        <td>01:30:00 - 02:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8255" class="selectslots" name="selectslots[]" onchange="getRate(8255);"><option disabled="" selected="">--select slot length--</option><option value="30/45/8255">30</option><option value="45/166/8256">45</option><option value="60/276/8257">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7835" name="mytvslots[]" value="7835/MelodyFM /2020-04-23/02:30:00 - 03:30:00">
                                                                            <label class="" for="advert7835">MelodyFM </label></td> <td>2020-04-23</td>
                                                                        <td>02:30:00 - 03:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7835" class="selectslots" name="selectslots[]" onchange="getRate(7835);"><option disabled="" selected="">--select slot length--</option><option value="30/89/7835">30</option><option value="45/235/7836">45</option><option value="60/338/7837">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8219" name="mytvslots[]" value="8219/Radio Progress FM/2020-04-23/06:00:00 - 07:00:00">
                                                                            <label class="" for="advert8219">Radio Progress FM</label></td> <td>2020-04-23</td>
                                                                        <td>06:00:00 - 07:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8219" class="selectslots" name="selectslots[]" onchange="getRate(8219);"><option disabled="" selected="">--select slot length--</option><option value="30/47/8219">30</option><option value="45/109/8220">45</option><option value="60/408/8221">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8691" name="mytvslots[]" value="8691/Goodlife FM/2020-04-23/07:00:00 - 08:00:00">
                                                                            <label class="" for="advert8691">Goodlife FM</label></td> <td>2020-04-23</td>
                                                                        <td>07:00:00 - 08:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8691" class="selectslots" name="selectslots[]" onchange="getRate(8691);"><option disabled="" selected="">--select slot length--</option><option value="30/80/8691">30</option><option value="45/228/8692">45</option><option value="60/467/8693">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8602" name="mytvslots[]" value="8602/Emak FM/2020-04-23/08:30:00 - 09:30:00">
                                                                            <label class="" for="advert8602">Emak FM</label></td> <td>2020-04-23</td>
                                                                        <td>08:30:00 - 09:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8602" class="selectslots" name="selectslots[]" onchange="getRate(8602);"><option disabled="" selected="">--select slot length--</option><option value="30/30/8602">30</option><option value="45/151/8603">45</option><option value="60/259/8604">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8225" name="mytvslots[]" value="8225/Word FM/2020-04-23/12:30:00 - 13:30:00">
                                                                            <label class="" for="advert8225">Word FM</label></td> <td>2020-04-23</td>
                                                                        <td>12:30:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8225" class="selectslots" name="selectslots[]" onchange="getRate(8225);"><option disabled="" selected="">--select slot length--</option><option value="30/52/8225">30</option><option value="45/141/8226">45</option><option value="60/334/8227">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8192" name="mytvslots[]" value="8192/Radio Upper West/2020-04-23/13:30:00 - 14:00:00">
                                                                            <label class="" for="advert8192">Radio Upper West</label></td> <td>2020-04-23</td>
                                                                        <td>13:30:00 - 14:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8192" class="selectslots" name="selectslots[]" onchange="getRate(8192);"><option disabled="" selected="">--select slot length--</option><option value="30/95/8192">30</option><option value="45/197/8193">45</option><option value="60/296/8194">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8279" name="mytvslots[]" value="8279/Rock FM/2020-04-23/14:30:00 - 15:00:00">
                                                                            <label class="" for="advert8279">Rock FM</label></td> <td>2020-04-23</td>
                                                                        <td>14:30:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8279" class="selectslots" name="selectslots[]" onchange="getRate(8279);"><option disabled="" selected="">--select slot length--</option><option value="30/76/8279">30</option><option value="45/105/8280">45</option><option value="60/481/8281">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7778" name="mytvslots[]" value="7778/SkyPowerFM /2020-04-23/18:00:00 - 19:00:00">
                                                                            <label class="" for="advert7778">SkyPowerFM </label></td> <td>2020-04-23</td>
                                                                        <td>18:00:00 - 19:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7778" class="selectslots" name="selectslots[]" onchange="getRate(7778);"><option disabled="" selected="">--select slot length--</option><option value="30/94/7778">30</option><option value="45/223/7779">45</option><option value="60/317/7780">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-24"><div class="popWrap" id="24" onclick="openThis(24);"><span class="daydate">24</span><span title="Available Radio Slots" class="open-button radio">15</span></div><div id="ad24"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7961" name="mytvslots[]" value="7961/Savannah RadioFM/2020-04-24/00:00:00 - 01:00:00">
                                                                            <label class="" for="advert7961">Savannah RadioFM</label></td> <td>2020-04-24</td>
                                                                        <td>00:00:00 - 01:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7961" class="selectslots" name="selectslots[]" onchange="getRate(7961);"><option disabled="" selected="">--select slot length--</option><option value="30/51/7961">30</option><option value="45/204/7962">45</option><option value="60/255/7963">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8578" name="mytvslots[]" value="8578/Sompa FM/2020-04-24/01:00:00 - 01:30:00">
                                                                            <label class="" for="advert8578">Sompa FM</label></td> <td>2020-04-24</td>
                                                                        <td>01:00:00 - 01:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8578" class="selectslots" name="selectslots[]" onchange="getRate(8578);"><option disabled="" selected="">--select slot length--</option><option value="30/138/7392">30</option><option value="45/347/7393">45</option><option value="60/976/7394">60</option><option value="30/80/8578">30</option><option value="45/250/8579">45</option><option value="60/429/8580">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8566" name="mytvslots[]" value="8566/Sompa FM/2020-04-24/01:30:00 - 02:00:00">
                                                                            <label class="" for="advert8566">Sompa FM</label></td> <td>2020-04-24</td>
                                                                        <td>01:30:00 - 02:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8566" class="selectslots" name="selectslots[]" onchange="getRate(8566);"><option disabled="" selected="">--select slot length--</option><option value="30/85/8566">30</option><option value="45/132/8567">45</option><option value="60/264/8568">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7634" name="mytvslots[]" value="7634/KessbenFM /2020-04-24/02:00:00 - 03:00:00">
                                                                            <label class="" for="advert7634">KessbenFM </label></td> <td>2020-04-24</td>
                                                                        <td>02:00:00 - 03:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7634" class="selectslots" name="selectslots[]" onchange="getRate(7634);"><option disabled="" selected="">--select slot length--</option><option value="30/30/7634">30</option><option value="45/195/7635">45</option><option value="60/386/7636">60</option><option value="30/50/8216">30</option><option value="45/227/8217">45</option><option value="60/391/8218">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7877" name="mytvslots[]" value="7877/Zaa RadioFM/2020-04-24/02:30:00 - 03:00:00">
                                                                            <label class="" for="advert7877">Zaa RadioFM</label></td> <td>2020-04-24</td>
                                                                        <td>02:30:00 - 03:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7877" class="selectslots" name="selectslots[]" onchange="getRate(7877);"><option disabled="" selected="">--select slot length--</option><option value="30/92/7877">30</option><option value="45/198/7878">45</option><option value="60/455/7879">60</option><option value="30/91/8620">30</option><option value="45/103/8621">45</option><option value="60/461/8622">60</option><option value="30/66/8709">30</option><option value="45/246/8710">45</option><option value="60/438/8711">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8551" name="mytvslots[]" value="8551/Yes FM/2020-04-24/03:00:00 - 04:00:00">
                                                                            <label class="" for="advert8551">Yes FM</label></td> <td>2020-04-24</td>
                                                                        <td>03:00:00 - 04:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8551" class="selectslots" name="selectslots[]" onchange="getRate(8551);"><option disabled="" selected="">--select slot length--</option><option value="30/31/8551">30</option><option value="45/148/8552">45</option><option value="60/426/8553">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8288" name="mytvslots[]" value="8288/Rock FM/2020-04-24/03:30:00 - 04:30:00">
                                                                            <label class="" for="advert8288">Rock FM</label></td> <td>2020-04-24</td>
                                                                        <td>03:30:00 - 04:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8288" class="selectslots" name="selectslots[]" onchange="getRate(8288);"><option disabled="" selected="">--select slot length--</option><option value="30/49/8288">30</option><option value="45/190/8289">45</option><option value="60/438/8290">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8027" name="mytvslots[]" value="8027/Holy FM/2020-04-24/12:00:00 - 13:00:00">
                                                                            <label class="" for="advert8027">Holy FM</label></td> <td>2020-04-24</td>
                                                                        <td>12:00:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8027" class="selectslots" name="selectslots[]" onchange="getRate(8027);"><option disabled="" selected="">--select slot length--</option><option value="30/76/8027">30</option><option value="45/166/8028">45</option><option value="60/486/8029">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8635" name="mytvslots[]" value="8635/Eastern FM/2020-04-24/12:30:00 - 13:30:00">
                                                                            <label class="" for="advert8635">Eastern FM</label></td> <td>2020-04-24</td>
                                                                        <td>12:30:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8635" class="selectslots" name="selectslots[]" onchange="getRate(8635);"><option disabled="" selected="">--select slot length--</option><option value="30/76/8635">30</option><option value="45/245/8636">45</option><option value="60/420/8637">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8300" name="mytvslots[]" value="8300/Rock FM/2020-04-24/13:30:00 - 14:30:00">
                                                                            <label class="" for="advert8300">Rock FM</label></td> <td>2020-04-24</td>
                                                                        <td>13:30:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8300" class="selectslots" name="selectslots[]" onchange="getRate(8300);"><option disabled="" selected="">--select slot length--</option><option value="30/34/8300">30</option><option value="45/171/8301">45</option><option value="60/356/8302">60</option><option value="30/44/8515">30</option><option value="45/112/8516">45</option><option value="60/460/8517">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7502" name="mytvslots[]" value="7502/Happy FM/2020-04-24/14:00:00 - 15:00:00">
                                                                            <label class="" for="advert7502">Happy FM</label></td> <td>2020-04-24</td>
                                                                        <td>14:00:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7502" class="selectslots" name="selectslots[]" onchange="getRate(7502);"><option disabled="" selected="">--select slot length--</option><option value="30/63/7502">30</option><option value="45/184/7503">45</option><option value="60/316/7504">60</option><option value="30/31/8659">30</option><option value="45/226/8660">45</option><option value="60/386/8661">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7670" name="mytvslots[]" value="7670/Fox FM/2020-04-24/15:30:00 - 16:30:00">
                                                                            <label class="" for="advert7670">Fox FM</label></td> <td>2020-04-24</td>
                                                                        <td>15:30:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7670" class="selectslots" name="selectslots[]" onchange="getRate(7670);"><option disabled="" selected="">--select slot length--</option><option value="30/39/7670">30</option><option value="45/193/7671">45</option><option value="60/464/7672">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7589" name="mytvslots[]" value="7589/LuvFM /2020-04-24/16:00:00 - 16:30:00">
                                                                            <label class="" for="advert7589">LuvFM </label></td> <td>2020-04-24</td>
                                                                        <td>16:00:00 - 16:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7589" class="selectslots" name="selectslots[]" onchange="getRate(7589);"><option disabled="" selected="">--select slot length--</option><option value="30/68/7589">30</option><option value="45/103/7590">45</option><option value="60/410/7591">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8120" name="mytvslots[]" value="8120/Radio Freed FM/2020-04-24/19:00:00 - 19:30:00">
                                                                            <label class="" for="advert8120">Radio Freed FM</label></td> <td>2020-04-24</td>
                                                                        <td>19:00:00 - 19:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8120" class="selectslots" name="selectslots[]" onchange="getRate(8120);"><option disabled="" selected="">--select slot length--</option><option value="30/55/8120">30</option><option value="45/123/8121">45</option><option value="60/316/8122">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8497" name="mytvslots[]" value="8497/Solar FM/2020-04-24/21:30:00 - 22:30:00">
                                                                            <label class="" for="advert8497">Solar FM</label></td> <td>2020-04-24</td>
                                                                        <td>21:30:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8497" class="selectslots" name="selectslots[]" onchange="getRate(8497);"><option disabled="" selected="">--select slot length--</option><option value="30/42/8497">30</option><option value="45/189/8498">45</option><option value="60/401/8499">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-25"><div class="popWrap" id="25" onclick="openThis(25);"><span class="daydate">25</span><span title="Available Radio Slots" class="open-button radio">7</span></div><div id="ad25"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8686" name="mytvslots[]" value="8686/Goodlife FM/2020-04-25/01:00:00 - 02:00:00">
                                                                            <label class="" for="advert8686">Goodlife FM</label></td> <td>2020-04-25</td>
                                                                        <td>01:00:00 - 02:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8686" class="selectslots" name="selectslots[]" onchange="getRate(8686);"><option disabled="" selected="">--select slot length--</option><option value="30/54/8686">30</option><option value="45/211/8687">45</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">211</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8468" name="mytvslots[]" value="8468/Spark FM/2020-04-25/06:30:00 - 07:30:00">
                                                                            <label class="" for="advert8468">Spark FM</label></td> <td>2020-04-25</td>
                                                                        <td>06:30:00 - 07:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8468" class="selectslots" name="selectslots[]" onchange="getRate(8468);"><option disabled="" selected="">--select slot length--</option><option value="30/75/8468">30</option><option value="45/199/8469">45</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7775" name="mytvslots[]" value="7775/SkyPowerFM /2020-04-25/08:30:00 - 09:30:00">
                                                                            <label class="" for="advert7775">SkyPowerFM </label></td> <td>2020-04-25</td>
                                                                        <td>08:30:00 - 09:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7775" class="selectslots" name="selectslots[]" onchange="getRate(7775);"><option disabled="" selected="">--select slot length--</option><option value="30/34/7775">30</option><option value="45/142/7776">45</option><option value="60/487/7777">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8611" name="mytvslots[]" value="8611/Emak FM/2020-04-25/15:30:00 - 16:00:00">
                                                                            <label class="" for="advert8611">Emak FM</label></td> <td>2020-04-25</td>
                                                                        <td>15:30:00 - 16:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8611" class="selectslots" name="selectslots[]" onchange="getRate(8611);"><option disabled="" selected="">--select slot length--</option><option value="30/45/8611">30</option><option value="45/127/8612">45</option><option value="60/273/8613">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7859" name="mytvslots[]" value="7859/RadioTamaleFM/2020-04-25/21:00:00 - 21:30:00">
                                                                            <label class="" for="advert7859">RadioTamaleFM</label></td> <td>2020-04-25</td>
                                                                        <td>21:00:00 - 21:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7859" class="selectslots" name="selectslots[]" onchange="getRate(7859);"><option disabled="" selected="">--select slot length--</option><option value="30/54/7859">30</option><option value="45/105/7860">45</option><option value="60/386/7861">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7443" name="mytvslots[]" value="7443/Prime FM/2020-04-25/22:00:00 - 22:30:00">
                                                                            <label class="" for="advert7443">Prime FM</label></td> <td>2020-04-25</td>
                                                                        <td>22:00:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7443" class="selectslots" name="selectslots[]" onchange="getRate(7443);"><option disabled="" selected="">--select slot length--</option><option value="30/68/7443">30</option><option value="45/182/7444">45</option><option value="60/276/7445">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8703" name="mytvslots[]" value="8703/Vision FM/2020-04-25/23:00:00 - 23:30:00">
                                                                            <label class="" for="advert8703">Vision FM</label></td> <td>2020-04-25</td>
                                                                        <td>23:00:00 - 23:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8703" class="selectslots" name="selectslots[]" onchange="getRate(8703);"><option disabled="" selected="">--select slot length--</option><option value="30/59/8703">30</option><option value="45/240/8704">45</option><option value="60/343/8705">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td></tr><tr><td border="1" class="day available" rel="2020-04-26"><div class="popWrap" id="26" onclick="openThis(26);"><span class="daydate">26</span><span title="Available Radio Slots" class="open-button radio">12</span></div><div id="ad26"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7580" name="mytvslots[]" value="7580/SweetMelodiesFM /2020-04-26/00:00:00 - 00:30:00">
                                                                            <label class="" for="advert7580">SweetMelodiesFM </label></td> <td>2020-04-26</td>
                                                                        <td>00:00:00 - 00:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7580" class="selectslots" name="selectslots[]" onchange="getRate(7580);"><option disabled="" selected="">--select slot length--</option><option value="30/49/7580">30</option><option value="45/121/7581">45</option><option value="60/287/7582">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8614" name="mytvslots[]" value="8614/Eastern FM/2020-04-26/01:00:00 - 02:00:00">
                                                                            <label class="" for="advert8614">Eastern FM</label></td> <td>2020-04-26</td>
                                                                        <td>01:00:00 - 02:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8614" class="selectslots" name="selectslots[]" onchange="getRate(8614);"><option disabled="" selected="">--select slot length--</option><option value="30/73/8614">30</option><option value="45/225/8615">45</option><option value="60/302/8616">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8318" name="mytvslots[]" value="8318/Radio Gurune FM/2020-04-26/04:00:00 - 05:00:00">
                                                                            <label class="" for="advert8318">Radio Gurune FM</label></td> <td>2020-04-26</td>
                                                                        <td>04:00:00 - 05:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8318" class="selectslots" name="selectslots[]" onchange="getRate(8318);"><option disabled="" selected="">--select slot length--</option><option value="30/98/8318">30</option><option value="45/116/8319">45</option><option value="60/289/8320">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7988" name="mytvslots[]" value="7988/Light FM/2020-04-26/05:30:00 - 06:30:00">
                                                                            <label class="" for="advert7988">Light FM</label></td> <td>2020-04-26</td>
                                                                        <td>05:30:00 - 06:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7988" class="selectslots" name="selectslots[]" onchange="getRate(7988);"><option disabled="" selected="">--select slot length--</option><option value="30/46/7988">30</option><option value="45/195/7989">45</option><option value="60/493/7990">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7844" name="mytvslots[]" value="7844/RadioTamaleFM/2020-04-26/08:30:00 - 09:00:00">
                                                                            <label class="" for="advert7844">RadioTamaleFM</label></td> <td>2020-04-26</td>
                                                                        <td>08:30:00 - 09:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7844" class="selectslots" name="selectslots[]" onchange="getRate(7844);"><option disabled="" selected="">--select slot length--</option><option value="30/86/7844">30</option><option value="45/122/7845">45</option><option value="60/360/7846">60</option><option value="30/65/8506">30</option><option value="45/140/8507">45</option><option value="60/376/8508">60</option><option value="30/84/8539">30</option><option value="45/194/8540">45</option><option value="60/500/8541">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8126" name="mytvslots[]" value="8126/Radio Freed FM/2020-04-26/10:30:00 - 11:30:00">
                                                                            <label class="" for="advert8126">Radio Freed FM</label></td> <td>2020-04-26</td>
                                                                        <td>10:30:00 - 11:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8126" class="selectslots" name="selectslots[]" onchange="getRate(8126);"><option disabled="" selected="">--select slot length--</option><option value="30/95/8126">30</option><option value="45/162/8127">45</option><option value="60/351/8128">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7610" name="mytvslots[]" value="7610/KessbenFM /2020-04-26/13:00:00 - 13:30:00">
                                                                            <label class="" for="advert7610">KessbenFM </label></td> <td>2020-04-26</td>
                                                                        <td>13:00:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7610" class="selectslots" name="selectslots[]" onchange="getRate(7610);"><option disabled="" selected="">--select slot length--</option><option value="30/60/7610">30</option><option value="45/174/7611">45</option><option value="60/466/7612">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7562" name="mytvslots[]" value="7562/SweetMelodiesFM /2020-04-26/14:30:00 - 15:00:00">
                                                                            <label class="" for="advert7562">SweetMelodiesFM </label></td> <td>2020-04-26</td>
                                                                        <td>14:30:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7562" class="selectslots" name="selectslots[]" onchange="getRate(7562);"><option disabled="" selected="">--select slot length--</option><option value="30/67/7562">30</option><option value="45/142/7563">45</option><option value="60/424/7564">60</option><option value="30/39/7595">30</option><option value="45/197/7596">45</option><option value="60/250/7597">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7520" name="mytvslots[]" value="7520/Happy FM/2020-04-26/16:30:00 - 17:00:00">
                                                                            <label class="" for="advert7520">Happy FM</label></td> <td>2020-04-26</td>
                                                                        <td>16:30:00 - 17:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7520" class="selectslots" name="selectslots[]" onchange="getRate(7520);"><option disabled="" selected="">--select slot length--</option><option value="30/62/7520">30</option><option value="45/109/7521">45</option><option value="60/488/7522">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7622" name="mytvslots[]" value="7622/KessbenFM /2020-04-26/21:00:00 - 22:00:00">
                                                                            <label class="" for="advert7622">KessbenFM </label></td> <td>2020-04-26</td>
                                                                        <td>21:00:00 - 22:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7622" class="selectslots" name="selectslots[]" onchange="getRate(7622);"><option disabled="" selected="">--select slot length--</option><option value="30/80/7622">30</option><option value="45/228/7623">45</option><option value="60/450/7624">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7550" name="mytvslots[]" value="7550/AsempaFM /2020-04-26/21:30:00 - 22:30:00">
                                                                            <label class="" for="advert7550">AsempaFM </label></td> <td>2020-04-26</td>
                                                                        <td>21:30:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7550" class="selectslots" name="selectslots[]" onchange="getRate(7550);"><option disabled="" selected="">--select slot length--</option><option value="30/96/7550">30</option><option value="45/178/7551">45</option><option value="60/276/7552">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7850" name="mytvslots[]" value="7850/RadioTamaleFM/2020-04-26/22:00:00 - 23:00:00">
                                                                            <label class="" for="advert7850">RadioTamaleFM</label></td> <td>2020-04-26</td>
                                                                        <td>22:00:00 - 23:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7850" class="selectslots" name="selectslots[]" onchange="getRate(7850);"><option disabled="" selected="">--select slot length--</option><option value="30/73/7850">30</option><option value="45/216/7851">45</option><option value="60/349/7852">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-27"><div class="popWrap" id="27" onclick="openThis(27);"><span class="daydate">27</span><span title="Available Radio Slots" class="open-button radio">10</span></div><div id="ad27"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7688" name="mytvslots[]" value="7688/Boss FM/2020-04-27/04:30:00 - 05:30:00">
                                                                            <label class="" for="advert7688">Boss FM</label></td> <td>2020-04-27</td>
                                                                        <td>04:30:00 - 05:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7688" class="selectslots" name="selectslots[]" onchange="getRate(7688);"><option disabled="" selected="">--select slot length--</option><option value="30/50/7688">30</option><option value="45/102/7689">45</option><option value="60/301/7690">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7754" name="mytvslots[]" value="7754/RadioSilverFM/2020-04-27/05:00:00 - 06:00:00">
                                                                            <label class="" for="advert7754">RadioSilverFM</label></td> <td>2020-04-27</td>
                                                                        <td>05:00:00 - 06:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7754" class="selectslots" name="selectslots[]" onchange="getRate(7754);"><option disabled="" selected="">--select slot length--</option><option value="30/35/7754">30</option><option value="45/204/7755">45</option><option value="60/406/7756">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7446" name="mytvslots[]" value="7446/Prime FM/2020-04-27/07:30:00 - 08:00:00">
                                                                            <label class="" for="advert7446">Prime FM</label></td> <td>2020-04-27</td>
                                                                        <td>07:30:00 - 08:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7446" class="selectslots" name="selectslots[]" onchange="getRate(7446);"><option disabled="" selected="">--select slot length--</option><option value="30/38/7446">30</option><option value="45/132/7447">45</option><option value="60/282/7448">60</option><option value="30/65/7547">30</option><option value="45/122/7548">45</option><option value="60/292/7549">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8075" name="mytvslots[]" value="8075/Jubilee Radio FM/2020-04-27/09:00:00 - 10:00:00">
                                                                            <label class="" for="advert8075">Jubilee Radio FM</label></td> <td>2020-04-27</td>
                                                                        <td>09:00:00 - 10:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8075" class="selectslots" name="selectslots[]" onchange="getRate(8075);"><option disabled="" selected="">--select slot length--</option><option value="30/40/8075">30</option><option value="45/246/8076">45</option><option value="60/329/8077">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7769" name="mytvslots[]" value="7769/SkyPowerFM /2020-04-27/12:00:00 - 13:00:00">
                                                                            <label class="" for="advert7769">SkyPowerFM </label></td> <td>2020-04-27</td>
                                                                        <td>12:00:00 - 13:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7769" class="selectslots" name="selectslots[]" onchange="getRate(7769);"><option disabled="" selected="">--select slot length--</option><option value="30/69/7769">30</option><option value="45/248/7770">45</option><option value="60/280/7771">60</option><option value="30/84/7901">30</option><option value="45/179/7902">45</option><option value="60/314/7903">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7709" name="mytvslots[]" value="7709/Boss FM/2020-04-27/12:30:00 - 13:30:00">
                                                                            <label class="" for="advert7709">Boss FM</label></td> <td>2020-04-27</td>
                                                                        <td>12:30:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7709" class="selectslots" name="selectslots[]" onchange="getRate(7709);"><option disabled="" selected="">--select slot length--</option><option value="30/86/7709">30</option><option value="45/114/7710">45</option><option value="60/347/7711">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8542" name="mytvslots[]" value="8542/Yes FM/2020-04-27/14:00:00 - 14:30:00">
                                                                            <label class="" for="advert8542">Yes FM</label></td> <td>2020-04-27</td>
                                                                        <td>14:00:00 - 14:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8542" class="selectslots" name="selectslots[]" onchange="getRate(8542);"><option disabled="" selected="">--select slot length--</option><option value="30/90/8542">30</option><option value="45/207/8543">45</option><option value="60/414/8544">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7514" name="mytvslots[]" value="7514/Happy FM/2020-04-27/14:30:00 - 15:00:00">
                                                                            <label class="" for="advert7514">Happy FM</label></td> <td>2020-04-27</td>
                                                                        <td>14:30:00 - 15:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7514" class="selectslots" name="selectslots[]" onchange="getRate(7514);"><option disabled="" selected="">--select slot length--</option><option value="30/91/7514">30</option><option value="45/111/7515">45</option><option value="60/320/7516">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8665" name="mytvslots[]" value="8665/Sunrise FM/2020-04-27/19:00:00 - 19:30:00">
                                                                            <label class="" for="advert8665">Sunrise FM</label></td> <td>2020-04-27</td>
                                                                        <td>19:00:00 - 19:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8665" class="selectslots" name="selectslots[]" onchange="getRate(8665);"><option disabled="" selected="">--select slot length--</option><option value="30/97/8665">30</option><option value="45/105/8666">45</option><option value="60/451/8667">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7673" name="mytvslots[]" value="7673/Fox FM/2020-04-27/21:30:00 - 22:00:00">
                                                                            <label class="" for="advert7673">Fox FM</label></td> <td>2020-04-27</td>
                                                                        <td>21:30:00 - 22:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7673" class="selectslots" name="selectslots[]" onchange="getRate(7673);"><option disabled="" selected="">--select slot length--</option><option value="30/73/7673">30</option><option value="45/167/7674">45</option><option value="60/370/7675">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-28"><div class="popWrap" id="28" onclick="openThis(28);"><span class="daydate">28</span><span title="Available Radio Slots" class="open-button radio">9</span></div><div id="ad28"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8234" name="mytvslots[]" value="8234/Word FM/2020-04-28/00:00:00 - 00:30:00">
                                                                            <label class="" for="advert8234">Word FM</label></td> <td>2020-04-28</td>
                                                                        <td>00:00:00 - 00:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8234" class="selectslots" name="selectslots[]" onchange="getRate(8234);"><option disabled="" selected="">--select slot length--</option><option value="30/77/8234">30</option><option value="45/203/8235">45</option><option value="60/495/8236">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8285" name="mytvslots[]" value="8285/Rock FM/2020-04-28/03:30:00 - 04:30:00">
                                                                            <label class="" for="advert8285">Rock FM</label></td> <td>2020-04-28</td>
                                                                        <td>03:30:00 - 04:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8285" class="selectslots" name="selectslots[]" onchange="getRate(8285);"><option disabled="" selected="">--select slot length--</option><option value="30/96/8285">30</option><option value="45/175/8286">45</option><option value="60/281/8287">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8715" name="mytvslots[]" value="8715/Vision FM/2020-04-28/07:00:00 - 08:00:00">
                                                                            <label class="" for="advert8715">Vision FM</label></td> <td>2020-04-28</td>
                                                                        <td>07:00:00 - 08:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8715" class="selectslots" name="selectslots[]" onchange="getRate(8715);"><option disabled="" selected="">--select slot length--</option><option value="30/81/8715">30</option><option value="45/147/8716">45</option><option value="60/279/8717">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8060" name="mytvslots[]" value="8060/Jubilee Radio FM/2020-04-28/08:30:00 - 09:00:00">
                                                                            <label class="" for="advert8060">Jubilee Radio FM</label></td> <td>2020-04-28</td>
                                                                        <td>08:30:00 - 09:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8060" class="selectslots" name="selectslots[]" onchange="getRate(8060);"><option disabled="" selected="">--select slot length--</option><option value="30/56/8060">30</option><option value="45/121/8061">45</option><option value="60/302/8062">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7967" name="mytvslots[]" value="7967/Savannah RadioFM/2020-04-28/10:00:00 - 10:30:00">
                                                                            <label class="" for="advert7967">Savannah RadioFM</label></td> <td>2020-04-28</td>
                                                                        <td>10:00:00 - 10:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7967" class="selectslots" name="selectslots[]" onchange="getRate(7967);"><option disabled="" selected="">--select slot length--</option><option value="30/34/7967">30</option><option value="45/148/7968">45</option><option value="60/498/7969">60</option><option value="30/75/8462">30</option><option value="45/177/8463">45</option><option value="60/277/8464">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8414" name="mytvslots[]" value="8414/Star FM/2020-04-28/12:30:00 - 13:30:00">
                                                                            <label class="" for="advert8414">Star FM</label></td> <td>2020-04-28</td>
                                                                        <td>12:30:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8414" class="selectslots" name="selectslots[]" onchange="getRate(8414);"><option disabled="" selected="">--select slot length--</option><option value="30/37/8414">30</option><option value="45/125/8415">45</option><option value="60/481/8416">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7544" name="mytvslots[]" value="7544/AsempaFM /2020-04-28/21:00:00 - 21:30:00">
                                                                            <label class="" for="advert7544">AsempaFM </label></td> <td>2020-04-28</td>
                                                                        <td>21:00:00 - 21:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7544" class="selectslots" name="selectslots[]" onchange="getRate(7544);"><option disabled="" selected="">--select slot length--</option><option value="30/37/7544">30</option><option value="45/130/7545">45</option><option value="60/484/7546">60</option><option value="30/34/8171">30</option><option value="45/101/8172">45</option><option value="60/295/8173">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8054" name="mytvslots[]" value="8054/Hope FM/2020-04-28/21:30:00 - 22:30:00">
                                                                            <label class="" for="advert8054">Hope FM</label></td> <td>2020-04-28</td>
                                                                        <td>21:30:00 - 22:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8054" class="selectslots" name="selectslots[]" onchange="getRate(8054);"><option disabled="" selected="">--select slot length--</option><option value="30/70/8054">30</option><option value="45/195/8055">45</option><option value="60/339/8056">60</option><option value="30/84/8402">30</option><option value="45/203/8403">45</option><option value="60/275/8404">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7508" name="mytvslots[]" value="7508/Happy FM/2020-04-28/22:00:00 - 23:00:00">
                                                                            <label class="" for="advert7508">Happy FM</label></td> <td>2020-04-28</td>
                                                                        <td>22:00:00 - 23:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7508" class="selectslots" name="selectslots[]" onchange="getRate(7508);"><option disabled="" selected="">--select slot length--</option><option value="30/42/7508">30</option><option value="45/135/7509">45</option><option value="60/293/7510">60</option><option value="30/32/8252">30</option><option value="45/169/8253">45</option><option value="60/398/8254">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day available" rel="2020-04-29"><div class="popWrap" id="29" onclick="openThis(29);"><span class="daydate">29</span><span title="Available Radio Slots" class="open-button radio">6</span></div><div id="ad29"><div class="ad-details"><span><h5>Select Your Timeslots</h5></span><i class="simple-icon-close close-button" title="Close View" onclick="closeThis();"></i> <div class="tbl-wrapper"><table id="bootstrap-data-table-export" class="table table-striped table-bordered"><thead class="small"><tr><th>Slot Selector</th><th>Date</th><th>Slot Time</th><th>Duration (Secs)</th><th>Slot Length (Secs)</th><th>Rate per slot(GHC)</th></tr></thead><tbody><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8057" name="mytvslots[]" value="8057/Jubilee Radio FM/2020-04-29/02:30:00 - 03:00:00">
                                                                            <label class="" for="advert8057">Jubilee Radio FM</label></td> <td>2020-04-29</td>
                                                                        <td>02:30:00 - 03:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8057" class="selectslots" name="selectslots[]" onchange="getRate(8057);"><option disabled="" selected="">--select slot length--</option><option value="30/37/8057">30</option><option value="45/214/8058">45</option><option value="60/418/8059">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8456" name="mytvslots[]" value="8456/Sky FM/2020-04-29/08:00:00 - 08:30:00">
                                                                            <label class="" for="advert8456">Sky FM</label></td> <td>2020-04-29</td>
                                                                        <td>08:00:00 - 08:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8456" class="selectslots" name="selectslots[]" onchange="getRate(8456);"><option disabled="" selected="">--select slot length--</option><option value="30/54/8456">30</option><option value="45/187/8457">45</option><option value="60/434/8458">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7652" name="mytvslots[]" value="7652/Ashh FM/2020-04-29/08:30:00 - 09:00:00">
                                                                            <label class="" for="advert7652">Ashh FM</label></td> <td>2020-04-29</td>
                                                                        <td>08:30:00 - 09:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7652" class="selectslots" name="selectslots[]" onchange="getRate(7652);"><option disabled="" selected="">--select slot length--</option><option value="30/68/7652">30</option><option value="45/248/7653">45</option><option value="60/495/7654">60</option><option value="30/88/8369">30</option><option value="45/104/8370">45</option><option value="60/442/8371">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7868" name="mytvslots[]" value="7868/Zaa RadioFM/2020-04-29/12:30:00 - 13:30:00">
                                                                            <label class="" for="advert7868">Zaa RadioFM</label></td> <td>2020-04-29</td>
                                                                        <td>12:30:00 - 13:30:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7868" class="selectslots" name="selectslots[]" onchange="getRate(7868);"><option disabled="" selected="">--select slot length--</option><option value="30/41/7326">30</option><option value="45/470/7327">45</option><option value="60/741/7328">60</option><option value="30/125/7413">30</option><option value="45/288/7414">45</option><option value="60/736/7415">60</option><option value="30/60/7868">30</option><option value="45/250/7869">45</option><option value="60/318/7870">60</option><option value="30/87/8315">30</option><option value="45/100/8316">45</option><option value="60/400/8317">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert7494" name="mytvslots[]" value="7494/Choice FM/2020-04-29/17:00:00 - 18:00:00">
                                                                            <label class="" for="advert7494">Choice FM</label></td> <td>2020-04-29</td>
                                                                        <td>17:00:00 - 18:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots7494" class="selectslots" name="selectslots[]" onchange="getRate(7494);"><option disabled="" selected="">--select slot length--</option><option value="30/33/7494">30</option><option value="45/210/7495">45</option><option value="60/382/7496">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr><tr><td class="pl-2">
                                                                            <input type="checkbox" class="advert-spaces" id="advert8144" name="mytvslots[]" value="8144/Radford FM/2020-04-29/21:30:00 - 22:00:00">
                                                                            <label class="" for="advert8144">Radford FM</label></td> <td>2020-04-29</td>
                                                                        <td>21:30:00 - 22:00:00</td>
                                                                        <td>120</td>
                                                                        <td><select id="selectslots8144" class="selectslots" name="selectslots[]" onchange="getRate(8144);"><option disabled="" selected="">--select slot length--</option><option value="30/80/8144">30</option><option value="45/125/8145">45</option><option value="60/338/8146">60</option></select></td>
                                                                        <td class="slot-rate" name="slotrate[]">0</td>
                                                                    </tr></tbody></table></div></div></div></td><td border="1" class="day" rel="2020-04-30"><span class="daydate">30</span><span class="info"></span> </td><td colspan="2">&nbsp;</td></tr></tbody></table></div><br><div class="custom-control custom-checkbox monitor">
                                        <input type="checkbox" class="custom-control-input slotcheck" id="monitor2" name="monitor2" value="1" checked="" required="">
                                        <label class="custom-control-label" for="monitor2">Monitor selected slots</label>
                                    </div><button type="button" name="submit" class="submit-slots btn btn-sm btn-danger" onclick="getPurchaseSummary();">Submit slots</button> <span style="color: #e00;">(Please select an advert space to submit)</span></form></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<footer class="page-footer" style="opacity: 1;">
    <div class="footer-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="mb-0 text-muted">ColoredStrategies 2019</p>
                </div>
                <div class="col-sm-6 d-none d-sm-block">
                    <ul class="breadcrumb pt-0 pr-0 float-right">
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Review</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Purchase</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Docs</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/vendor/jquery-3.3.1.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/Chart.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/chartjs-plugin-datalabels.js" style="opacity: 1;"></script>
<script src="js/vendor/moment.min.js" style="opacity: 1;"></script>
<script src="js/vendor/fullcalendar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/datatables.min.js" style="opacity: 1;"></script>
<script src="js/vendor/perfect-scrollbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/progressbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/jquery.barrating.min.js" style="opacity: 1;"></script>
<script src="js/vendor/select2.full.js" style="opacity: 1;"></script>
<script src="js/vendor/nouislider.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap-datepicker.js" style="opacity: 1;"></script>
<script src="js/vendor/Sortable.js" style="opacity: 1;"></script>
<script src="js/vendor/mousetrap.min.js" style="opacity: 1;"></script>
<script src="js/vendor/glide.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/dataTables.buttons.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.flash.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.html5.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.print.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/JSZip/jszip.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/pdfmake.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/vfs_fonts.js" style="opacity: 1;"></script>
<script src="js/dore.script.js" style="opacity: 1;"></script>
<script src="js/scripts.js" style="opacity: 1;"></script><div class="theme-colors default-transition" style="opacity: 1;"><div class="p-4"><p class="text-muted mb-2">Light Theme</p><div class="d-flex flex-row justify-content-between mb-4"><a href="#" data-theme="dore.light.blue.min.css" class="theme-color theme-color-blue active"></a><a href="#" data-theme="dore.light.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.light.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.light.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.light.red.min.css" class="theme-color theme-color-red"></a></div><p class="text-muted mb-2">Dark Theme</p><div class="d-flex flex-row justify-content-between"><a href="#" data-theme="dore.dark.blue.min.css" class="theme-color theme-color-blue"></a><a href="#" data-theme="dore.dark.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.dark.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.dark.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.dark.red.min.css" class="theme-color theme-color-red"></a></div></div><div class="p-4"><p class="text-muted mb-2">Border Radius</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="roundedRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="rounded"><label class="custom-control-label" for="roundedRadio">Rounded</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="flatRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="flat" checked="checked"><label class="custom-control-label" for="flatRadio">Flat</label></div></div><div class="p-4"><p class="text-muted mb-2">Direction</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="ltrRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="ltr" checked="checked"><label class="custom-control-label" for="ltrRadio">Ltr</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="rtlRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="rtl"><label class="custom-control-label" for="rtlRadio">Rtl</label></div></div><a href="#" class="theme-button"> <i class="simple-icon-magic-wand"></i> </a></div>
<script src="js/client.custom.js" style="opacity: 1;"></script>


</body></html>