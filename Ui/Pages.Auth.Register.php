<html dir="ltr" lang="en"><head>
    <meta charset="UTF-8">
    <title>AYA - Smart Media Buy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="font/iconsmind-s/css/iconsminds.css">
    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/vendor/bootstrap.rtl.only.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-float-label.min.css">
    <link rel="stylesheet" type="text/css" href="css/dore.light.blue.min.css"><link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body class="background no-footer ltr flat" cz-shortcut-listen="true">
<div class="fixed-background" style="opacity: 1;"></div>
<main style="opacity: 1;" class="default-transition">
    <div class="container">
        <div class="row h-100">
            <div class="col-12 col-md-10 mx-auto my-auto">
                <div class="card auth-card">
                    <div class="position-relative image-side ">
                        <p class=" text-white h2">MAGIC IS IN THE DETAILS</p>
                        <p class="white mb-0">
                            Please use this form to register.
                            <br>If you are a member, please<br>
                            <a href="Pages.Auth.Login.php" class="btn btn-info btn-lg btn-shadow">login</a>
                        </p>
                    </div>
                    <div class="form-side">
                        <a href="Dashboard.Default.html">
                            <span class="logo-single"></span>
                        </a>
                        <h6 class="mb-4">Register</h6>
                        <p style="font-size:small; text-align:right;">Fields with <span style="color:red;">*</span> are required.</p>
                        <div class="err-notice"></div>
                        <form action="processing/registration.php" method="POST">
                            <label class="form-group has-float-label mb-4">
                                <input class="form-control" name="fname" id="fname" required="" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                                <span>First Name <span style="color:red;">*</span></span>
                                <div class="fname-notice"></div>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <input class="form-control" name="lname" id="lname" required="">
                                <span>Last Name <span style="color:red;">*</span></span>
                                <div class="lname-notice"></div>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <input class="form-control" id="username" name="username" onchange="checkUsername();" required="">
                                <span>Username <span style="color:red;">*</span></span>
                                <div class="username-notice"></div>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <input class="form-control" name="email" id="email" onchange="validateEmail('email')" required="">
                                <span>E-mail <span style="color:red;">*</span></span>
                                <div class="email-notice"></div>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <input class="form-control" name="phone" id="phone">
                                <span>Phone</span>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <div class="company">
                                    <select name="company" id="company" class="form-control">

                                        <option value="" selected="" disabled="">Please select Company</option>
                                        <option value="999999">Not A Company</option>


                                        <option value="520"> BAT-BIO ENGINEERING SERVICES</option>


                                        <option value="491"> JUMIA</option>


                                        <option value="330">A &amp; G INVESTMENTS</option>


                                        <option value="340">ABLEKUMA PHARMA LTD</option>


                                        <option value="567">ABSA GROUP LTD</option>


                                        <option value="277">ACACIA HEALTH INSURANCE</option>


                                        <option value="495">ACADEMIC CITY COLLEGE</option>


                                        <option value="248">ACCESS BANK </option>


                                        <option value="97">ACCRA BREWERY LTD</option>


                                        <option value="650">ADANSI TRAVELS</option>


                                        <option value="118">ADB</option>


                                        <option value="477">ADB SCIENTIFIC PRODUCTS LTD</option>


                                        <option value="440">ADOM SAVINGS AND LOANS</option>


                                        <option value="425">ADU MOTORS COMPANY LTD</option>


                                        <option value="342">ADUTWUMWAA HERBAL INDUSTRIES</option>


                                        <option value="565">ADVANCE SAVINGS AND LOANS</option>


                                        <option value="364">AF ELECTRICAL LTD</option>


                                        <option value="622">AF ELECTRICAL LTD</option>


                                        <option value="225">AFB Bank </option>


                                        <option value="441">AFRICA WORLD AIRLINES</option>


                                        <option value="205">AFRICAN COLA COMPANY </option>


                                        <option value="69">AFRODAN GH LTD</option>


                                        <option value="328">AGBEVE HERBAL CENTER</option>


                                        <option value="132">AGORWU FURNITURE LTD</option>


                                        <option value="374">AGYA APPIAH BITTERS LTD</option>


                                        <option value="409">AIRFRANCE LTD</option>


                                        <option value="1">AIRTEL</option>


                                        <option value="442">AIRTELTIGO</option>


                                        <option value="216">ALLIANZ INSURANCE LTD </option>


                                        <option value="2">ALLIED</option>


                                        <option value="569">ALMANAC REAL ESTATE LTD</option>


                                        <option value="443">ALPHA COLLEGE OF TECHNOLOGY</option>


                                        <option value="570">ALPHA COLLEGE OF TECHNOLOGY</option>


                                        <option value="571">ALPHA HOMEOPATHIC CENTRE</option>


                                        <option value="519">ALPHA HOUSE LTD</option>


                                        <option value="444">ALPHA INVESTMENT AND PROPERTIES</option>


                                        <option value="606">ALYAH SATELLITE COMMUNICATIONS</option>


                                        <option value="70">AMEN SCIENTIFIC HERBAL</option>


                                        <option value="332">AMIR CHAND KUMAR EXPORT LTD</option>


                                        <option value="77">AMPONSAH EFAH PHARMACEUTICALS</option>


                                        <option value="300">ANGEL HERBAL </option>


                                        <option value="426">ANOWA MOTHERCARE LTD</option>


                                        <option value="341">ANUMWAA ENTERPRISE</option>


                                        <option value="475">APPOINTED TIME SCREEN PRINTING</option>


                                        <option value="572">APPOLONIA CITY LTD</option>


                                        <option value="389">APPOLONIA DEVELOPMENT COMPANY LTD</option>


                                        <option value="390">APPROACHERS GHANA LTD</option>


                                        <option value="28">AQUAFRESH LTD</option>


                                        <option value="573">ARIK AIRLINES</option>


                                        <option value="254">ARLA LTD</option>


                                        <option value="468">ARMANI CHEMIST LTD</option>


                                        <option value="445">ARNOLD RICE LTD</option>


                                        <option value="574">ASANGOD ROOFING</option>


                                        <option value="446">ASANTE-AKYEM RURAL BANK</option>


                                        <option value="575">ASATEK COMPANY LTD</option>


                                        <option value="423">ASEDA SALES LTD</option>


                                        <option value="663">ASHESI UNIVERSITY</option>


                                        <option value="157">ASHFOAM LTD</option>


                                        <option value="379">ASHFORD LABS </option>


                                        <option value="343">ASIA GOLDEN RICE CO. LTD</option>


                                        <option value="3">ASN FINANCE</option>


                                        <option value="63">ATONA FOODS</option>


                                        <option value="594">AUTO PLAZA</option>


                                        <option value="535">AUTO-FIX ENGINEERING </option>


                                        <option value="576">AUTOFIX ENGINEERING</option>


                                        <option value="101">AYTON DRUGS</option>


                                        <option value="113">AZAR COMPANY LTD</option>


                                        <option value="526">BADU-NKASAH BOOKS AND STATIONERY</option>


                                        <option value="361">BALONA FOODS</option>


                                        <option value="315">BAMIMO ENTERPRISE</option>


                                        <option value="71">BANK OF AFRICA</option>


                                        <option value="634">BANK OF GHANA</option>


                                        <option value="4">BARCLAYS</option>


                                        <option value="635">BARITAS</option>


                                        <option value="577">BARTMART CONSTRUCTIONS</option>


                                        <option value="376">BASELINE PHARMACEUTICALS</option>


                                        <option value="323">BAYPORT FINANCE</option>


                                        <option value="280">BEATEX ENTERPRISE LTD </option>


                                        <option value="72">BEEMA INSURANCE</option>


                                        <option value="201">BEIERSDORF</option>


                                        <option value="197">BEIGE CAPITAL</option>


                                        <option value="420">BENCHMARK GH LTD</option>


                                        <option value="579">BENJIGATE ESTATE AND INVESTMENT</option>


                                        <option value="421">BENNY LTD</option>


                                        <option value="256">BEST ASSURANCE</option>


                                        <option value="6">BEST POINT</option>


                                        <option value="509">BET YETU GH</option>


                                        <option value="198">BETWAY</option>


                                        <option value="580">BETYETU</option>


                                        <option value="167">BINATONE</option>


                                        <option value="344">BIOCARE LABS</option>


                                        <option value="427">BISA LTD</option>


                                        <option value="422">BLACK SECRET LTD</option>


                                        <option value="191">BLOW CHEM INDUSTRIES </option>


                                        <option value="108">BLOW PAK INDUSTRIES</option>


                                        <option value="275">BOAFO HERBAL</option>


                                        <option value="274">BODY FASHION LTD </option>


                                        <option value="273">BOND SAVINGS AND LOANS </option>


                                        <option value="73">BORACK GH LTD</option>


                                        <option value="646">BREDENOORD GENERATORS</option>


                                        <option value="190">BRITANICA INDUSTRIES LTD</option>


                                        <option value="521">BRONZETONE</option>


                                        <option value="508">BRUHM LTD</option>


                                        <option value="664">BUDGET CASH AND CARRY</option>


                                        <option value="322">BUFFALO LTD</option>


                                        <option value="60">Busy4G</option>


                                        <option value="637">CAD FAM</option>


                                        <option value="235">CADBURY GH LTD </option>


                                        <option value="8">CALBANK</option>


                                        <option value="163">CAPITAL BANK </option>


                                        <option value="512">CAPITAL O2 LTD</option>


                                        <option value="334">CARABAO GHANA LTD</option>


                                        <option value="272">CASA PAINT LTD </option>


                                        <option value="527">CASTROL</option>


                                        <option value="482">CATHOLIC UNIVERSTIY COLLEGE</option>


                                        <option value="74">CCML BANK</option>


                                        <option value="490">CEE ROYAL CANOPIES LTD</option>


                                        <option value="154">CEMIX LTD</option>


                                        <option value="615">CENTER FOR AWARENESS</option>


                                        <option value="188">CERES </option>


                                        <option value="153">CETMAT COMPUTERS</option>


                                        <option value="218">CEYLON BISCUIT LTD</option>


                                        <option value="415">CFAO LTD</option>


                                        <option value="131">CFAO MOTORS </option>


                                        <option value="267">CHEERS </option>


                                        <option value="137">CHIEF TEST PREP</option>


                                        <option value="175">CHIGO GH LTD</option>


                                        <option value="614">CHOCHO INDUSTRIES LTD</option>


                                        <option value="628">CIMAF GHANA LTD</option>


                                        <option value="541">CITY TV</option>


                                        <option value="507">CITYCALLS SMARTPHONES LTD</option>


                                        <option value="629">CITYDIA</option>


                                        <option value="514">CO-RO COMPANY LTD</option>


                                        <option value="9">COCA COLA</option>


                                        <option value="353">COCOA PROCESSING COMPANY</option>


                                        <option value="289">COLGATE HERBAL</option>


                                        <option value="481">COLGATE PALMOLIVE LTD</option>


                                        <option value="183">COLOURMAX</option>


                                        <option value="528">COMMERCE ENERGY</option>


                                        <option value="392">COMSYS TELESOL GHANA LTD</option>


                                        <option value="560">Confessions </option>


                                        <option value="237">CONSERVERIA AFRICANA GHANA LTD</option>


                                        <option value="398">COPACI LTD</option>


                                        <option value="362">CORNELLY GHANA</option>


                                        <option value="581">CRYSTAL HIGH SCHOOL</option>


                                        <option value="166">CST LTD.</option>


                                        <option value="336">DAHIADA HERBAL CENTER</option>


                                        <option value="483">DAIKIN SOLUTIONS</option>


                                        <option value="288">DALIAH BEAUTY AND PHARMA LTD </option>


                                        <option value="350">DANK HEALTH CARE</option>


                                        <option value="616">DANNIE HERBAL &amp; TRADING ENTERPRISE</option>


                                        <option value="670">DATABANK</option>


                                        <option value="613">DAVERO ELECTRICAL ENGINEERING LTD</option>


                                        <option value="215">DAVITA TRADING LTD </option>


                                        <option value="320">DBS ROOFING SYSTEMS GH LTD</option>


                                        <option value="130">DE-LUXY PAINT </option>


                                        <option value="351">DE-NA PLUS LTD</option>


                                        <option value="543">DE-TIME LTD</option>


                                        <option value="382">DEBBIES LTD</option>


                                        <option value="671">DEL INTERNATIONAL HOSPITAL</option>


                                        <option value="10">DELAY</option>


                                        <option value="120">Delta Equipment Ltd</option>


                                        <option value="308">DENSU INDUSTRIES</option>


                                        <option value="231">DEPENDABLE PHARMACY</option>


                                        <option value="164">DESJOYAUX</option>


                                        <option value="539">DEVTRACO ESTATE LTD</option>


                                        <option value="210">DH INDUSTRIES LTD</option>


                                        <option value="13">DIAGEO</option>


                                        <option value="75">DIAMOND CEMENT CO LTD</option>


                                        <option value="396">DIGIT AFRICA LTD</option>


                                        <option value="313">DIRECT SAVINGS AND LOANS</option>


                                        <option value="400">DIVINE LTD</option>


                                        <option value="94">DK ALUMINIUM PLUS</option>


                                        <option value="447">DKT INTERNATIONAL</option>


                                        <option value="335">DODO COSMETICS</option>


                                        <option value="366">DOOA LTD</option>


                                        <option value="582">DOWNTOWN MIRACLE BOUTIQUE</option>


                                        <option value="146">DREAM COSMETICS </option>


                                        <option value="600">DUFIL PRIMA FOODS PLC</option>


                                        <option value="583">DURAPLAST GHANA LTD</option>


                                        <option value="363">DUSK CAPITAL</option>


                                        <option value="302">DVLA </option>


                                        <option value="410">E-DIGIT INSTITUTE</option>


                                        <option value="11">ECOBANK</option>


                                        <option value="264">ECONET</option>


                                        <option value="181">EDDYS PIZZA</option>


                                        <option value="484">EDDYS PIZZA</option>


                                        <option value="180">EDMAXX ROOFING </option>


                                        <option value="510">ELDER PHARMACEUTICALS LTD</option>


                                        <option value="129">ELECTROLAND GHANA LTD </option>


                                        <option value="651">ELECTROMART ELECTRONICS</option>


                                        <option value="462">ELLEEN MOTHERCARE</option>


                                        <option value="584">EMERALD PROPERTIES</option>


                                        <option value="585">EMIRATE</option>


                                        <option value="310">ENA PA</option>


                                        <option value="500">ENDPOINT HOMEOPATHIC CLINIC</option>


                                        <option value="522">ENESCO GH LTD</option>


                                        <option value="586">ENTERPRISE GROUP</option>


                                        <option value="12">ENTERPRISE INSURANCE</option>


                                        <option value="587">ENVIA PROPERTIES</option>


                                        <option value="194">EQUATOR FOODS GHANA</option>


                                        <option value="306">EQUITY ASSURANCE</option>


                                        <option value="301">ERATA MOTORS </option>


                                        <option value="356">ERNEST ACHEAMPONG ENTERPRISE LTD</option>


                                        <option value="78">ERNEST CHEMIST</option>


                                        <option value="177">EUPHORIA LTD</option>


                                        <option value="588">EVEREST LUBRICANTS PVT LTD</option>


                                        <option value="311">EVERPURE KUMASI LTD</option>


                                        <option value="292">EXCELLENCE BOUTIQUE</option>


                                        <option value="352">FABRIMETAL</option>


                                        <option value="286">FAIN GH LTD </option>


                                        <option value="411">FAITHWORKS SCHOOL LTD</option>


                                        <option value="179">FAN MILK LTD </option>


                                        <option value="161">FASHION CITY FURNITURE </option>


                                        <option value="79">FBN BANK GHANA</option>


                                        <option value="544">FC BAKERY LTD</option>


                                        <option value="470">FEDEK VENTURES </option>


                                        <option value="485">FEKKO GH LTD</option>


                                        <option value="14">FIDELITY BANK</option>


                                        <option value="589">FINE FINE HERBAL CENTRE</option>


                                        <option value="81">FIRST ATLANTIC BANK</option>


                                        <option value="619">FIRST BANC FINANCIAL SERVICES</option>


                                        <option value="638">FIRST BANK</option>


                                        <option value="505">FIRST NATIONAL BANK</option>


                                        <option value="437">FIRST TIN ENTERPRISE</option>


                                        <option value="536">FLEXI GHANA</option>


                                        <option value="295">FOODTECH LTD</option>


                                        <option value="270">FOREVER CLAIR PRODUCTS </option>


                                        <option value="652">FOREVER CONSTRUCTIONS CO. LTD</option>


                                        <option value="412">FOREWIN GHANA LTD</option>


                                        <option value="401">FORKUO HERBAL CENTER</option>


                                        <option value="82">FORTUNE PRODUCTS</option>


                                        <option value="95">FRANKO TRADING</option>


                                        <option value="187">FREDDIES CORNER </option>


                                        <option value="360">FREDIMENS HERBAL CENTER</option>


                                        <option value="123">FREEDOM BRAIDS COLLECTION </option>


                                        <option value="653">FREIGHT CONSULT</option>


                                        <option value="38">FREISLAND</option>


                                        <option value="383">FRESHLADY LTD</option>


                                        <option value="211">FROSTYBITE GHANA </option>


                                        <option value="381">FRUITEE LTD</option>


                                        <option value="241">FRUTELLI GHANA</option>


                                        <option value="139">FURNITURE CITY </option>


                                        <option value="504">FUTURE LIFE LTD</option>


                                        <option value="417">G.B PHARMA LTD</option>


                                        <option value="15">GCB</option>


                                        <option value="428">GENESIS HEALTH CENTER</option>


                                        <option value="590">GENESIS HEALTH CENTER</option>


                                        <option value="104">GENTLE GIANTS LTD</option>


                                        <option value="290">GEOWAG LTD</option>


                                        <option value="126">GHACEM LTD</option>


                                        <option value="665">GHANA FREEZONE AUTHORITY</option>


                                        <option value="654">GHANA HEALTH SERVICE</option>


                                        <option value="384">GHANA REVENUE AUTHORITY</option>


                                        <option value="189">GHANDOUR COSMETICS </option>


                                        <option value="568">GHIPSS</option>


                                        <option value="209">GIFAX HERBAL CENTRE </option>


                                        <option value="17">GIVERS</option>


                                        <option value="144">GLAMS MAKE-UP</option>


                                        <option value="255">GLAXO SMITH KLINE LTD</option>


                                        <option value="83">GLICO INSURANCE</option>


                                        <option value="18">GLO</option>


                                        <option value="623">GLORIAFRICA LTD</option>


                                        <option value="283">GMR INDUSTRIES</option>


                                        <option value="391">GN LIFE ASSURANCE</option>


                                        <option value="19">GOIL</option>


                                        <option value="282">GOKALS LTD </option>


                                        <option value="385">GOVERNMENT OF GHANA</option>


                                        <option value="503">GOWUF FURNITURE WORKS</option>


                                        <option value="639">GRACE HOMEOPATHIC CLINIC</option>


                                        <option value="561">GREAT GRACE SCHOOL</option>


                                        <option value="448">GREENFIELD SCEINTIFIC HERBAL CLINIC</option>


                                        <option value="20">GT BANK</option>


                                        <option value="21">GTP</option>


                                        <option value="329">GUANGZHOU BAOFENG COSMETICS GROUP</option>


                                        <option value="171">H&amp;H Gh Ltd</option>


                                        <option value="357">HAANO COMPANY LTD</option>


                                        <option value="112">HAPPY DELIGHT</option>


                                        <option value="317">HEALTHILIFE LTD</option>


                                        <option value="592">HEALTHILINK LTD</option>


                                        <option value="403">HEEL INTERNATIONAL HOMEOPATHIC CLINIC</option>


                                        <option value="647">HERBAL INTERVENTION CO. LTD</option>


                                        <option value="449">HERBAL TECHNOLOGY LTD</option>


                                        <option value="199">HERITAGE BANK </option>


                                        <option value="247">HFC BANK</option>


                                        <option value="418">HI-FLOW LTD </option>


                                        <option value="195">HISENSE </option>


                                        <option value="84">HOLLARD INSURANCE</option>


                                        <option value="645">HONDA</option>


                                        <option value="316">HOOCH FOODS</option>


                                        <option value="450">HUBTEL LTD</option>


                                        <option value="430">HUMBLEMAN ENTERPRISE</option>


                                        <option value="593">HUMBLEMAN ENTERPRISE</option>


                                        <option value="24">IDEAL FINANCE</option>


                                        <option value="222">IFS FINANCIAL SERVICES </option>


                                        <option value="595">IMPERIAL ASSURANCE</option>


                                        <option value="25">INESFLY AFRICA </option>


                                        <option value="627">INSAANIYYA HERBAL DRUG CENTER</option>


                                        <option value="523">INSTITUTE OF CHILDHOOD EDUCATION</option>


                                        <option value="26">INTERPLAST</option>


                                        <option value="375">ITAL PRESTRESS &amp; CONSTRUCTION </option>


                                        <option value="262">J M ADDO And SONS</option>


                                        <option value="476">JACCD PROFESSIONAL DEVELOPMENT CENTER</option>


                                        <option value="530">JAGGREYS NATURAL HEALTH AND FERTILITY CLINIC</option>


                                        <option value="151">JAMAL HASSAN COMPANY </option>


                                        <option value="111">JAMESON WHISKY </option>


                                        <option value="472">JANSSEN PHARMACEUTICAL LTD</option>


                                        <option value="86">JCB POWER GENERATORS</option>


                                        <option value="27">JOY INDUSTRIES LTD </option>


                                        <option value="419">JOY-WIND COMPANY LTD</option>


                                        <option value="135">JUBAILI BROS. LTD</option>


                                        <option value="511">JUMIA GH LTD</option>


                                        <option value="260">K&amp;G HERBAL CENTRE</option>


                                        <option value="453">K-K PREPRAH COMPANY LTD</option>


                                        <option value="538">K.K PREPRAH COMPANY LTD</option>


                                        <option value="655">KABFAM</option>


                                        <option value="596">KAPA OIL REFINERIES LTD</option>


                                        <option value="29">KASAPREKO</option>


                                        <option value="291">KASEMPA ENTERPRISE </option>


                                        <option value="228">KENOGA COMPANY LTD</option>


                                        <option value="451">KESSBEN DRIVING SCHOOL</option>


                                        <option value="239">KFC</option>


                                        <option value="158">KIMO HOME</option>


                                        <option value="196">KINA GROUP LTD </option>


                                        <option value="107">KINAPHARMA</option>


                                        <option value="30">KINGDOM </option>


                                        <option value="501">KINGDOM BEVERAGES</option>


                                        <option value="452">KINGDOM BOOKS AND STATIONERY</option>


                                        <option value="185">KINGDOM FURNITURE WORKS </option>


                                        <option value="102">KNG HERBAL</option>


                                        <option value="454">KNUST</option>


                                        <option value="531">KOFIKROM PHAMACY LTD</option>


                                        <option value="339">KOJACH PAHARMA</option>


                                        <option value="524">KONO SWA LTD </option>


                                        <option value="386">KROWAA HERBAL CENTRE</option>


                                        <option value="474">KWANGDONG PHARMCEUTICALS COMPANY LTD </option>


                                        <option value="578">KWIK PHARMA LTD</option>


                                        <option value="630">LABORATOIRES CHEMINEAU</option>


                                        <option value="597">LANSDOWN RESORT</option>


                                        <option value="31">LATEX FOAM</option>


                                        <option value="32">LAV</option>


                                        <option value="284">LAWSON HERBAL CENTRE </option>


                                        <option value="257">LEAP</option>


                                        <option value="57">LEXTA GHANA LTD</option>


                                        <option value="184">LG</option>


                                        <option value="168">LION ALUMINIUM LTD</option>


                                        <option value="220">LIQUID TELECOM GROUP </option>


                                        <option value="149">LOGISTICS MOVERS GHANA </option>


                                        <option value="243">LUCKY HERBAL CLINIC</option>


                                        <option value="455">LYDIAS FASHION LTD</option>


                                        <option value="261">M Y CAESAR CO. LTD  </option>


                                        <option value="249">M&amp;K GHANA LIMITED </option>


                                        <option value="515">MAC GHANA LTD</option>


                                        <option value="478">MAJIFRESH LTD</option>


                                        <option value="405">MAL-BERNICE ESTATE LTD</option>


                                        <option value="598">MARTIN COURIER EXPRESS</option>


                                        <option value="34">MASTERCARD</option>


                                        <option value="142">MAXIM COSMETICS </option>


                                        <option value="89">MCDAN SHIPPING</option>


                                        <option value="675">MEDEA PHARMACY</option>


                                        <option value="93">MEDI MOSES</option>


                                        <option value="404">MEDISOFT </option>


                                        <option value="62">Melcom</option>


                                        <option value="23">MENKISH IMPEX LTD</option>


                                        <option value="152">METALEX GROUP </option>


                                        <option value="640">METHODIST UNIVERSITY COLLEGE</option>


                                        <option value="465">MICARDO PHAMACY</option>


                                        <option value="182">MIDLAND SAVINGS AND LOANS </option>


                                        <option value="656">MIGHTY POWER GROUP OF CO.</option>


                                        <option value="435">MIKADO PHARMACY LTD</option>


                                        <option value="599">MILIFE INSURANCE</option>


                                        <option value="367">MINAZEN</option>


                                        <option value="562">MISTY HERBAL LTD</option>


                                        <option value="641">MOBILE WATER GHANA</option>


                                        <option value="200">MONEYGRAM</option>


                                        <option value="559">MONEYGRAM NIGERIA  </option>


                                        <option value="658">MOTSON MARKETING ENTERPRISE</option>


                                        <option value="258">MR CHEF </option>


                                        <option value="456">MR PRICE</option>


                                        <option value="35">MTN</option>


                                        <option value="457">MULTI INSURANCE LTD</option>


                                        <option value="76">MULTICHOICE GH LTD</option>


                                        <option value="479">MULTIMEDIA GROUP LTD</option>


                                        <option value="268">MULTIPAC LTD </option>


                                        <option value="202">Multipro Private Ltd </option>


                                        <option value="338">MVP</option>


                                        <option value="499">MY BET GH</option>


                                        <option value="537">MY SMART COKE.COM</option>


                                        <option value="226">NAMIO GH LTD</option>


                                        <option value="601">NANA ADDO SCIENTIFIC HERBAL CLINIC</option>


                                        <option value="368">NANOFIXIT COMPANY LTD</option>


                                        <option value="458">NASAN REAL ESTATE LTD</option>


                                        <option value="602">NATARAJ GHANA</option>


                                        <option value="115">NATIONAL DEMOCRATIC CONGRESS</option>


                                        <option value="591">NATIONAL IDENTIFICATION AUTHORITY</option>


                                        <option value="319">NATIONAL LOTTERY AUTHORITY</option>


                                        <option value="563">NATIONAL PETROLEUM AUTHORITY</option>


                                        <option value="434">NATIONWIDE MEDICAL INSURANCE</option>


                                        <option value="498">NATURAL WAY HERBAL CLINIC</option>


                                        <option value="299">NEAT FOOD COMPANY LTD.</option>


                                        <option value="36">NESTLE GH LTD</option>


                                        <option value="657">NETLIFE AFRICA</option>


                                        <option value="348">NEW GENERATION HERBAL HOME</option>


                                        <option value="666">NEW LIFE BODY CARE LTD</option>


                                        <option value="193">NEW PATRIOTIC PARTY </option>


                                        <option value="90">NEXANS CABLE</option>


                                        <option value="232">NIB</option>


                                        <option value="337">NICHE GHANA</option>


                                        <option value="125">NIIT</option>


                                        <option value="393">NISSI PHARMACY LTD</option>


                                        <option value="394">NKULENU INDUSTRIES LTD</option>


                                        <option value="304">NLA</option>


                                        <option value="265">NOBLE HAIR SYNTHETICS LTD</option>


                                        <option value="324">NOKIA </option>


                                        <option value="387">NORTH HILLS INTERNATIONAL SCHOOL</option>


                                        <option value="371">NOURISHER PROCESSING LTD</option>


                                        <option value="92">NP GHANDOUR</option>


                                        <option value="631">NSOROMA GPS LTD</option>


                                        <option value="165">NUECE LTD</option>


                                        <option value="42">NUTRIFOODS</option>


                                        <option value="424">OAK PLAZA HOTEL LTD</option>


                                        <option value="331">OBA MAKARNA LTD</option>


                                        <option value="110">OBIBINI BLACKMAN</option>


                                        <option value="413">OCEANIC HOTEL LTD</option>


                                        <option value="40">OLAM</option>


                                        <option value="229">OLD MUTUAL LIFE ASSURANCE </option>


                                        <option value="667">OLIVE PAINT LTD</option>


                                        <option value="546">OMAMA BAKERY LTD</option>


                                        <option value="492">OPERA</option>


                                        <option value="134">OPPORTUNITY INTERNATIONAL SAVINGS AND LOANS</option>


                                        <option value="91">ORCA DECO</option>


                                        <option value="269">ORGANIC HILLS CLINIC </option>


                                        <option value="355">ORKMAN GH LTD</option>


                                        <option value="611">ORKMAN GHANA LTD</option>


                                        <option value="224">OSONS CHEMIST LTD</option>


                                        <option value="143">PACKAGING MATRIXX </option>


                                        <option value="156">PACKPLUS INTERNATIONAL </option>


                                        <option value="128">PALACE HOME DECOR </option>


                                        <option value="460">PAPAS PIZZA LTD</option>


                                        <option value="162">PAPAYE </option>


                                        <option value="431">PCN ICT TRAINING CENTER</option>


                                        <option value="145">PEDUASE VALLEY RESORT </option>


                                        <option value="39">PEPSI</option>


                                        <option value="346">PERFECT SKIN AND BODY THERAPY LTD</option>


                                        <option value="204">PERFETTI VAN MELLE</option>


                                        <option value="659">PETRA TRUST CO. LTD</option>


                                        <option value="88">PHARMATON</option>


                                        <option value="545">PHARMATRUST LTD</option>


                                        <option value="395">PHARMATRUST PROFESSIONAL COLLEGE</option>


                                        <option value="547">PHERSONS HEALTH COLLEGE</option>


                                        <option value="378">PHYTO RIKER GIHOC PHARMACEUTICALS</option>


                                        <option value="564">PIAB AUTOPARTS AND ACCESSORIES LTD</option>


                                        <option value="116">PIZZA HUT </option>


                                        <option value="373">POKUPHARMA LTD</option>


                                        <option value="603">POLYTANK GHANA</option>


                                        <option value="429">POWER CLINIC LTD</option>


                                        <option value="548">POWER SPECIALIST HERBAL CLINIC</option>


                                        <option value="549">PPL LTD</option>


                                        <option value="513">PREMIER HEALTH INSURANCE</option>


                                        <option value="533">PREMIUM HEALTH INSURANCE</option>


                                        <option value="487">PREYMAN CLOTHING LTD</option>


                                        <option value="325">PRIME INSURANCE</option>


                                        <option value="170">PRINTEX</option>


                                        <option value="309">PRINTWELL COMAPANY LTD</option>


                                        <option value="203">PROCTER AND GAMBLE LTD </option>


                                        <option value="461">PROGRESS SAVINGS AND LOANS</option>


                                        <option value="37">PROMASIDOR GH LTD</option>


                                        <option value="318">PROVIDENCE INSURANCE</option>


                                        <option value="660">PROVIDENT INSURANCE</option>


                                        <option value="369">PRUDENTIAL BANK </option>


                                        <option value="607">PUMA ENERGY</option>


                                        <option value="281">PZ CUSSONS</option>


                                        <option value="486">QUAINOO LUCKY STORE</option>


                                        <option value="464">QUANTUM NATURAL HERBAL CENTER</option>


                                        <option value="207">QUANZHOU CHUANGDA INDUSTRIAL LTD</option>


                                        <option value="636">QUICK PHARMA LTD</option>


                                        <option value="669">QUICK PHARMA LTD</option>


                                        <option value="246">QUIMICAS ORO LTD </option>


                                        <option value="287">QUINOO LUCKY STORE </option>


                                        <option value="245">QUOLOON GHANA LTD </option>


                                        <option value="532">QUOLOON GHANA LTD</option>


                                        <option value="377">RADICO LTD </option>


                                        <option value="33">RAMANI DISTRIBUTION</option>


                                        <option value="459">RAMIA HOUSE LTD</option>


                                        <option value="610">RANA MOTORS</option>


                                        <option value="621">RAWUS PAHARMACEUTICALS</option>


                                        <option value="259">REAL GROUP LTD</option>


                                        <option value="642">REAL PLAN ESTATE</option>


                                        <option value="208">RECKITT BENKISER</option>


                                        <option value="354">REHOBOTH EJ LTD </option>


                                        <option value="321">REMIE FOODS COMPANY LTD</option>


                                        <option value="643">RENAULT</option>


                                        <option value="380">REPASH VENTURES</option>


                                        <option value="493">REPUBLIC BANK</option>


                                        <option value="550">RESPONSE-ONE GH LTD</option>


                                        <option value="542">RICHCO HERBAL LTD</option>


                                        <option value="296">RICHJONES COMPANY LTD</option>


                                        <option value="326">ROBERT AND SONS EYE CENTER</option>


                                        <option value="345">ROCK CARE CLINIC LTD</option>


                                        <option value="506">ROCKMAN PHARMA LTD</option>


                                        <option value="399">ROJAN HERBAL CENTER</option>


                                        <option value="551">ROKI-DOLLS LTD</option>


                                        <option value="293">ROXIN GH LTD</option>


                                        <option value="96">ROYAL BANK</option>


                                        <option value="271">ROYAL BOW COMPANY LTD </option>


                                        <option value="178">ROYAL LIGHTING </option>


                                        <option value="370">ROYAL LIGHTING INVESTMENT LTD</option>


                                        <option value="238">ROYAL RICE COLLECTION</option>


                                        <option value="99">ROYAL UNIBREW</option>


                                        <option value="554">S-O HERBAL CENTRE</option>


                                        <option value="279">SAMARA COMPANY LTD </option>


                                        <option value="138">SAMIVA LTD </option>


                                        <option value="552">SAMOCID LTD</option>


                                        <option value="173">SAMSUNG </option>


                                        <option value="414">SARANATIS GROUP</option>


                                        <option value="402">SASEM PHARMACUETICALS</option>


                                        <option value="136">SATGURU TRAVELS </option>


                                        <option value="124">SAVANNA CIDER</option>


                                        <option value="41">SC JOHNSON</option>


                                        <option value="553">SECOND IMAGE INTERNATIONAL</option>


                                        <option value="649">SECOND KINGS REAL ESTATE</option>


                                        <option value="463">SENETA LTD</option>


                                        <option value="644">SG GHANA BANK</option>


                                        <option value="661">SHANDONIA PROPERTIES</option>


                                        <option value="407">SHARP PAHRMACEUTICALS</option>


                                        <option value="44">SHELL</option>


                                        <option value="253">SHIELD PREMIUM PAINT GH</option>


                                        <option value="516">SHOP AND SAVE</option>


                                        <option value="98">SHOPRITE</option>


                                        <option value="312">SHOPRITE LTD</option>


                                        <option value="294">SIC LIFE </option>


                                        <option value="626">SIGMA HEALTH AND BEAUTY SCHOOL</option>


                                        <option value="52">SILENT BEDDING</option>


                                        <option value="305">SIMBINS FURNITURE LTD</option>


                                        <option value="497">SINAPI ABA SAVINGS AND LOANS</option>


                                        <option value="416">SIPACO LTD</option>


                                        <option value="140">SIVOP </option>


                                        <option value="397">SKD MORNACH LTD</option>


                                        <option value="525">SM MOBILE MART</option>


                                        <option value="85">SMART INVESTMENT</option>


                                        <option value="303">SMOOCH BOUTIQUE</option>


                                        <option value="488">SO KLIN</option>


                                        <option value="298">SOLAK NATURAL CLINIC</option>


                                        <option value="534">SOLARTEK GH LTD</option>


                                        <option value="388">SOLLATEK GHANA</option>


                                        <option value="297">SONIAG GH LTD</option>


                                        <option value="45">SPECIAL INVESTMENTS</option>


                                        <option value="555">SPEED WRITE INTERNATIONAL</option>


                                        <option value="252">STANBIC BANK </option>


                                        <option value="251">STANDARD CHARTERED</option>


                                        <option value="174">STAR ASSURANCE </option>


                                        <option value="466">STAR CAR LTD</option>


                                        <option value="233">STARLIFE INSURANCE</option>


                                        <option value="212">STARTIMES GHANA LTD</option>


                                        <option value="609">STARWINS PRODUCTS LTD</option>


                                        <option value="155">STOUB CARS</option>


                                        <option value="406">STRIKE LTD</option>


                                        <option value="172">SULTANA RICE </option>


                                        <option value="43">SUMARA COMPANY LTD</option>


                                        <option value="365">SUNDA INTERNATIONAL</option>


                                        <option value="604">SUNU ASSURANCE POLICY</option>


                                        <option value="46">SURFLINE</option>


                                        <option value="347">SUZZY HERBAL CENTER</option>


                                        <option value="673">SYINIX ELECTRONICS</option>


                                        <option value="100">SYNDICATED CAPITAL</option>


                                        <option value="278">TAABEA COMPANY LTD</option>


                                        <option value="489">TANDRINA LTD</option>


                                        <option value="674">TECNO</option>


                                        <option value="436">TELAD PHARMACY</option>


                                        <option value="47">TELEDATA</option>


                                        <option value="219">TELEFONIKA  </option>


                                        <option value="648">TELESOL GHANA LTD</option>


                                        <option value="68">Testpko</option>


                                        <option value="49">TIGO</option>


                                        <option value="244">TINATET HERBAL</option>


                                        <option value="50">TISU</option>


                                        <option value="668">TOBESTIN ENT LTD </option>


                                        <option value="467">TOP HERBAL CLINIC</option>


                                        <option value="432">TOPMAN FARMS</option>


                                        <option value="51">TOTAL</option>


                                        <option value="496">TRANS GLOBAL LOGISTICS</option>


                                        <option value="556">TRANSITIONS SERVICES LTD</option>


                                        <option value="517">TRELLIDOR LTD</option>


                                        <option value="540">TRIPPLE ACTION LTD</option>


                                        <option value="127">TROPICAL CABLES &amp; CONDUCTORS LTD</option>


                                        <option value="676">TROYSTEEL CO. LTD</option>


                                        <option value="349">TULLOW</option>


                                        <option value="662">TUMIWURA HERBAL</option>


                                        <option value="469">TUMIWURA RESEARCH AND CLINIC</option>


                                        <option value="633">TURKISH AIRLINES SPECIAL PRICES</option>


                                        <option value="214">TWELLIUM GHANA</option>


                                        <option value="53">U2</option>


                                        <option value="227">UBA</option>


                                        <option value="438">UBER</option>


                                        <option value="103">UMB FINANCIAL CORP</option>


                                        <option value="240">UNIBANK</option>


                                        <option value="359">UNICREDIT</option>


                                        <option value="66">UNILEVER GH LTD </option>


                                        <option value="566">UNIQUE INSURANCE</option>


                                        <option value="358">UNIROB PHARMACEUTICALS</option>


                                        <option value="266">UNITED ARAB EMIRATE  </option>


                                        <option value="234">UNITED DUTCH BEVERAGES</option>


                                        <option value="471">UNITED PENSION TRUST</option>


                                        <option value="433">UNITY OIL</option>


                                        <option value="557">UNIVERSAL LTD</option>


                                        <option value="230">UT BANK</option>


                                        <option value="327">UTRAK SAVINGS AND LOANS</option>


                                        <option value="55">VANGUARD</option>


                                        <option value="133">VANTAGE HOME EXPO </option>


                                        <option value="150">VARTA COMPANY </option>


                                        <option value="117">VICCO DRINKS </option>


                                        <option value="263">VICTORIA DISTILLERIES LTD</option>


                                        <option value="677">VITA WATER LTD</option>


                                        <option value="333">VIVO LTD</option>


                                        <option value="122">VLISCO</option>


                                        <option value="56">VODAFONE</option>


                                        <option value="192">VOLTIC GH LTD</option>


                                        <option value="16">WATANAMAL</option>


                                        <option value="473">WEST HILLS MALL</option>


                                        <option value="250">WESTERN UNION </option>


                                        <option value="624">WESTNET PHARMACEUTICALS LTD</option>


                                        <option value="558">WESTWOOD COLLEGE REMEDIAL</option>


                                        <option value="121">WILMAR AFRICA LTD</option>


                                        <option value="518">WOODIN</option>


                                        <option value="0">Xavier Group</option>


                                        <option value="285">Y &amp; W INDUSTRY GH LTD </option>


                                        <option value="494">YAAKSON HERBAL CENTER</option>


                                        <option value="105">YAFO HERBAL PRODUCTS</option>


                                        <option value="276">YARA GHANA LTD</option>


                                        <option value="169">YAZBEK PAINT </option>


                                        <option value="372">YELLOW AND ORANGE PHARMA LTD</option>


                                        <option value="58">YERIPA</option>


                                        <option value="147">YOKOHAMA TIRE CORP.</option>


                                        <option value="186">ZEERA LOGISTICS </option>


                                        <option value="439">ZENEL LTD</option>


                                        <option value="242">ZENITH BANK</option>


                                        <option value="408">ZIPTV</option>


                                    </select>
                                    <span style="font-size: small;">Can't find your company? <span class="add-company" style="cursor:pointer; color:#337ab7;">add your company</span></span>
                                </div>
                                <div class="company-notice"></div>
                                <div style="display:none" ;="" class="new-company">
                                    <input type="text" class="form-control span12" name="new-company" id="new-company">
        <span style="font-size: small;">Back to company list? <span class="view-company" style="cursor:pointer;
        color:#337ab7;">view companies</span></span>
                                </div>
                                <span>Company <span style="color:red;">*</span></span>
                                <div class="new-company-notice"></div>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <div class="industry">
                                    <select name="industry" id="industry" class="form-control">

                                        <option value="" selected="" disabled="">Please select Industry</option>

                                        <option value="18">AUTOMOTIVES</option>


                                        <option value="4">BEVERAGE</option>


                                        <option value="9">CONSTRUCTION</option>


                                        <option value="19">EDUCATION</option>


                                        <option value="21">ELECTRONICS </option>


                                        <option value="3">FINANCE</option>


                                        <option value="5">FMCG</option>


                                        <option value="22">HOSPITALITY </option>


                                        <option value="8">HOUSEHOLD</option>


                                        <option value="20">INDUSTRIAL</option>


                                        <option value="7">MANUFACTURING</option>


                                        <option value="0">MEDIA</option>


                                        <option value="2">PETROLEUM</option>


                                        <option value="6">PHARMACEUTICALS</option>


                                        <option value="17">POLITICAL</option>


                                        <option value="12">RETAIL</option>


                                        <option value="11">SERVICES</option>


                                        <option value="1">TELCO</option>


                                    </select>
                                    <span style="font-size: small;">Can't find your industry? <span class="add-industry" style="cursor:pointer; color:#337ab7;">add your industry</span></span>
                                </div>
                                <div class="industry-notice"></div>
                                <div style="display:none;" class="new-industry">
                                    <input type="text" class="form-control span12" name="new-industry" id="new-industry">
                                    <span style="font-size: small;">Back to industry list? <span class="view-industry" style="cursor:pointer; color:#337ab7;">view industries</span></span>
                                </div>
                                <span>Industry <span style="color:red;">*</span></span>
                                <div class="new-industry-notice"></div>
                            </label>

                            <div class="form-group  mb-4 ">
                                <input type="checkbox" class="" name="broadcaster" id="broadcaster" value="1" style="padding: 5px;"> I am a Media House
                            </div>

                            <label class="form-group has-float-label mb-4 show-stations" style="display: none;">
                                <div class="station">
                                    <select name="station" id="station" class="form-control">

                                        <option value="" selected="" disabled="">Please select A Station</option>

                                        <option value="79">A1 Radio FM</option>


                                        <option value="9">AdomFM </option>


                                        <option value="5">AdomTV </option>


                                        <option value="62">Agoo FM</option>


                                        <option value="83">Anapua FM</option>


                                        <option value="101">Angel FM</option>


                                        <option value="29">AngelFM </option>


                                        <option value="99">AngelTV</option>


                                        <option value="64">Anigye FM</option>


                                        <option value="19">AsempaFM </option>


                                        <option value="65">Ashh FM</option>


                                        <option value="44">Atlantis RadioFM</option>


                                        <option value="66">Boss FM</option>


                                        <option value="45">Channel R FM</option>


                                        <option value="46">Choice FM</option>


                                        <option value="18">CitiFM </option>


                                        <option value="100">ClassFM</option>


                                        <option value="36">DiamondFM</option>


                                        <option value="86">Dinpa FM</option>


                                        <option value="93">Eastern FM</option>


                                        <option value="97">Emak FM</option>


                                        <option value="69">Focus FM</option>


                                        <option value="67">Fox FM</option>


                                        <option value="68">Freedom FM</option>


                                        <option value="4">GHOne</option>


                                        <option value="96">Goodlife FM</option>


                                        <option value="47">Great FM</option>


                                        <option value="42">GreenFM</option>


                                        <option value="6">GTV </option>


                                        <option value="48">Happy FM</option>


                                        <option value="14">HitzFM </option>


                                        <option value="72">Holy FM</option>


                                        <option value="74">Hope FM</option>


                                        <option value="49">Hot FM</option>


                                        <option value="11">JoyFM </option>


                                        <option value="33">JoyPrime </option>


                                        <option value="70">Jubilee Radio FM</option>


                                        <option value="13">KasapaFM</option>


                                        <option value="30">KessbenFM </option>


                                        <option value="24">Ksi-YFM </option>


                                        <option value="23">Kwese Sports</option>


                                        <option value="71">Light FM</option>


                                        <option value="31">LuvFM </option>


                                        <option value="25">MelodyFM </option>


                                        <option value="3">Metro TV </option>


                                        <option value="32">NhyiraFM </option>


                                        <option value="39">NorthStar RadioFM</option>


                                        <option value="43">ObonuFM</option>


                                        <option value="12">OkayFM </option>


                                        <option value="20">OmanFM </option>


                                        <option value="10">PeaceFM </option>


                                        <option value="89">Pink FM</option>


                                        <option value="50">Pluzz FM</option>


                                        <option value="63">Pravda Radio FM</option>


                                        <option value="51">Prime FM</option>


                                        <option value="78">Radford FM</option>


                                        <option value="61">Radio Ada FM</option>


                                        <option value="76">Radio Freed FM</option>


                                        <option value="52">Radio Gold FM</option>


                                        <option value="81">Radio Gurune FM</option>


                                        <option value="34">Radio JusticeFM</option>


                                        <option value="77">Radio Progress FM</option>


                                        <option value="75">Radio Upper West</option>


                                        <option value="60">Radio XYZ FM</option>


                                        <option value="26">RadioMaxxFM</option>


                                        <option value="27">RadioSilverFM</option>


                                        <option value="38">RadioTamaleFM</option>


                                        <option value="41">Rainbow RadioFM</option>


                                        <option value="82">Rock FM</option>


                                        <option value="98">Rok FM</option>


                                        <option value="37">Savannah RadioFM</option>


                                        <option value="53">Sena Radio FM</option>


                                        <option value="87">Sky FM</option>


                                        <option value="28">SkyPowerFM </option>


                                        <option value="92">Solar FM</option>


                                        <option value="88">Sompa FM</option>


                                        <option value="85">Space FM</option>


                                        <option value="91">Spark FM</option>


                                        <option value="84">Star FM</option>


                                        <option value="17">StarrFM </option>


                                        <option value="54">Sunny FM</option>


                                        <option value="95">Sunrise FM</option>


                                        <option value="55">Sweet Melodies FM</option>


                                        <option value="15">SweetMelodiesFM </option>


                                        <option value="56">Top Radio FM</option>


                                        <option value="1">TV3 </option>


                                        <option value="21">TVAfrica </option>


                                        <option value="57">Uniiq FM</option>


                                        <option value="7">UTV </option>


                                        <option value="58">Vibe FM</option>


                                        <option value="73">Victory FM</option>


                                        <option value="94">Vision FM</option>


                                        <option value="22">Vision1FM </option>


                                        <option value="80">Word FM</option>


                                        <option value="59">XFM</option>


                                        <option value="90">Yes FM</option>


                                        <option value="16">YFM </option>


                                        <option value="35">Zaa RadioFM</option>


                                    </select>
                                    <span style="font-size: small;">Can't find your station? <span class="add-station" style="cursor:pointer; color:#337ab7;">add your station</span></span>
                                </div>
                                <div class="station-notice"></div>
                                <div style="display:none;" class="new-station">
                                    <input type="text" class="form-control span12" name="new-station" id="new-station">
                                    <span style="font-size: small;">Back to station list? <span class="view-station" style="cursor:pointer; color:#337ab7;">view stations</span></span>
                                </div>
                                <span>Station <span style="color:red;">*</span></span>
                                <div class="new-station-notice"></div>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <input id="password" name="password" class="form-control" type="password" placeholder="" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                                <span>Password <span style="color:red;">*</span></span>
                                <div class="password-notice"></div>
                                <div id="message" style="border-radius: 11px 11px 11px 11px; padding-top: 10px;padding-bottom: 10px;
        padding-left: 20px;padding-right: 20px;display:none;">
                                    <div style="font-size: 13px; margin-bottom:5px;"><b>Password Hints:</b></div>
                                    <div style="font-size: 13px;" id="length" class="invalid">Minimum <b>6 characters</b></div>
                                    <div style="font-size: 13px;" id="capital" class="invalid">A <b>capital (uppercase)</b> letter</div>
                                    <div style="font-size: 13px;" id="number" class="invalid">A <b>number</b></div>
                                    <div style="font-size: 13px;" id="white" class="invalid"><b>No white space</b></div>
                                </div>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <input id="confirm-password" name="confirm-password" class="form-control" type="password" placeholder="" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                                <span>Confirm Password <span style="color:red;">*</span></span>
                                <div class="confirm-password-notice" id="matching" style="font-size: 13px; border-radius: 11px; padding: 10px 20px; display: none;">Checking for password match</div>
                            </label>

                            <div class="d-flex justify-content-end align-items-center">
                                <button class="btn btn-primary btn-lg btn-shadow" type="button" onclick="submitRegistration()">REGISTER</button>
                            </div>
                        </form>

                        <!--hide/show new item fields-->
                        <script>
                            $('.add-industry').click(function(){
                                $('.industry').css("display", "none");
                                $('.new-industry').css("display", "block");
                                $('#industry').val('Please select An Industry');
                            });
                            $('.view-industry').click(function(){
                                $('.new-industry').css("display", "none");
                                $('.industry').css("display", "block");
                            });
                            $('.add-company').click(function(){
                                $('.company').css("display", "none");
                                $('.new-company').css("display", "block");
                                $('#company').val('Please select A Company');
                            });
                            $('.view-company').click(function(){
                                $('.new-company').css("display", "none");
                                $('.company').css("display", "block");
                            });

                            $('#broadcaster').click(function () {
                                if($('input[type="checkbox"]').prop("checked") == true){
                                    $('.show-stations').css('display', 'block');
                                    $('#station').attr('required', true);
//      console.log('true');
                                }else{
                                    $('.show-stations').css('display', 'none');
                                    $('#station').attr('required', false);
//      console.log('false');
                                }
                            });

                            $('.add-station').click(function(){
                                $('.station').css("display", "none");
                                $('.new-station').css("display", "block");
                                $('#station').val('Please select A Station');
                            });

                            $('.view-station').click(function(){
                                $('.new-station').css("display", "none");
                                $('.station').css("display", "block");
                            });

                            //check if username exists
                            function checkUsername(){
                                var username = document.getElementById("username").value;
                                $.ajax({
                                    url:"processing/registration.php",
                                    type: "post",
                                    data: "check-username="+username,
                                    success:function(html){
                                        $('.username-notice').html(html);
                                        // console.log(html + username);
                                    }
                                })
                            }
                        </script>

                        <!--Password hints and validation-->
                        <script>

                            document.getElementById("message").style.display = "none";
                            document.getElementById("matching").style.display = "none";

                            var myInput = document.getElementById("password");
                            var white = document.getElementById("white");
                            var capital = document.getElementById("capital");
                            var number = document.getElementById("number");
                            var length = document.getElementById("length");

                            var passmatch= document.getElementById("confirm-password");
                            var matching=document.getElementById("matching");

                            var emailcheck=document.getElementById("email");
                            var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

                            // When the user clicks on the password field, show the message box
                            myInput.onfocus = function() {
                                document.getElementById("message").style.display = "block";
                            }

                            // When the user clicks outside of the password field, hide the message box
                            myInput.onblur = function() {
                                document.getElementById("message").style.display = "none";
                            }

                            // When the user starts to type something inside the password field

                            myInput.onkeyup = function() {
                                // Validate lowercase letters

                                // Validate capital letters
                                var upperCaseLetters = /[A-Z]/g;
                                if(myInput.value.match(upperCaseLetters)) {
                                    capital.classList.remove("invalid");
                                    capital.classList.add("valid");
                                } else {
                                    capital.classList.remove("valid");
                                    capital.classList.add("invalid");
                                }

                                // Validate numbers
                                var numbers = /[0-9]/g;
                                if(myInput.value.match(numbers)) {
                                    number.classList.remove("invalid");
                                    number.classList.add("valid");
                                } else {
                                    number.classList.remove("valid");
                                    number.classList.add("invalid");
                                }

                                // Validate length
                                if(myInput.value.length >= 6) {
                                    length.classList.remove("invalid");
                                    length.classList.add("valid");
                                } else {
                                    length.classList.remove("valid");
                                    length.classList.add("invalid");
                                }

                                //validating white space.
                                if(myInput.value.match(/\s/g)){
                                    white.classList.remove("valid");
                                    white.classList.add("invalid");
                                }
                                else {
                                    white.classList.remove("invalid");
                                    white.classList.add("valid");
                                }


                            }
                            passmatch.onfocus = function() {
                                document.getElementById("matching").style.display = "block";
                            }

                            // When the user clicks outside of the password field, hide the message box
                            passmatch.onblur = function() {
                                document.getElementById("matching").style.display = "none";
                            }
                            passmatch.onkeyup=function(){
                                //check confirmation
                                if(passmatch.value.match(myInput.value)){
                                    matching.classList.remove("invalid");
                                    matching.classList.add("valid");
                                    $('#matching').html('Passwords match');
                                }
                                else if (!passmatch.value.match(myInput.value)){
                                    matching.classList.remove("valid");
                                    matching.classList.add("invalid");
                                    $('#matching').html('Passwords do not match');
                                }
                            }

                            //error notification
                            $(document).ready(function(){
                                let searchParams = new URLSearchParams(window.location.search)
                                searchParams.has('r') // true
                                let param = searchParams.get('r');
                                if(param == 'registration-failed'){
                                    $('.err-notice').html("<div class='alert alert-danger' role='alert'>Registration failed. Please try again.</div>");
                                }
                                if(param == 'account-exists'){
                                    $('.err-notice').html("<div class='alert alert-danger' role='alert'>Account Already Exists. Please check and try again.</div>");
                                }
                                if(param == 'username-exists'){
                                    $('.err-notice').html("<div class='alert alert-danger' role='alert'>The username provided has already been used. Please check and try again.</div>");
                                }
                            })

                            //    check if required field empty
                            function isRequired(field, id){
//        console.log(id);
                                if(field == ''){
                                    $('#'+id).css('border','1px #d00 solid');
                                    $('.'+id+'-notice' ).html("<div class='alert alert-danger' role='alert'>The highlighted field is required.</div>");
                                    return 1;
                                }else{
                                    $('#'+id).css('border','1px solid #d7d7d7');
                                    $('.'+id+'-notice').html('');
                                    return 0;
                                }
                            }

                            //    validate email
                            function validateEmail(id) {
                                var email = document.getElementById('email').value
                                var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                if(!emailReg.test( email )){
                                    $('#'+id).css('border','1px #d00 solid');
                                    $('.'+id+'-notice' ).html("<div class='alert alert-danger' role='alert'>This is not a valid email address.</div>");
                                    return 1;
                                }else{
                                    $('.'+id+'-notice').html('');
                                    $('#'+id).css('border','1px solid #d7d7d7');
                                    return 0;
                                }
                            }

                            //    check for matching password
                            function passwordMatch(id){
                                var password = document.getElementById('password').value,
                                        confirmPassword = document.getElementById('confirm-password').value;
                                if(password != confirmPassword){
                                    $('#'+id).css('border','1px #d00 solid');
                                    $('.'+id+'-notice' ).html("<div class='alert alert-danger' role='alert'>Passwords do not match</div>");
                                    return 1;
                                }else{
                                    $('#'+id).css('border','1px solid #d7d7d7');
                                    $('.'+id+'-notice').html('');
                                    return 0;
                                }
                            }

                            //    form submission process
                            function submitRegistration(){
                                let fname = document.getElementById('fname').value,
                                        lname = document.getElementById('lname').value,
                                        username = document.getElementById('username').value,
                                        phone = document.getElementById('phone').value,
                                        email = document.getElementById('email').value,
                                        company = document.getElementById('company').value,
                                        companyNew = document.getElementById('new-company').value,
                                        industry = document.getElementById('industry').value,
                                        industryNew = document.getElementById('new-industry').value,
                                        station = document.getElementById('station').value,
                                        stationNew = document.getElementById('new-station').value,
                                        password = document.getElementById('password').value,
                                        confirmPassword = document.getElementById('confirm-password').value,
                                        mediaHse =0;
                                if($('#broadcaster').prop("checked")== true){
                                    mediaHse=1;
                                }

                                //check required
                                if(isRequired(fname, 'fname')==0 && isRequired(lname, 'lname')==0 && isRequired(username, 'username')==0 && isRequired(email, 'email')==0 && validateEmail('email')==0 && isRequired(password, 'password')==0  && isRequired(confirmPassword, 'confirm-password')==0 && passwordMatch('confirm-password')==0 ){
                                    //check for other required fields
                                    var dataString = "fname="+fname+"&lname="+lname+"&username="+username+"&phone="+phone+"&email="+email+"&company="+company+"&new-company="+companyNew+"&industry="+industry+"&new-industry="+industryNew+"&broadcaster="+mediaHse+"&station="+station+"&new-station="+stationNew+"&password="+password;
                                    $.ajax({
                                        url: "processing/registration.php",
                                        method: "post",
                                        data: dataString,
                                        success: function(html){
                                            var data = html.split('|');
                                            console.log(data[0]);
                                            if(data[0]==0){
                                                $('.err-notice').html(data[1]);
                                                window.location.href = "http://aya.mediawatch.com.gh/v3/Pages.Auth.Activate.php?user="+username;
                                            }else {
                                                $('.err-notice').html(data[1]);
                                            }
                                        }
                                    });
                                }
                            }

                        </script></div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="js/vendor/jquery-3.3.1.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap.bundle.min.js" style="opacity: 1;"></script>
<script src="js/dore.script.js" style="opacity: 1;"></script>
<script src="js/scripts.js" style="opacity: 1;"></script><div class="theme-colors default-transition" style="opacity: 1;"><div class="p-4"><p class="text-muted mb-2">Light Theme</p><div class="d-flex flex-row justify-content-between mb-4"><a href="#" data-theme="dore.light.blue.min.css" class="theme-color theme-color-blue active"></a><a href="#" data-theme="dore.light.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.light.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.light.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.light.red.min.css" class="theme-color theme-color-red"></a></div><p class="text-muted mb-2">Dark Theme</p><div class="d-flex flex-row justify-content-between"><a href="#" data-theme="dore.dark.blue.min.css" class="theme-color theme-color-blue"></a><a href="#" data-theme="dore.dark.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.dark.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.dark.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.dark.red.min.css" class="theme-color theme-color-red"></a></div></div><div class="p-4"><p class="text-muted mb-2">Border Radius</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="roundedRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="rounded"><label class="custom-control-label" for="roundedRadio">Rounded</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="flatRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="flat" checked="checked"><label class="custom-control-label" for="flatRadio">Flat</label></div></div><div class="p-4"><p class="text-muted mb-2">Direction</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="ltrRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="ltr" checked="checked"><label class="custom-control-label" for="ltrRadio">Ltr</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="rtlRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="rtl"><label class="custom-control-label" for="rtlRadio">Rtl</label></div></div><a href="#" class="theme-button"> <i class="simple-icon-magic-wand"></i> </a></div>
<!--registration form default display-->
<script style="opacity: 1;">
    $.ajax({
        url:"forms/registration.Form.php",
        type: "GET",
        success:function(html){
            $('.form-side').html(html);
        }
    })
</script>


</body></html>