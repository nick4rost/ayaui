<html dir="ltr" lang="en"><head>
    <meta charset="UTF-8">
    <title>AYA - Smart Media Buy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="font/iconsmind-s/css/iconsminds.css">
    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/vendor/bootstrap.rtl.only.min.css">
    <link rel="stylesheet" href="css/vendor/fullcalendar.min.css">
    <link rel="stylesheet" href="css/vendor/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/datatables.responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/select2.min.css">
    <link rel="stylesheet" href="css/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="css/vendor/glide.core.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-stars.css">
    <link rel="stylesheet" href="css/vendor/nouislider.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="css/vendor/component-custom-switch.min.css">
    <link rel="stylesheet" type="text/css" href="css/dore.light.blue.min.css"><link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/custom.css">
    <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>

<body id="app-container" class="menu-default ltr flat" cz-shortcut-listen="true">
<nav class="navbar fixed-top" style="opacity: 1;">
    <div class="d-flex align-items-center navbar-left">
        <a href="#" class="menu-button d-none d-md-block">
            <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                <rect x="0.48" y="0.5" width="7" height="1"></rect>
                <rect x="0.48" y="7.5" width="7" height="1"></rect>
                <rect x="0.48" y="15.5" width="7" height="1"></rect>
            </svg>
            <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                <rect x="1.56" y="0.5" width="16" height="1"></rect>
                <rect x="1.56" y="7.5" width="16" height="1"></rect>
                <rect x="1.56" y="15.5" width="16" height="1"></rect>
            </svg>
        </a>

        <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                <rect x="0.5" y="0.5" width="25" height="1"></rect>
                <rect x="0.5" y="7.5" width="25" height="1"></rect>
                <rect x="0.5" y="15.5" width="25" height="1"></rect>
            </svg>
        </a>

        <a class="navbar-logo" href="Admin.Dashboard.Default.html">
            <span class="logo d-none d-xs-block"></span>
            <span class="logo-mobile d-block d-xs-none"></span>
        </a>

    </div>

    <div class="navbar-right">

        <div class="user d-inline-block">
            <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="name">admin</span>
                    <span>
                        <img alt="Profile Picture" src="img/profile-pic-l.jpg">
                    </span>
            </button>

            <div class="dropdown-menu dropdown-menu-right mt-3">
                <a class="dropdown-item" href="processing/logout.php">Sign out</a>
            </div>
        </div>
    </div>
</nav>
<div class="menu" style="opacity: 1;">
    <script>
        var index = window.location.href.lastIndexOf("/") + 1;
        var filenameWithExtension = window.location.href.substr(index);
        var page = filenameWithExtension.split(".")[0]; // <-- added this line
        var role =0

        if(role == 4 && page != "Client"){
            window.location.href = "Client.Dashboard.Default.php";
        }else if(role == 5 && page != "Media"){
            window.location.href = "Media.Dashboard.Default.php";
        }else if(role != 0 && role != 4 && role != 5){
            window.location.href = "processing/logout.php";
        }
    </script>
    <div class="main-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled">
                <!--                admin menu-->
                <li id="Admin">
                    <a href="#dashboard">
                        <i class="iconsminds-administrator"></i>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
                <!--                client menu-->
                <li id="Client">
                    <a href="#client">
                        <i class="iconsminds-business-man-woman"></i>
                        <span>Client Dashboard</span>
                    </a>
                </li>
                <!--                media menu-->
                <li id="Media" class="active">
                    <a href="#media">
                        <i class="iconsminds-building"></i>
                        <span>Media House Dashboard</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div>

    <div class="sub-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled" data-link="dashboard">
                <li>
                    <a href="Admin.Dashboard.Default.php">
                        <i class="simple-icon-book-open"></i><span class="d-inline-block">Advert Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Bookings.php">
                        <i class="simple-icon-calendar"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Monitored.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Monitored</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Insight.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Media Insight</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Social.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="client">
                <li>
                    <a href="Client.Dashboard.Default.php">
                        <i class="iconsminds-dashboard"></i><span class="d-inline-block">My Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Library.php">
                        <i class="simple-icon-playlist"></i><span class="d-inline-block">Adverts Library</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Details.php">
                        <i class="iconsminds-letter-open"></i><span class="d-inline-block">Advert Details</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Booked.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Booked Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Social.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="media" style="display: block;">
                <li class="active">
                    <a href="Media.Dashboard.Default.php">
                        <i class="simple-icon-event"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Media.Dashboard.Report.php">
                        <i class="simple-icon-chart"></i><span class="d-inline-block">Reports</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div></div>

<main style="opacity: 1;" class="default-transition">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Bookings</h1>
                <div class="separator mb-5"></div>
            </div>

            <div class="col-12 mb-3 data-search ">
                <div class="col-sm-12 mt-2 message-box">
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-12 mb-4">
                <div class="card">
                    <div class="card-body">
                        <div id="example_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row view-filter"><div class="col-sm-12 mb-2"><div class="float-right"><div class="dt-buttons btn-group">          <button class="btn btn-secondary buttons-copy buttons-html5" tabindex="0" aria-controls="example"><span>Copy</span></button> <button class="btn btn-secondary buttons-excel buttons-html5" tabindex="0" aria-controls="example"><span>Excel</span></button> <button class="btn btn-secondary buttons-csv buttons-html5" tabindex="0" aria-controls="example"><span>CSV</span></button> <button class="btn btn-secondary buttons-pdf buttons-html5" tabindex="0" aria-controls="example"><span>PDF</span></button> <button class="btn btn-secondary buttons-print" tabindex="0" aria-controls="example"><span>Print</span></button> </div></div></div><div class="col-sm-12"><div class="float-right"><div class="dataTables_length" id="example_length"><label>Items Per Page <select name="example_length" aria-controls="example" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select></label></div></div><div class="float-left"><div id="example_filter" class="dataTables_filter"><label><input type="search" class="form-control form-control-sm" placeholder="Search..." aria-controls="example"></label></div></div><div class="clearfix"></div></div></div><table id="example" class="data-table table-striped data-table-media-booking dataTable no-footer" role="grid" aria-describedby="example_info">
                                <thead>
                                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 56.8167px;" aria-sort="ascending" aria-label="Company: activate to sort column descending">Company</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 69.8667px;" aria-label="Advert: activate to sort column ascending">Advert</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 28.1px;" aria-label="Type: activate to sort column ascending">Type</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 59.6333px;" aria-label="Length (Secs): activate to sort column ascending">Length (Secs)</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 56.7667px;" aria-label="Date: activate to sort column ascending">Date</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 70.6833px;" aria-label="Segment: activate to sort column ascending">Segment</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 48.7833px;" aria-label="Slot (Secs): activate to sort column ascending">Slot (Secs)</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 15.55px;" aria-label="PL: activate to sort column ascending">PL</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 113.233px;" aria-label="File: activate to sort column ascending">File</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 141.767px;" aria-label="Action: activate to sort column ascending">Action</th></tr>
                                </thead>
                                <tbody>



















                                <tr class="small odd" role="row">
                                    <td class="sorting_1"></td>
                                    <td>my awesome ad again</td>
                                    <td>Radio</td>
                                    <td>45</td>
                                    <td>2020-03-18</td>
                                    <td>8:00 - 9:30</td>
                                    <td>15</td><td><span class="119-current-status">Yes</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/my sample ad.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-danger small my-small-btn" id="119" rel="mp-status" title="Cancel Schedule" onclick="manageSchedule(119,'mp-status' )">Cancel Schedule</button></td></tr><tr class="small even" role="row">
                                    <td class="sorting_1"></td>
                                    <td>my awesome ad again</td>
                                    <td>Radio</td>
                                    <td>45</td>
                                    <td>2020-03-18</td>
                                    <td>8:00 - 9:30</td>
                                    <td>15</td><td><span class="120-current-status">Yes</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/my sample ad.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-danger small my-small-btn" id="120" rel="mp-status" title="Cancel Schedule" onclick="manageSchedule(120,'mp-status' )">Cancel Schedule</button></td></tr><tr class="small odd" role="row">
                                    <td class="sorting_1"></td>
                                    <td>my awesome ad again</td>
                                    <td>Radio</td>
                                    <td>45</td>
                                    <td>2020-03-18</td>
                                    <td>13:00 - 14:00</td>
                                    <td>15</td><td><span class="121-current-status">Yes</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/my sample ad.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-danger small my-small-btn" id="121" rel="mp-status" title="Cancel Schedule" onclick="manageSchedule(121,'mp-status' )">Cancel Schedule</button></td></tr><tr class="small even" role="row">
                                    <td class="sorting_1">COCA COLA</td>
                                    <td>cola mango</td>
                                    <td>Radio</td>
                                    <td>23</td>
                                    <td>2020-03-31</td>
                                    <td>00:30:00 - 01:30:00</td>
                                    <td>30</td><td><span class="125-current-status">No</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/file_example_MP3_1MG.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-primary small my-small-btn" id="125" rel="mp-status" title="Confirm Schedule" onclick="manageSchedule(125,'mp-status' )">Confirm Schedule</button></td></tr><tr class="small odd" role="row">
                                    <td class="sorting_1">COCA COLA</td>
                                    <td>Kwachua mamili</td>
                                    <td>Radio</td>
                                    <td>20</td>
                                    <td>2020-03-31</td>
                                    <td>01:00:00 - 02:00:00</td>
                                    <td>60</td><td><span class="128-current-status">No</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/file_example_MP3_1MG.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-primary small my-small-btn" id="128" rel="mp-status" title="Confirm Schedule" onclick="manageSchedule(128,'mp-status' )">Confirm Schedule</button></td></tr><tr class="small even" role="row">
                                    <td class="sorting_1">COCA COLA</td>
                                    <td>Kwachua mamili</td>
                                    <td>Radio</td>
                                    <td>20</td>
                                    <td>2020-03-31</td>
                                    <td>02:30:00 - 03:30:00</td>
                                    <td>60</td><td><span class="129-current-status">No</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/file_example_MP3_1MG.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-primary small my-small-btn" id="129" rel="mp-status" title="Confirm Schedule" onclick="manageSchedule(129,'mp-status' )">Confirm Schedule</button></td></tr><tr class="small odd" role="row">
                                    <td class="sorting_1">COCA COLA</td>
                                    <td>Kwachua mamili</td>
                                    <td>Radio</td>
                                    <td>20</td>
                                    <td>2020-03-31</td>
                                    <td>01:00:00 - 02:00:00</td>
                                    <td>60</td><td><span class="130-current-status">Yes</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/file_example_MP3_1MG.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-danger small my-small-btn" id="130" rel="mp-status" title="Cancel Schedule" onclick="manageSchedule(130,'mp-status' )">Cancel Schedule</button></td></tr><tr class="small even" role="row">
                                    <td class="sorting_1">COCA COLA</td>
                                    <td>Kwachua mamili</td>
                                    <td>Radio</td>
                                    <td>20</td>
                                    <td>2020-03-31</td>
                                    <td>02:30:00 - 03:30:00</td>
                                    <td>60</td><td><span class="131-current-status">No</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/file_example_MP3_1MG.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-primary small my-small-btn" id="131" rel="mp-status" title="Confirm Schedule" onclick="manageSchedule(131,'mp-status' )">Confirm Schedule</button></td></tr><tr class="small odd" role="row">
                                    <td class="sorting_1">COCA COLA</td>
                                    <td>Kwachua mamili</td>
                                    <td>Radio</td>
                                    <td>20</td>
                                    <td>2020-03-31</td>
                                    <td>06:30:00 - 07:30:00</td>
                                    <td>60</td><td><span class="132-current-status">Yes</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/file_example_MP3_1MG.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-danger small my-small-btn" id="132" rel="mp-status" title="Cancel Schedule" onclick="manageSchedule(132,'mp-status' )">Cancel Schedule</button></td></tr><tr class="small even" role="row">
                                    <td class="sorting_1">COCA COLA</td>
                                    <td>Kwachua mamili</td>
                                    <td>Radio</td>
                                    <td>20</td>
                                    <td>2019-07-23</td>
                                    <td>01:00:00 - 02:00:00</td>
                                    <td>60</td><td><span class="133-current-status">No</span></td><td><a class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal3" onclick="showAdvert(&quot;myads/file_example_MP3_1MG.mp3&quot;, &quot;&quot;);">View Advert</a></td><td><button class="btn btn-primary small my-small-btn" id="133" rel="mp-status" title="Confirm Schedule" onclick="manageSchedule(133,'mp-status' )">Confirm Schedule</button></td></tr></tbody>
                            </table><div class="row view-pager"><div class="col-sm-12"><div class="text-center"><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 19 entries</div><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination pagination-sm"><li class="paginate_button page-item previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link prev"><i class="simple-icon-arrow-left"></i></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0" class="page-link next"><i class="simple-icon-arrow-right"></i></a></li></ul></div></div></div></div></div>
                    </div>
                </div>

                <!-- Advert Display Media Modal -->
                <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title advert-title-display" id="exampleModalLabel3"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body display-advert">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<footer class="page-footer" style="opacity: 1;">
    <div class="footer-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="mb-0 text-muted">ColoredStrategies 2019</p>
                </div>
                <div class="col-sm-6 d-none d-sm-block">
                    <ul class="breadcrumb pt-0 pr-0 float-right">
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Review</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Purchase</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Docs</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/vendor/jquery-3.3.1.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/Chart.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/chartjs-plugin-datalabels.js" style="opacity: 1;"></script>
<script src="js/vendor/moment.min.js" style="opacity: 1;"></script>
<script src="js/vendor/fullcalendar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/datatables.min.js" style="opacity: 1;"></script>
<script src="js/vendor/perfect-scrollbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/progressbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/jquery.barrating.min.js" style="opacity: 1;"></script>
<script src="js/vendor/select2.full.js" style="opacity: 1;"></script>
<script src="js/vendor/nouislider.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap-datepicker.js" style="opacity: 1;"></script>
<script src="js/vendor/Sortable.js" style="opacity: 1;"></script>
<script src="js/vendor/mousetrap.min.js" style="opacity: 1;"></script>
<script src="js/vendor/glide.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/dataTables.buttons.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.flash.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.html5.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.print.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/JSZip/jszip.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/pdfmake.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/vfs_fonts.js" style="opacity: 1;"></script>
<script src="js/dore.script.js" style="opacity: 1;"></script>
<script src="js/scripts.js" style="opacity: 1;"></script><div class="theme-colors default-transition" style="opacity: 1;"><div class="p-4"><p class="text-muted mb-2">Light Theme</p><div class="d-flex flex-row justify-content-between mb-4"><a href="#" data-theme="dore.light.blue.min.css" class="theme-color theme-color-blue active"></a><a href="#" data-theme="dore.light.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.light.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.light.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.light.red.min.css" class="theme-color theme-color-red"></a></div><p class="text-muted mb-2">Dark Theme</p><div class="d-flex flex-row justify-content-between"><a href="#" data-theme="dore.dark.blue.min.css" class="theme-color theme-color-blue"></a><a href="#" data-theme="dore.dark.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.dark.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.dark.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.dark.red.min.css" class="theme-color theme-color-red"></a></div></div><div class="p-4"><p class="text-muted mb-2">Border Radius</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="roundedRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="rounded"><label class="custom-control-label" for="roundedRadio">Rounded</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="flatRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="flat" checked="checked"><label class="custom-control-label" for="flatRadio">Flat</label></div></div><div class="p-4"><p class="text-muted mb-2">Direction</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="ltrRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="ltr" checked="checked"><label class="custom-control-label" for="ltrRadio">Ltr</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="rtlRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="rtl"><label class="custom-control-label" for="rtlRadio">Rtl</label></div></div><a href="#" class="theme-button"> <i class="simple-icon-magic-wand"></i> </a></div>
<script src="js/media.custom.js" style="opacity: 1;"></script>




</body></html>