<html dir="ltr" lang="en"><head>
    <meta charset="UTF-8">
    <title>AYA - Smart Media Buy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="font/iconsmind-s/css/iconsminds.css">
    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/vendor/bootstrap.rtl.only.min.css">
    <link rel="stylesheet" href="css/vendor/fullcalendar.min.css">
    <link rel="stylesheet" href="css/vendor/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/datatables.responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="css/vendor/select2.min.css">
    <link rel="stylesheet" href="css/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="css/vendor/glide.core.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-stars.css">
    <link rel="stylesheet" href="css/vendor/nouislider.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="css/vendor/component-custom-switch.min.css">
    <link rel="stylesheet" type="text/css" href="css/dore.light.blue.min.css"><link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/custom.css">
    <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>

<body id="app-container" class="menu-default ltr flat" cz-shortcut-listen="true">
<nav class="navbar fixed-top" style="opacity: 1;">
    <div class="d-flex align-items-center navbar-left">
        <a href="#" class="menu-button d-none d-md-block">
            <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                <rect x="0.48" y="0.5" width="7" height="1"></rect>
                <rect x="0.48" y="7.5" width="7" height="1"></rect>
                <rect x="0.48" y="15.5" width="7" height="1"></rect>
            </svg>
            <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                <rect x="1.56" y="0.5" width="16" height="1"></rect>
                <rect x="1.56" y="7.5" width="16" height="1"></rect>
                <rect x="1.56" y="15.5" width="16" height="1"></rect>
            </svg>
        </a>

        <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                <rect x="0.5" y="0.5" width="25" height="1"></rect>
                <rect x="0.5" y="7.5" width="25" height="1"></rect>
                <rect x="0.5" y="15.5" width="25" height="1"></rect>
            </svg>
        </a>

        <a class="navbar-logo" href="Admin.Dashboard.Default.html">
            <span class="logo d-none d-xs-block"></span>
            <span class="logo-mobile d-block d-xs-none"></span>
        </a>

    </div>

    <div class="navbar-right">

        <div class="user d-inline-block">
            <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="name">admin</span>
                    <span>
                        <img alt="Profile Picture" src="img/profile-pic-l.jpg">
                    </span>
            </button>

            <div class="dropdown-menu dropdown-menu-right mt-3">
                <a class="dropdown-item" href="processing/logout.php">Sign out</a>
            </div>
        </div>
    </div>
</nav>
<div class="menu" style="opacity: 1;">
    <script>
        var index = window.location.href.lastIndexOf("/") + 1;
        var filenameWithExtension = window.location.href.substr(index);
        var page = filenameWithExtension.split(".")[0]; // <-- added this line
        var role =0

        if(role == 4 && page != "Client"){
            window.location.href = "Client.Dashboard.Default.php";
        }else if(role == 5 && page != "Media"){
            window.location.href = "Media.Dashboard.Default.php";
        }else if(role != 0 && role != 4 && role != 5){
            window.location.href = "processing/logout.php";
        }
    </script>
    <div class="main-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled">
                <!--                admin menu-->
                <li id="Admin">
                    <a href="#dashboard">
                        <i class="iconsminds-administrator"></i>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
                <!--                client menu-->
                <li id="Client" class="active">
                    <a href="#client">
                        <i class="iconsminds-business-man-woman"></i>
                        <span>Client Dashboard</span>
                    </a>
                </li>
                <!--                media menu-->
                <li id="Media">
                    <a href="#media">
                        <i class="iconsminds-building"></i>
                        <span>Media House Dashboard</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div>

    <div class="sub-menu default-transition">
        <div class="scroll ps">
            <ul class="list-unstyled" data-link="dashboard">
                <li>
                    <a href="Admin.Dashboard.Default.php">
                        <i class="simple-icon-book-open"></i><span class="d-inline-block">Advert Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Bookings.php">
                        <i class="simple-icon-calendar"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Monitored.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Monitored</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Insight.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Media Insight</span>
                    </a>
                </li>
                <li>
                    <a href="Admin.Dashboard.Social.php">
                        <i class="iconsminds-video"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="client" style="display: block;">
                <li>
                    <a href="Client.Dashboard.Default.php">
                        <i class="iconsminds-dashboard"></i><span class="d-inline-block">My Dashboard</span>
                    </a>
                </li>
                <li class="active">
                    <a href="Client.Dashboard.Library.php">
                        <i class="simple-icon-playlist"></i><span class="d-inline-block">Adverts Library</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Details.php">
                        <i class="iconsminds-letter-open"></i><span class="d-inline-block">Advert Details</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Booked.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Booked Spaces</span>
                    </a>
                </li>
                <li>
                    <a href="Client.Dashboard.Social.php">
                        <i class="iconsminds-check"></i><span class="d-inline-block">Social Media</span>
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="media">
                <li>
                    <a href="Media.Dashboard.Default.php">
                        <i class="simple-icon-event"></i><span class="d-inline-block">Bookings</span>
                    </a>
                </li>
                <li>
                    <a href="Media.Dashboard.Report.php">
                        <i class="simple-icon-chart"></i><span class="d-inline-block">Reports</span>
                    </a>
                </li>
            </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div></div>

<main style="opacity: 1;" class="default-transition">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Adverts Library</h1>
                <div class="separator mb-5"></div>
            </div>

            <div class="col-12 mb-3 data-search ">
                <div class="col-sm-12 text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForm();">Buy Advert Space</button>
                    <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="showAddAdvertForm();">Add New Advert</button>
                </div>
                <div class="col-sm-12 mt-2 message-box">
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-12 mb-4">
                <div class="card">
                    <div class="card-body main-display">
<!--                        <div id="example_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row view-filter"><div class="col-sm-12 mb-2"><div class="float-right"><div class="dt-buttons btn-group">          <button class="btn btn-secondary buttons-copy buttons-html5" tabindex="0" aria-controls="example"><span>Copy</span></button> <button class="btn btn-secondary buttons-excel buttons-html5" tabindex="0" aria-controls="example"><span>Excel</span></button> <button class="btn btn-secondary buttons-csv buttons-html5" tabindex="0" aria-controls="example"><span>CSV</span></button> <button class="btn btn-secondary buttons-pdf buttons-html5" tabindex="0" aria-controls="example"><span>PDF</span></button> <button class="btn btn-secondary buttons-print" tabindex="0" aria-controls="example"><span>Print</span></button> </div></div></div><div class="col-sm-12"><div class="float-right"><div class="dataTables_length" id="example_length"><label>Items Per Page <select name="example_length" aria-controls="example" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select></label></div></div><div class="float-left"><div id="example_filter" class="dataTables_filter"><label><input type="search" class="form-control form-control-sm" placeholder="Search..." aria-controls="example"></label></div></div><div class="clearfix"></div></div></div>-->
                            <table id="example" class="data-table table-striped data-table-client-library dataTable no-footer" role="grid" aria-describedby="example_info" style="width: 983px;">
                                <thead>
                                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 0px;" aria-sort="ascending" aria-label=": activate to sort column descending" hidden=""></th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 90px;" aria-label="Added On: activate to sort column ascending">Added On</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 156px;" aria-label="Advert Name: activate to sort column ascending">Advert Name</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 92px;" aria-label="Advert Type: activate to sort column ascending">Advert Type</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 102px;" aria-label="Length (Secs): activate to sort column ascending">Length (Secs)</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 92px;" aria-label="Company: activate to sort column ascending">Company</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 265px;" aria-label="Action: activate to sort column ascending">Action</th></tr>
                                </thead>
                                <tbody>
                                <tr id="6" role="row" class="odd">
                                    <td class="sorting_1" hidden="">6</td>
                                    <td>2020-03-31</td>
                                    <td>Kwachua mamili</td>
                                    <td>Radio</td>
                                    <td>20</td>
                                    <td>COCA COLA</td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditAdvertForm(6)" ;="">Edit</button>
                                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForForm(&quot;Kwachua mamili&quot;, 6)" ;="">Book</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteAdvert(6);">Delete</button></td>
                                </tr><tr id="7" role="row" class="even">
                                    <td class="sorting_1" hidden="">7</td>
                                    <td>2020-03-18</td>
                                    <td>cola mango</td>
                                    <td>Radio</td>
                                    <td>23</td>
                                    <td>COCA COLA</td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditAdvertForm(7)" ;="">Edit</button>
                                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForForm(&quot;cola mango&quot;, 7)" ;="">Book</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteAdvert(7);">Delete</button></td>
                                </tr><tr id="8" role="row" class="odd">
                                    <td class="sorting_1" hidden="">8</td>
                                    <td>2020-03-18</td>
                                    <td>File Sample</td>
                                    <td>Radio</td>
                                    <td>60</td>
                                    <td>Xavier</td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditAdvertForm(8)" ;="">Edit</button>
                                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForForm(&quot;File Sample&quot;, 8)" ;="">Book</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteAdvert(8);">Delete</button></td>
                                </tr><tr id="9" role="row" class="even">
                                    <td class="sorting_1" hidden="">9</td>
                                    <td>2020-03-18</td>
                                    <td>my awesome ad again</td>
                                    <td>Radio</td>
                                    <td>45</td>
                                    <td></td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditAdvertForm(9)" ;="">Edit</button>
                                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForForm(&quot;my awesome ad again&quot;, 9)" ;="">Book</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteAdvert(9);">Delete</button></td>
                                </tr><tr id="10" role="row" class="odd">
                                    <td class="sorting_1" hidden="">10</td>
                                    <td>2020-03-18</td>
                                    <td>Wild Wild West</td>
                                    <td>Radio</td>
                                    <td>45</td>
                                    <td>Xavier</td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditAdvertForm(10)" ;="">Edit</button>
                                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForForm(&quot;Wild Wild West&quot;, 10)" ;="">Book</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteAdvert(10);">Delete</button></td>
                                </tr><tr id="11" role="row" class="even">
                                    <td class="sorting_1" hidden="">11</td>
                                    <td>2020-03-18</td>
                                    <td>All My Life</td>
                                    <td>TV</td>
                                    <td>60</td>
                                    <td>Xavier</td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditAdvertForm(11)" ;="">Edit</button>
                                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForForm(&quot;All My Life&quot;, 11)" ;="">Book</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteAdvert(11);">Delete</button></td>
                                </tr><tr id="12" role="row" class="odd">
                                    <td class="sorting_1" hidden="">12</td>
                                    <td>2020-03-18</td>
                                    <td>werer</td>
                                    <td>Radio</td>
                                    <td>45</td>
                                    <td>Xavier</td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditAdvertForm(12)" ;="">Edit</button>
                                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForForm(&quot;werer&quot;, 12)" ;="">Book</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteAdvert(12);">Delete</button></td>
                                </tr><tr id="13" role="row" class="even">
                                    <td class="sorting_1" hidden="">13</td>
                                    <td>2020-03-18</td>
                                    <td>werer</td>
                                    <td>Radio</td>
                                    <td>30</td>
                                    <td>testpko</td>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" onclick="showEditAdvertForm(13)" ;="">Edit</button>
                                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal2" onclick="buyAdvertForForm(&quot;werer&quot;, 13)" ;="">Book</button>
                                        <button type="button" class="btn btn-danger" onclick="deleteAdvert(13);">Delete</button></td>
                                </tr></tbody>
                            </table>
<!--                        <div class="row view-pager"><div class="col-sm-12"><div class="text-center"><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 8 of 8 entries</div><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination pagination-sm"><li class="paginate_button page-item previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link prev"><i class="simple-icon-arrow-left"></i></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item next disabled" id="example_next"><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link next"><i class="simple-icon-arrow-right"></i></a></li></ul></div></div></div></div></div>-->
<!--                    </div>-->
                </div>


                    <!-- Add New Advert Space Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add New Avert</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="form-view" id="form-view">
                                    <div class="login-content">
                                        <div class="login-form">
                                            <form method="post" action="processing/Client.Processing.php" enctype="multipart/form-data" class="form-horizontal">
                                                <div class="row mb-3" style="color: red; padding-bottom:5px;">If Your Ad is larger than 45MB please contact support on 0277977000.<br></div>
                                                <div class="form-elements">
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="text-input" class="">Ad Name</label></div>
                                                        <div class="col-12 col-md-8"><input type="text" id="adname" name="adname" placeholder="Enter advertisment name" value="" class="form-control" required=""></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="text-input" class="">Ad Type</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <select id="adtype" name="adtype" class="form-control" required="">
                                                                <option selected="" disabled="">Select Ad Type</option>
                                                                <option value="T">TV</option>
                                                                <option value="R">Radio</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="text-input" class="">Ad Duration</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <select id="adduration" name="adduration" class="form-control" required="">
                                                                <option selected="" disabled="">Select the closest duration</option>
                                                                <option value="30">30 Secs</option>
                                                                <option value="45">45 Secs</option>
                                                                <option value="60">60 Secs</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="file-input" class="">Upload Ad File <br>(Max 45MB)</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                                                <input type="file" class="sr-only" id="inputImage" name="fileToUpload" accept=".mp4, .mp3" required="">
                                                                Select File
                                                            </label>
                                                            <br><span class="small"></span></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-12 small" style="color: red; padding-bottom:5px;">If Ad is for drugs or consumables (food, drinks etc.) please upload FDA Certificate (pdf)<br></div>
                                                        <div class="col col-md-4"><label for="file-input" class="">Upload Certificate<br>(Max 45MB)</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                                                <input type="file" class="sr-only" id="inputImage" name="certToUpload" accept=".pdf,.doc,.docx">
                                                                Select File
                                                            </label>
                                                            <br><span class="small"></span></div>
                                                    </div>
                                                </div>
                                                <div class="form-footer text-right border-top pt-3">
                                                    <button type="submit" class="btn btn-primary" id="adsubmit" name="adsubmit" style="display: inline-block; "> Submit </button>
                                                    <button type="reset" class="btn btn-warning" style=" display: inline-block; ">
                                                        <i class="fa fa-refresh"></i> Reset
                                                    </button>
                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger" style="display: inline-block; ">
                                                        <i class="fa fa-close"></i> Cancel
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Buy Advert Space Modal -->
                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel2">Buy Avert Space</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div>
                                        <select class="form-control" style="width: 350px;" id="select-ad" name="select-ad" required=""><option value="" selected="" disabled="">Select an advert</option><option value="Kwachua mamili">Kwachua mamili</option><option value="cola mango">cola mango</option><option value="File Sample">File Sample</option><option value="my awesome ad again">my awesome ad again</option><option value="Wild Wild West">Wild Wild West</option><option value="All My Life">All My Life</option><option value="werer">werer</option><option value="werer">werer</option></select><br></div><div class="select-mediatype">
                                        <select class="form-control" style="width: 350px;" id="select-mt" name="select-mt" ><option value="" selected="" disabled="">Select a media type</option><option value="TV">TV</option><option value="Radio">Radio</option></select><br></div><div class="select-region" style="display: block;"><h4 class="regionsLabel">Select a region</h4><div class="custom-control custom-checkbox regions-list mb-2">
                                            <input type="checkbox" class="custom-control-input" id="all-regions" name="select-region" value="1"  checked="">
                                            <label class="custom-control-label" for="all-regions">ALL REGIONS</label>
                                        </div>
                                        <div class="regions-list-wrap" style="display: none;">
                                            <!--                                        <div class="regionsbtn btn  btn-outline-primary mt-2 text-right" >Fetch</div></div><br></div><div class="radio-tv"><div class="select-station"><select class="form-control" style="width: 350px;" id="select-station" name="select-station" onchange="getCalendar();"><option value="" selected="" disabled="">Select a station</option><option value="0">View All</option><option value="51">Prime FM</option><option value="46">Choice FM</option><option value="48">Happy FM</option><option value="19">AsempaFM </option><option value="15">SweetMelodiesFM </option><option value="31">LuvFM </option><option value="30">KessbenFM </option><option value="65">Ashh FM</option><option value="67">Fox FM</option><option value="66">Boss FM</option><option value="98">Rok FM</option><option value="27">RadioSilverFM</option><option value="28">SkyPowerFM </option><option value="26">RadioMaxxFM</option><option value="25">MelodyFM </option><option value="38">RadioTamaleFM</option><option value="35">Zaa RadioFM</option><option value="36">DiamondFM</option><option value="39">NorthStar RadioFM</option><option value="37">Savannah RadioFM</option><option value="71">Light FM</option><option value="72">Holy FM</option><option value="74">Hope FM</option><option value="70">Jubilee Radio FM</option><option value="73">Victory FM</option><option value="76">Radio Freed FM</option><option value="78">Radford FM</option><option value="75">Radio Upper West</option><option value="77">Radio Progress FM</option><option value="80">Word FM</option><option value="79">A1 Radio FM</option><option value="82">Rock FM</option><option value="81">Radio Gurune FM</option><option value="86">Dinpa FM</option><option value="83">Anapua FM</option><option value="85">Space FM</option><option value="84">Star FM</option><option value="87">Sky FM</option><option value="91">Spark FM</option><option value="92">Solar FM</option><option value="89">Pink FM</option><option value="90">Yes FM</option><option value="88">Sompa FM</option><option value="97">Emak FM</option><option value="93">Eastern FM</option><option value="95">Sunrise FM</option><option value="96">Goodlife FM</option><option value="94">Vision FM</option><option value="12">OkayFM </option></select><br></div></div><div class="wait-loading mb-2"></div><div class="media-insight mb-2"></div><div class="calendar"><table id="tableID" class="calendar table table-bordered table-striped" cellspacing="0" width="100%" border="2"><caption><h3>April 2020 - 0 Available Time Slots</h3>-->
                                            <div class="adlegend" style="width: 100%; margin-bottom: 20px;">
                                                <span style="background: #158DA6; color: #fff; padding:5px 10px; border-radius:5px; margin-right:10px; text-align: center;">TV</span>
                                                <span style="background: #ffb318; color: #fff; padding:5px 10px; border-radius:5px;  text-align: center;">Radio</span>
                                            </div>
                                            <div class="error-msg" style="color: red; font-size: small;"></div></caption>
                                            <!--                                            <tbody><tr><th class="header">Sun</th><th class="header">Mon</th><th class="header">Tue</th><th class="header">Wed</th><th class="header">Thu</th><th class="header">Fri</th><th class="header">Sat</th></tr><tr><td colspan="3">&nbsp;</td><td border="1" class="day" rel="2020-04-01"><span class="daydate">1</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-02"><span class="daydate">2</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-03"><span class="daydate">3</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-04"><span class="daydate">4</span><span class="info"></span> </td></tr><tr><td border="1" class="day" rel="2020-04-05"><span class="daydate">5</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-06"><span class="daydate">6</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-07"><span class="daydate">7</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-08"><span class="daydate">8</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-09"><span class="daydate">9</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-10"><span class="daydate">10</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-11"><span class="daydate">11</span><span class="info"></span> </td></tr><tr><td border="1" class="day" rel="2020-04-12"><span class="daydate">12</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-13"><span class="daydate">13</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-14"><span class="daydate">14</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-15"><span class="daydate">15</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-16"><span class="daydate">16</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-17"><span class="daydate">17</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-18"><span class="daydate">18</span><span class="info"></span> </td></tr><tr><td border="1" class="day" rel="2020-04-19"><span class="daydate">19</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-20"><span class="daydate">20</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-21"><span class="daydate">21</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-22"><span class="daydate">22</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-23"><span class="daydate">23</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-24"><span class="daydate">24</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-25"><span class="daydate">25</span><span class="info"></span> </td></tr><tr><td border="1" class="day" rel="2020-04-26"><span class="daydate">26</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-27"><span class="daydate">27</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-28"><span class="daydate">28</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-29"><span class="daydate">29</span><span class="info"></span> </td><td border="1" class="day" rel="2020-04-30"><span class="daydate">30</span><span class="info"></span> </td><td colspan="2">&nbsp;</td></tr></tbody></table></div><br><div class="custom-control custom-checkbox monitor">-->
                                            <input type="checkbox" class="custom-control-input slotcheck" id="monitor2" name="monitor2" value="1" checked="" required="">
                                            <label class="custom-control-label" for="monitor2">Monitor selected slots</label>
                                        </div><button type="button" name="submit" class="submit-slots btn btn-sm btn-danger" onclick="getPurchaseSummary();">Submit slots</button> <span style="color: #e00;">(Please select an advert space to submit)</span></form></div>
                        </div>
                    </div>
                </div>

                <!-- Edit Advert Space Modal -->
                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel1">Edit Avert</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                            </div>
                            <div class="modal-body edit-advert">

                                <div class="form-view" id="form-view">
                                    <div class="login-content">
                                        <div class="login-form">
                                            <form method="post" action="processing/Client.Processing.php?edit=6" enctype="multipart/form-data" class="form-horizontal">                <div class="row mb-3" style="color: red; padding-bottom:5px;">If Your Ad is larger than 45MB please contact support on 0277977000.<br></div>
                                                <div class="form-elements">
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="text-input" class="">Ad Name</label></div>
                                                        <div class="col-12 col-md-8"><input type="text" id="adname" name="adname" placeholder="Enter advertisment name" value="Kwachua mamili" class="form-control" required=""></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="text-input" class="">Ad Type</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <select id="adtype" name="adtype" class="form-control" required="">
                                                                <option selected="" value="Radio">Radio</option>                                <option value="T">TV</option>
                                                                <option value="R">Radio</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="text-input" class="">Ad Duration</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <select id="adduration" name="adduration" class="form-control" required="">
                                                                <option selected="" value="20">20 Secs</option>                                <option value="30">30 Secs</option>
                                                                <option value="45">45 Secs</option>
                                                                <option value="60">60 Secs</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-4"><label for="file-input" class="">Upload Ad File <br>(Max 45MB)</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                                                <input type="file" class="sr-only" id="inputImage" name="fileToUpload" accept=".mp4, .mp3" required="">
                                                                Select File
                                                            </label>
                                                            <br><span class="small">Current advert link: myads/file_example_MP3_1MG.mp3</span></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-12 small" style="color: red; padding-bottom:5px;">If Ad is for drugs or consumables (food, drinks etc.) please upload FDA Certificate (pdf)<br></div>
                                                        <div class="col col-md-4"><label for="file-input" class="">Upload Certificate<br>(Max 45MB)</label></div>
                                                        <div class="col-12 col-md-8">
                                                            <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                                                <input type="file" class="sr-only" id="inputImage" name="certToUpload" accept=".pdf,.doc,.docx">
                                                                Select File
                                                            </label>
                                                            <br><span class="small">Current certificate link: </span></div>
                                                    </div>
                                                </div>
                                                <div class="form-footer text-right border-top pt-3">
                                                    <button type="submit" class="btn btn-primary" id="edit" name="edit" style="display: inline-block; "> Submit </button>                    <button type="reset" class="btn btn-warning" style=" display: inline-block; ">
                                                        <i class="fa fa-refresh"></i> Reset
                                                    </button>
                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger" style="display: inline-block; ">
                                                        <i class="fa fa-close"></i> Cancel
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<footer class="page-footer" style="opacity: 1;">
    <div class="footer-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="mb-0 text-muted">ColoredStrategies 2019</p>
                </div>
                <div class="col-sm-6 d-none d-sm-block">
                    <ul class="breadcrumb pt-0 pr-0 float-right">
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Review</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Purchase</a>
                        </li>
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Docs</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/vendor/jquery-3.3.1.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/Chart.bundle.min.js" style="opacity: 1;"></script>
<script src="js/vendor/chartjs-plugin-datalabels.js" style="opacity: 1;"></script>
<script src="js/vendor/moment.min.js" style="opacity: 1;"></script>
<script src="js/vendor/fullcalendar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/datatables.min.js" style="opacity: 1;"></script>
<script src="js/vendor/perfect-scrollbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/progressbar.min.js" style="opacity: 1;"></script>
<script src="js/vendor/jquery.barrating.min.js" style="opacity: 1;"></script>
<script src="js/vendor/select2.full.js" style="opacity: 1;"></script>
<script src="js/vendor/nouislider.min.js" style="opacity: 1;"></script>
<script src="js/vendor/bootstrap-datepicker.js" style="opacity: 1;"></script>
<script src="js/vendor/Sortable.js" style="opacity: 1;"></script>
<script src="js/vendor/mousetrap.min.js" style="opacity: 1;"></script>
<script src="js/vendor/glide.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/dataTables.buttons.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.flash.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.html5.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/Buttons/js/buttons.print.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/JSZip/jszip.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/pdfmake.min.js" style="opacity: 1;"></script>
<script src="js/DataTables/pdfmake/vfs_fonts.js" style="opacity: 1;"></script>
<script src="js/dore.script.js" style="opacity: 1;"></script>
<script src="js/scripts.js" style="opacity: 1;"></script><div class="theme-colors default-transition" style="opacity: 1;"><div class="p-4"><p class="text-muted mb-2">Light Theme</p><div class="d-flex flex-row justify-content-between mb-4"><a href="#" data-theme="dore.light.blue.min.css" class="theme-color theme-color-blue active"></a><a href="#" data-theme="dore.light.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.light.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.light.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.light.red.min.css" class="theme-color theme-color-red"></a></div><p class="text-muted mb-2">Dark Theme</p><div class="d-flex flex-row justify-content-between"><a href="#" data-theme="dore.dark.blue.min.css" class="theme-color theme-color-blue"></a><a href="#" data-theme="dore.dark.purple.min.css" class="theme-color theme-color-purple"></a><a href="#" data-theme="dore.dark.green.min.css" class="theme-color theme-color-green"></a><a href="#" data-theme="dore.dark.orange.min.css" class="theme-color theme-color-orange"></a><a href="#" data-theme="dore.dark.red.min.css" class="theme-color theme-color-red"></a></div></div><div class="p-4"><p class="text-muted mb-2">Border Radius</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="roundedRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="rounded"><label class="custom-control-label" for="roundedRadio">Rounded</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="flatRadio" name="radiusRadio" class="custom-control-input radius-radio" data-radius="flat" checked="checked"><label class="custom-control-label" for="flatRadio">Flat</label></div></div><div class="p-4"><p class="text-muted mb-2">Direction</p><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="ltrRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="ltr" checked="checked"><label class="custom-control-label" for="ltrRadio">Ltr</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="rtlRadio" name="directionRadio" class="custom-control-input direction-radio" data-direction="rtl"><label class="custom-control-label" for="rtlRadio">Rtl</label></div></div><a href="#" class="theme-button"> <i class="simple-icon-magic-wand"></i> </a></div>
<script src="js/client.custom.js" style="opacity: 1;"></script>


<!--advert spaces table default display-->


</body></html>